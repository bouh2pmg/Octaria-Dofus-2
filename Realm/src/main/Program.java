package main;

import org.slf4j.bridge.SLF4JBridgeHandler;

import server.inter.InterServer;
import server.realm.RealmServer;
import utils.SqlUtils;
import utils.Utils;

public class Program 
{
	public static void main(String[] args)
	{
		SLF4JBridgeHandler.install();
		SqlUtils.initContext();
		Utils.loadStaticData();
		RealmServer.getInstance().start();
		InterServer.getInstance().start();
		System.out.println("OctoriaR was start !");
	}
}
