package data.sql.service.impl;

import java.util.List;

import data.sql.dao.PlayerDAO;
import data.sql.entity.Player;
import data.sql.service.IPlayerService;


public class PlayerService implements IPlayerService{
	private PlayerDAO playerDao;
	
	public void setPlayerDao(PlayerDAO playerDao) {
        this.playerDao = playerDao;
    }

	@Override
	public Player findById(int id) {
		return (Player)playerDao.getSession().get(Player.class,id);
	}

	@Override
	public Player findByName(String name) {
		return null;
	}

	@Override
	public List<Player> findAll() {
		return null;
	}
}
