package data.sql.service.impl;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

import data.sql.dao.AccountDAO;
import data.sql.entity.Account;
import data.sql.service.IAccountService;

public class AccountService implements IAccountService{
    private AccountDAO accountDao;
    
    public void setAccountDao(AccountDAO accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public Account findById(int id) {
        return (Account) accountDao.getSession().get(Account.class,id);
    }

    @Override
    public Account findByName(String name) {
        Session session = accountDao.getSession();
        Query query = session.getNamedQuery("findAccountByName");
        query.setParameter("name", name);
        Account acc = (Account) query.uniqueResult();
        return acc;
    }

    @Override
    public List<Account> findAll() {
        return null;
    }
}
