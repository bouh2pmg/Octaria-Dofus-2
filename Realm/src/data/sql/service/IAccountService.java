package data.sql.service;

import java.util.List;

import data.sql.entity.Account;

public interface IAccountService {
    public Account findById(int id);
    public Account findByName(String name);
    public List<Account> findAll();
}
