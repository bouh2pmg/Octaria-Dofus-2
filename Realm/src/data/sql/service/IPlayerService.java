package data.sql.service;

import java.util.List;

import data.sql.entity.Player;


public interface IPlayerService {
	public Player findById(int id);
    public Player findByName(String name);
    public List<Player> findAll();
}
