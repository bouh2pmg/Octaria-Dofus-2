package server.inter.client;

import network.inter.InterPacketID;
import network.inter.message.connection.information.Information;

import org.apache.mina.core.buffer.IoBuffer;

import server.inter.InterServer;

public class InterParser {
	private InterClient client;
	
	public InterParser(InterClient client) {
		setClient(client);
	}
	
	@SuppressWarnings("incomplete-switch")
	public void parse(IoBuffer packet) {
		InterPacketID id = InterPacketID.getPacket(packet.get());
		InterServer.getLogger().info("Inter << " + id);
		switch(id) {
		case INFO:
			new Information(client,packet);
			break;
		}
	}

	public InterClient getClient() {
		return client;
	}

	public void setClient(InterClient client) {
		this.client = client;
	}
}
