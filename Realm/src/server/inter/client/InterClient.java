package server.inter.client;

import java.util.Map;
import java.util.TreeMap;

import network.inter.InterPacket;
import network.inter.message.connection.Connected;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

import reader.d2o.Definition;

import utils.Utils;

public class InterClient {
	private static Map<Integer,InterClient> servers = new TreeMap<Integer,InterClient>();
	
	private IoSession session;
	private InterParser parser;
	
	private int id = 0;
	private byte status = 0; // STATUS_UNKNOWN:uint = 0 OFFLINE:uint = 1 STARTING:uint = 2 ONLINE:uint = 3 NOJOIN:uint = 4 SAVING:uint = 5 STOPING:uint = 6 FULL:uint = 7
	private byte completion = 20;//?
	private boolean isSelectable = false;
	private String ip = new String();
	private int port = 0;
	
	public InterClient(IoSession session) {
		setSession(session);
		setParser(new InterParser(this));
		new Connected(this);
	}
	
	public static void addServer(InterClient client) {
		if(!servers.containsKey(client.getId())) {
			servers.put(client.getId(), client);
		}else{
			if(servers.get(client.getId()).equals(client.getId()))
			{
				for(InterClient interClient : servers.values())
				{
					if(interClient.getIp().equals(client.id))
					{
						servers.remove(interClient);
					}
				}
			}
		}
	}
	
	public static void removeServer(InterClient client) {
		if(servers.containsKey(client.getId())) {
			servers.remove(client.getId());
		}else{
			//Send error?Hack?
		}
	}
	
	public IoBuffer toServerList(byte characCount) {
		IoBuffer data = IoBuffer.allocate(2048);
		
		Definition def = Utils.d2o.files.get("Servers").read(id,false);
		if(def != null) {
			data.putShort((short)getId()).putUnsigned(getStatus()).putUnsigned(getCompletion())
			.put(isSelectable() ? (byte)1 : (byte)0).put(characCount)
			.putDouble(Double.valueOf(def.getField("openingDate").value.toString()));
		}else{
			data.putShort((short)getId()).putUnsigned(getStatus()).putUnsigned(getCompletion())
			.put(isSelectable() ? (byte)1 : (byte)0).put(characCount)
			.putDouble(System.currentTimeMillis());
		}
		return data.flip();
	}
	
	public static IoBuffer toServerList(int id,byte characCount) {
		IoBuffer buff = IoBuffer.allocate(2048);
		Definition def = Utils.d2o.files.get("Servers").read(id,false);
		if(def != null) {
			buff.putShort((short)id).putUnsigned(1).putUnsigned(20).put((byte) 0).put(characCount).putDouble(Double.valueOf(def.getField("openingDate").value.toString()));
		}else{
			buff.putShort((short)id).putUnsigned(1).putUnsigned(20).put((byte) 0).put(characCount).putDouble(System.currentTimeMillis());
		}
		return buff.flip();
	}
	
	public void toParse(IoBuffer packet) {
		parser.parse(packet);
	}
	
	public void send(InterPacket packet) {
		getSession().write(packet.packetOut.flip());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public byte getCompletion() {
		return completion;
	}

	public void setCompletion(byte completion) {
		this.completion = completion;
	}

	public boolean isSelectable() {
		return isSelectable;
	}

	public void setSelectable(boolean isSelectable) {
		this.isSelectable = isSelectable;
	}

	public static Map<Integer,InterClient> getServers() {
		return servers;
	}

	public static void setServers(Map<Integer,InterClient> servers) {
		InterClient.servers = servers;
	}

	public IoSession getSession() {
		return session;
	}

	public void setSession(IoSession session) {
		this.session = session;
	}

	public InterParser getParser() {
		return parser;
	}

	public void setParser(InterParser parser) {
		this.parser = parser;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
