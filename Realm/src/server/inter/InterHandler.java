package server.inter;

import network.inter.InterPacketID;
import network.realm.message.login.connection.ServerList;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import server.inter.client.InterClient;
import server.realm.RealmServer;
import server.realm.client.RealmClient;

public class InterHandler extends IoHandlerAdapter {
	@Override
    public void sessionCreated(IoSession session) throws Exception {
		InterClient client = new InterClient(session);
		session.setAttribute("client", client);
    }
	
	@Override
    public void sessionClosed(IoSession session) throws Exception {
		if(session.containsAttribute("client")) {
			if(session.getAttribute("client") instanceof InterClient) {
				InterClient.removeServer((InterClient)session.getAttribute("client"));
				//Close server
				for(IoSession sess : RealmServer.getInstance().getAcceptor().getManagedSessions().values()) 
				{
					RealmClient rClient = (RealmClient)sess.getAttribute("client");
					if(rClient != null) 
					{
						new ServerList(rClient);
					}
				}
			}
		}
    }
	
	@Override
	public void exceptionCaught(IoSession session, Throwable cause) {
	}

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception 
    {
    	if(message != null && message instanceof IoBuffer) 
    	{
    		IoBuffer packet = IoBuffer.allocate(((IoBuffer)message).capacity());
    		packet.put((IoBuffer)message);
    		packet.flip();
    		InterClient client = (InterClient) session.getAttribute("client");
    		client.toParse(packet);
    		packet.free();
    	}
    }
    
    @Override
    public void messageSent(IoSession session, Object message) throws Exception 
    {
    	if(message != null && message instanceof IoBuffer) 
    	{
    		IoBuffer packet = IoBuffer.allocate(((IoBuffer)message).capacity());
    		packet.put((IoBuffer)message);
    		packet.flip();
    		InterServer.getLogger().info("Inter >> " + InterPacketID.getPacket(packet.get()));
    		packet.free();
    	}
    }
}
