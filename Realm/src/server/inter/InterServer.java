package server.inter;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.RealmConfig;

public class InterServer {
	private NioSocketAcceptor acceptor;
	private static InterServer instance;
	private static Logger logger = LoggerFactory.getLogger(InterServer.class);

	private InterServer() {
		setAcceptor(new NioSocketAcceptor());
		getAcceptor().setHandler(new InterHandler());
	}
	
	public static InterServer getInstance() {
		if (instance == null) {
			instance = new InterServer();
		}
		return instance;
	}
	
	public void start() {
		try {
			getAcceptor().bind(new InetSocketAddress(RealmConfig.INTER_IP, RealmConfig.INTER_PORT));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void stop() {
		getAcceptor().unbind();
		getAcceptor().dispose();
	}
	
	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		InterServer.logger = logger;
	}

	public NioSocketAcceptor getAcceptor() {
		return acceptor;
	}

	public void setAcceptor(NioSocketAcceptor acceptor) {
		this.acceptor = acceptor;
	}
}
