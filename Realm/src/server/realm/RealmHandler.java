package server.realm;

import network.Header;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import server.realm.client.RealmClient;

public class RealmHandler extends IoHandlerAdapter {
	@Override
    public void sessionCreated(IoSession session) throws Exception {
		RealmServer.getLogger().info("New connection : " + session.getRemoteAddress());
		session.setAttribute("client", new RealmClient(session));
    }
	
	@Override
    public void sessionClosed(IoSession session) throws Exception {
		RealmServer.getLogger().info("Deconnection : " + session.getRemoteAddress());
		session.close(true);
    }
	
	@Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		RealmServer.getLogger().info("Idle : " + session.getRemoteAddress() + " ->" + session.getIdleCount(status));
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
    	if(message instanceof IoBuffer) {
    		IoBuffer packet = IoBuffer.allocate(((IoBuffer)message).capacity());
    		packet.put((IoBuffer)message);
    		packet.flip();
    		Header header = new Header(packet);
    		if(session.getAttribute("client") instanceof RealmClient) {
    			RealmClient client = (RealmClient) session.getAttribute("client");
    			try{
    				client.getParser().parse(header);
    			}catch(Exception e) {
    				e.printStackTrace();
    				RealmServer.getLogger().error(e.getLocalizedMessage());
    				client.close();
    			}
    		} 
    		packet.free();
    	}
    }
    
    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
    	if(message instanceof IoBuffer) {
    		IoBuffer packet = IoBuffer.allocate(((IoBuffer)message).capacity());
    		packet.put((IoBuffer)message);
    		packet.flip();
    		Header header = new Header(packet);
    		RealmServer.getLogger().info("Realm >> " + header.toString());
    		packet.free();
    	}
    }
}
