package server.realm.client;

import server.realm.RealmServer;
import network.Header;
import network.realm.message.login.connection.IdentificationMessage;
import network.realm.message.login.connection.ServerSelected;

public class RealmParser {
	private RealmClient client;
	
	public RealmParser(RealmClient client) {
		setClient(client);
	}
	
	public void parse(Header header) {
		if(header.getID() == null) {
			return;
		}else{
			RealmServer.getLogger().info("Realm << " + header.toString());
		}
		switch(header.getID()) {
		case MSG_IDENTIFICATION:
			new IdentificationMessage(header,getClient());
			break;
		case MSG_SERVER_SELECTED:
			new ServerSelected(header,getClient());
			break;
		default:
			RealmServer.getLogger().info("Packet unmanaged : " + header.getID());
			break;
		}
	}

	public RealmClient getClient() {
		return client;
	}

	public void setClient(RealmClient client) {
		this.client = client;
	}
}

