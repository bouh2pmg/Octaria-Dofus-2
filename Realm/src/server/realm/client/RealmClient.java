package server.realm.client;

import crypto.RSA;
import network.AbstractMessage;
import network.MessageFormater;
import network.realm.message.login.connection.HelloConnect;
import network.realm.message.login.connection.IdentificationMessage.Certificate;
import network.realm.message.login.handshake.ProtocolRequired;

import org.apache.mina.core.session.IoSession;

import server.realm.RealmServer;
import data.sql.entity.Account;
import utils.Utils;

public class RealmClient {
	private IoSession client;
	private RealmParser parser;
	private String salt;
	private RSA cryptorRSA;
    private Account acc;
    private Certificate certificate;
	
	public RealmClient(IoSession client) {
            try {
                setClient(client);
		setParser(new RealmParser(this));
		setCryptorRSA(new RSA());
		setSalt(Utils.RandomString(32));
		getCryptorRSA().generateKeyPair();
		new ProtocolRequired(this);
		new HelloConnect(this);
            }catch(Exception e) {
                e.printStackTrace();
            }
	}
	
	public void send(AbstractMessage packet) {
            packet.getPacketOut().flip();
            MessageFormater mf = new MessageFormater(packet);
            client.write(mf.getToSend().flip());
	}

	public IoSession getClient() {
            return client;
	}

	public void setClient(IoSession client) {
            this.client = client;
	}

	public RealmParser getParser() {
            return parser;
	}

	public void setParser(RealmParser parser) {
            this.parser = parser;
	}

	public String getSalt() {
            return salt;
	}

	public void setSalt(String salt) {
            this.salt = salt;
	}

	public RSA getCryptorRSA() {
            return cryptorRSA;
	}

	public void setCryptorRSA(RSA cryptorRSA) {
            this.cryptorRSA = cryptorRSA;
	}
        
    public Account getAcc() {
        return acc;
    }

    public void setAcc(Account acc) {
    	this.acc = acc;
    }
    
    public void close() { 
    	client.close(true);
    	if(acc == null)
    	    RealmServer.getLogger().info("Client disconnected !");
    	else
    	    RealmServer.getLogger().info("Client disconnected : " + acc.getGuid());
    }

	public Certificate getCertificate() {
		return certificate;
	}

	public void setCertificate(Certificate certificate) {
		this.certificate = certificate;
	}
}
