package server.realm;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.RealmConfig;

public class RealmServer {
	private NioSocketAcceptor acceptor;
	private static RealmServer instance;
	private static Logger logger = LoggerFactory.getLogger(RealmServer.class);
	
	private RealmServer() {
		setAcceptor(new NioSocketAcceptor());
		getAcceptor().getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, RealmConfig.TIME_OUT);
		getAcceptor().setHandler(new RealmHandler());
	}
	
	public static RealmServer getInstance() {
		if(instance == null){
			instance = new RealmServer();
		}
		return instance;
	}
	
	public void start() {
		try {
			getAcceptor().bind(new InetSocketAddress(RealmConfig.REALM_IP, RealmConfig.REALM_PORT));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void stop() {
		getAcceptor().unbind();
		getAcceptor().dispose();
	}

	public NioSocketAcceptor getAcceptor() {
		return acceptor;
	}

	public void setAcceptor(NioSocketAcceptor acceptor) {
		this.acceptor = acceptor;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		RealmServer.logger = logger;
	}
}
