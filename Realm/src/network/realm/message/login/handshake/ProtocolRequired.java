package network.realm.message.login.handshake;

import server.realm.client.RealmClient;
import utils.RealmConfig;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;

public class ProtocolRequired extends AbstractMessage implements MessageToSend{
        private RealmClient client;
        
	public ProtocolRequired(RealmClient client) {
		super(new Header(MessageID.MSG_PROTOCOL_REQUIRED));
                setClient(client);
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putInt(RealmConfig.REQUIRED_VERSION);
		getPacketOut().putInt(RealmConfig.CURRENT_VERSION);
	}

	@Override
	public void doPacket() {
		getClient().send(this);
	}
        
        public RealmClient getClient() {
            return client;
        }

        public void setClient(RealmClient client) {
            this.client = client;
        }
}

