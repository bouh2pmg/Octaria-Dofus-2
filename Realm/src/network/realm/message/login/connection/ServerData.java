package network.realm.message.login.connection;

import server.inter.client.InterClient;
import server.realm.client.RealmClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
import network.inter.message.account.WaitingAccount;

public class ServerData extends AbstractMessage implements MessageToSend {
	private short serverId = 0;
	private RealmClient client;

	public ServerData(Header header, short serverId, RealmClient client) {
		super(header);
		this.serverId = serverId;
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		InterClient server = InterClient.getServers().get((int)serverId);
		if(server != null) {
			new WaitingAccount(server,client.getSalt(),client.getAcc().getGuid());
			getPacketOut().putShort(serverId);
			writeUTF(server.getIp());
			getPacketOut().putUnsignedShort(server.getPort());
			getPacketOut().put((byte)(client.getAcc().getCharacters().size() >= 5 ? 0 : 1));
			writeUTF(client.getSalt());
		}
	}

	@Override
	public void doPacket() {
		client.send(this);
		client.close();
	}
}
