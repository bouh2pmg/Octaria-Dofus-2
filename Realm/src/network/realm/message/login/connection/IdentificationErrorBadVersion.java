package network.realm.message.login.connection;

import server.realm.client.RealmClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
import network.realm.message.login.connection.IdentificationMessage.Version;

public class IdentificationErrorBadVersion extends AbstractMessage implements
		MessageToSend {
	private RealmClient client;
	private Version version;

	public IdentificationErrorBadVersion(Header header, RealmClient client, Version version) {
		super(header);
		this.client = client;
		this.version = version;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put((byte) 1);//Reason : 1 = BadVersion
		getPacketOut().put(version.getInfo());
	}

	@Override
	public void doPacket() {
		client.send(this);
		client.close();
	}
}
