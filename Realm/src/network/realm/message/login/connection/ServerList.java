package network.realm.message.login.connection;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import data.sql.entity.Player;

import server.inter.client.InterClient;
import server.realm.client.RealmClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;

public class ServerList extends AbstractMessage implements MessageToSend {
	private RealmClient client;
	
	public ServerList(RealmClient client) {
		super(new Header(MessageID.MSG_SERVERLIST));
		setClient(client);
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		Map<Integer,Byte> data = new TreeMap<Integer,Byte>();
		for(InterClient client : InterClient.getServers().values()) {
			data.put(client.getId(), (byte) 0);
		}
		for (Player player: client.getAcc().getCharacters()) {
			int gameGuid = player.getGameGuid();
			if(!data.containsKey(gameGuid)){
				data.put(gameGuid,(byte) 1);
			}else{
				int count = data.get(gameGuid);
				data.remove(gameGuid);
				data.put(gameGuid,(byte)(count+1));
			}
        }
		getPacketOut().putShort((short)data.size());
		for(InterClient client : InterClient.getServers().values()) {
			getPacketOut().put(client.toServerList(data.get(client.getId())));
			data.remove(client.getId());
		}
		if(!data.isEmpty()) {
			for(Entry<Integer,Byte> entry : data.entrySet()) {
				getPacketOut().put(InterClient.toServerList(entry.getKey(), entry.getValue()));
			}
		}
	}

	@Override
	public void doPacket() {
		getClient().send(this);
	}

	public RealmClient getClient() {
		return client;
	}

	public void setClient(RealmClient client) {
		this.client = client;
	}
}
