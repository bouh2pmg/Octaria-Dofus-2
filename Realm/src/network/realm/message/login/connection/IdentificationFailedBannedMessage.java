package network.realm.message.login.connection;

import server.realm.client.RealmClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;


public class IdentificationFailedBannedMessage  extends AbstractMessage implements
		MessageToSend {
	private RealmClient client;
	private long banTime;
	

	public IdentificationFailedBannedMessage (RealmClient client,long banTime) {
		super(new Header(MessageID.MSG_E_FAILED_BANNED));
		this.banTime = banTime;
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putLong(banTime);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
		client.close();
	}
}