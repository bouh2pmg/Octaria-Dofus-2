package network.realm.message.login.connection;

import server.realm.client.RealmClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class ServerSelected extends AbstractMessage implements MessageReceived{
	private short serverId = 0;
	private RealmClient client;
	

	public ServerSelected(Header header, RealmClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() {
		serverId = getPacketIn().getShort();
	}
	
	@Override
	public void doPacket() {
		new ServerData(new Header(MessageID.MSG_SERVER_DATA),serverId,client);
	}
}
