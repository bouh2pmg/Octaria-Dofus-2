package network.realm.message.login.connection;

import java.util.BitSet;

import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;
import server.realm.client.RealmClient;
import utils.BitsUtils;

public class IdentificationSuccessMessage extends AbstractMessage implements MessageToSend{
    private RealmClient client;
    
    public IdentificationSuccessMessage(RealmClient client) {
        super(new Header(MessageID.MSG_IDENTIFICATION_SUCCESS));
        setClient(client);
        pack();
        doPacket();
    }
    
    @Override
    public void pack() {
        BitSet bits = new BitSet(8);
        bits.set(0,getClient().getAcc().hasRight());
        bits.set(1,getClient().getAcc().isConnected());
        getPacketOut().put(BitsUtils.toByte(bits));
        writeUTF(getClient().getAcc().getName());
        writeUTF(getClient().getAcc().getPseudo());
        getPacketOut().putInt(getClient().getAcc().getGuid());
        getPacketOut().put(getClient().getAcc().getCommunity());
        writeUTF(getClient().getAcc().getQuestion());
        getPacketOut().putDouble(getClient().getAcc().getEndDate());
        getPacketOut().putDouble(getClient().getAcc().getCreationDate());
    }
    
    @Override
    public void doPacket() {
        getClient().send(this);
    }
    public RealmClient getClient() {
        return client;
    }

    public void setClient(RealmClient client) {
        this.client = client;
    }
}
