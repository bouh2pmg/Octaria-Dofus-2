package network.realm.message.login.connection;

import server.realm.client.RealmClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;

public class IdentificationFailed extends AbstractMessage implements MessageToSend 
{
	private RealmClient client;
	private int errorID;

	public IdentificationFailed(RealmClient client,int errorID) {
		super(new Header(MessageID.MSG_IDENTIFICATION_FAILED));
		this.errorID = errorID;
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put((byte) errorID);
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
