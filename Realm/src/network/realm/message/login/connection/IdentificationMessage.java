package network.realm.message.login.connection;

import java.util.BitSet;

import org.apache.mina.core.buffer.IoBuffer;

import data.sql.entity.Account;

import server.realm.client.RealmClient;
import server.realm.RealmServer;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;
import utils.BitsUtils;
import utils.RealmConfig;
import utils.SqlUtils;

public class IdentificationMessage extends AbstractMessage implements MessageReceived 
{
	private boolean autoConnect = false;
	private boolean useCertificate = false;
	private boolean useLoginToken = false;//Login with account name?
	private Version version = new Version();
	private String lang = "";
	private String login = "";
	private String pass = "";
	private byte[] credential;
	private byte serverID = 0;
	private RealmClient client;

	public IdentificationMessage(Header header, RealmClient client) 
	{
		super(header);
		setClient(client);
		unpack();
		doPacket();
	}

	@Override
	public void unpack() 
	{
		BitSet bits = BitsUtils.fromByte(getPacketIn().get());
		autoConnect = bits.get(0);
		useCertificate = bits.get(1);
		useLoginToken = bits.get(2);
		
		/* Version Dofus */
		version.setMajor(getPacketIn().get());
		version.setMinor(getPacketIn().get());
		version.setRelease(getPacketIn().get());
		version.setRevision(getPacketIn().getUnsignedShort());
		version.setPatch(getPacketIn().get());
		version.setBuildType(getPacketIn().get());
		version.setInstall(getPacketIn().get());
		version.setTechnology(getPacketIn().get());

		lang = readUTF();
		login = readUTF();
		int credenLength = getPacketIn().getUnsignedShort();
		credential = new byte[credenLength];
		for(int i = 0; i < credenLength; i++) 
		{
			credential[i] = getPacketIn().get();
		}
		if(!useCertificate) // Si il n'utilise pas le systeme de certificat.
		{
			pass = (getClient().getCryptorRSA().decryptInString(credential)).replace(getClient().getSalt(), "");
		}
		else // Sinon ...
		{
			System.out.println("Using certificate");
			IoBuffer buff = IoBuffer.wrap(getClient().getCryptorRSA().decryptInBytes(credential));

			int length = buff.getInt();
			StringBuilder salt = new StringBuilder();
			for(int i = 0; i < length; i++)
			{
				char c = (char)buff.get();
				salt.append(c);
			}

			int certificateId = buff.getInt();
			int certiLength = buff.getInt();
			StringBuilder certificate = new StringBuilder();
			for(int i = 0; i < certiLength; i++)
			{
				char c = (char)buff.get();
				certificate.append(c);
			}
			getClient().setCertificate(new Certificate(certificateId,certificate.toString()));

			int passLength = buff.getInt();
			StringBuilder passe = new StringBuilder();
			for(int i = 0; i < passLength; i++)
			{
				char c = (char)buff.get();
				passe.append(c);
			}
			pass = passe.toString();
		}
		serverID = getPacketIn().get();
	}

	@Override
	public void doPacket() {
		if(RealmConfig.USE_VERSION)
		{
			if(!(version.toString().equals(RealmConfig.DOFUS_VERSION))) {
				new IdentificationErrorBadVersion(new Header(MessageID.MSG_E_BAD_VERSION),getClient(),Version.getVersionByString(RealmConfig.DOFUS_VERSION));
				return;
			}
		}
		else if(!lang.equals("fr")) 
		{
			new IdentificationFailed(getClient(),5); // Il n'as pas la bonne lang :)
			getClient().close(); // Ont le fout dehord un coup de pied dans le cul !
			return;
		}
		Account acc = SqlUtils.getAccountService().findByName(login);
		if(useLoginToken) // Si il utilise un token pour ce connecter
		{
			if(acc != null) 
			{
				getClient().setAcc(acc);
				RealmServer.getLogger().info("Account connected : " + acc.getGuid() + " -> " + getClient().getClient().getRemoteAddress());
				if(autoConnect && serverID > 0) 
				{
					new IdentificationSuccessMessage(getClient());
					new ServerData(new Header(MessageID.MSG_SERVER_DATA),serverID,getClient());
				}
				else
				{
					new IdentificationSuccessMessage(getClient());
					new ServerList(getClient());
				}
			}
			else
			{
				RealmServer.getLogger().info("Error login : " + client.getClient().getRemoteAddress());
				new IdentificationFailed(getClient(),2);
				getClient().close();
				return;
			}
		}
		else if(acc != null && acc.getPassword().equals(pass))
		{
			getClient().setAcc(acc);
			RealmServer.getLogger().info("Account connected : " + acc.getGuid() + " -> " + getClient().getClient().getRemoteAddress());
			if(autoConnect && serverID > 0)
			{
				new IdentificationSuccessMessage(getClient());
				new ServerData(new Header(MessageID.MSG_SERVER_DATA),serverID,getClient());
			}
			else
			{
				new IdentificationSuccessMessage(getClient());
				new ServerList(getClient());
			}
		}else
		{
			new IdentificationFailed(getClient(),2);
			getClient().close();
			return;
		}
	}

	public RealmClient getClient() 
	{
		return client;
	}

	public void setClient(RealmClient client) 
	{
		this.client = client;
	}

	public static class Version 
	{
		private int major= 0;
		private int minor = 0;
		private int release = 0;
		private int revision = 0;
		private int patch = 0;
		private int buildType = 0;
		private int install = 0;
		private int technology = 0;

		public int getMajor() {
			return major;
		}
		public void setMajor(int major) {
			this.major = major;
		}
		public int getMinor() {
			return minor;
		}
		public void setMinor(int minor) {
			this.minor = minor;
		}
		public int getRelease() {
			return release;
		}
		public void setRelease(int release) {
			this.release = release;
		}
		public int getRevision() {
			return revision;
		}
		public void setRevision(int revision) {
			this.revision = revision;
		}
		public int getPatch() {
			return patch;
		}
		public void setPatch(int patch) {
			this.patch = patch;
		}
		public int getBuildType() {
			return buildType;
		}
		public void setBuildType(int buildType) {
			this.buildType = buildType;
		}

		@Override
		public String toString() {
			return major + "." + minor + "." + release + "." + revision + "." + patch;
		}

		public static Version getVersionByString(String version) {
			Version vers = new Version();
			String[] info = version.split(".");
			vers.major = Integer.parseInt(info[0]);
			vers.minor = Integer.parseInt(info[1]);
			vers.release = Integer.parseInt(info[2]);
			vers.revision = Integer.parseInt(info[3]);
			vers.patch = Integer.parseInt(info[4]);
			vers.buildType = 0;
			return vers;
		}

		public IoBuffer getInfo()
		{
			IoBuffer buff = IoBuffer.allocate(10);
			buff.put((byte) major);
			buff.put((byte) minor);
			buff.put((byte) release);
			buff.putShort((short) revision);
			buff.put((byte) patch);
			buff.put((byte) buildType);
			return buff.flip();
		}

		public int getInstall() {
			return install;
		}
		public void setInstall(int install) {
			this.install = install;
		}
		public int getTechnology() {
			return technology;
		}
		public void setTechnology(int technology) {
			this.technology = technology;
		}
	}

	public static class Certificate 
	{
		private int id = 0;
		private String hash = "";

		public Certificate(int id,String hash) {
			setId(id);
			setHash(hash);
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getHash() {
			return hash;
		}

		public void setHash(String hash) {
			this.hash = hash;
		}
	}
}

