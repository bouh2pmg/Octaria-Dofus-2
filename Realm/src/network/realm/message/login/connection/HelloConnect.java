package network.realm.message.login.connection;

import server.realm.client.RealmClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;

public class HelloConnect extends AbstractMessage implements MessageToSend {
        private RealmClient client;

	public HelloConnect(RealmClient client) {
		super(new Header(MessageID.MSG_HELLO_CONNECT));
        setClient(client);
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		writeUTF(getClient().getSalt());
		getPacketOut().putUnsignedShort(getClient().getCryptorRSA().getPk().getEncoded().length);
		for(byte b : getClient().getCryptorRSA().getPk().getEncoded()) {
			getPacketOut().put(b);
		}
	}

	@Override
	public void doPacket() {
		getClient().send(this);
	}

        public RealmClient getClient() {
            return client;
        }

        public void setClient(RealmClient client) {
            this.client = client;
        }
}

