package network.inter.message.connection.information;

import org.apache.mina.core.buffer.IoBuffer;

import server.inter.client.InterClient;
import network.inter.InterPacket;
import network.inter.InterPacketID;

public class InformationAgreed extends InterPacket {
	public InformationAgreed(InterClient client) {
		super(client);
		pack(null);
		doPacket(null);
	}

	@Override
	public void unpack(IoBuffer buffer) {
	}

	@Override
	public void pack(IoBuffer buffer) {
		packetOut.put(InterPacketID.INFO_AGREED.getId());
	}

	@Override
	public void doPacket(IoBuffer buffer) {
		client.send(this);
	}
}
