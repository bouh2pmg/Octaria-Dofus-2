package network.inter.message.connection.information;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

import server.inter.InterServer;
import server.inter.client.InterClient;
import server.realm.client.RealmClient;
import server.realm.RealmServer;
import network.inter.InterPacket;
import network.realm.message.login.connection.ServerList;

public class Information extends InterPacket 
{
	public Information(InterClient client, IoBuffer packet) {
		super(client);
		unpack(packet);
		doPacket(packet);
	}

	@Override
	public void unpack(IoBuffer buffer) {
		client.setId(buffer.get());
		client.setCompletion(buffer.get());
		client.setIp(readString(buffer));
		client.setPort(buffer.getShort());
		client.setStatus((byte) buffer.getInt());
		
	}

	@Override
	public void pack(IoBuffer buffer) {
	}

	@Override
	public void doPacket(IoBuffer buffer) {
		client.setSelectable(true);
		InterServer.getLogger().info("New GameServer : " + client.getId());
		InterClient.addServer(client);
		new InformationAgreed(client);
		for(IoSession sess : RealmServer.getInstance().getAcceptor().getManagedSessions().values()) {
			RealmClient rClient = (RealmClient)sess.getAttribute("client");
			if(rClient != null) {
				new ServerList(rClient);
			}
		}
	}
}
