package network.inter.message.account;

import org.apache.mina.core.buffer.IoBuffer;

import server.inter.client.InterClient;
import network.inter.InterPacket;
import network.inter.InterPacketID;

public class WaitingAccount extends InterPacket {
	private String ticket = new String();
	private int accId = 0;
	
	public WaitingAccount(InterClient client,String ticket,int accountId) {
		super(client);
		this.ticket = ticket;
		this.accId = accountId;
		pack(null);
		doPacket(null);
	}

	@Override
	public void unpack(IoBuffer buffer) {
	}

	@Override
	public void pack(IoBuffer buffer) {
		packetOut.put(InterPacketID.ACC_WAITING.getId());
		writeString(ticket);
		packetOut.putInt(accId);
	}

	@Override
	public void doPacket(IoBuffer buffer) {
		client.send(this);
	}
}
