package utils;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import data.sql.service.IAccountService;
import data.sql.service.IPlayerService;

public class SqlUtils {
    private static IAccountService accountService;
    private static IPlayerService playerService;

    public static void initContext() 
    {
    	String[] springFiles = { "Hibernate.xml", "dao.xml" };
    	ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(springFiles);
    	accountService = (IAccountService) applicationContext.getBean("accountService");
    	playerService = (IPlayerService) applicationContext.getBean("playerService");
    }
    
    public static IAccountService getAccountService()
    {
        return accountService;
    }

	public static IPlayerService getPlayerService() 
	{
		return playerService;
	}
}
