package utils;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.ConfigurationException;

public class RealmConfig {
	
	static XMLConfiguration config = null;
	
	public static boolean readConfig(String path)
	{
    	try 
    	{
        	config = new XMLConfiguration();
        	config.setFileName(path);
        	config.load();
        	TIME_OUT = (config.getInt("TIME_OUT")*60*1000);
        	REALM_IP = config.getString("REALM_IP");
        	REALM_PORT = config.getInt("REALM_PORT");
        	INTER_IP = config.getString("INTER_IP");
        	INTER_PORT = config.getInt("INTER_PORT");
        	DOFUS_VERSION = config.getString("DOFUS_VERSION");
        	D2O_PATH = config.getString("D2O_PATH");
        	config = null;
    	}
    	catch (ConfigurationException ex) 
    	{
    		ex.printStackTrace();
    		return false;
    	}
    	return true;
    }
    
    public static int TIME_OUT = (10*1000*60);
	
    public static String REALM_IP = "127.0.0.1";
    public static int REALM_PORT = 1234;
    
    public static String INTER_IP = "127.0.0.1";
    public static int INTER_PORT = 446;
    
    public static int REQUIRED_VERSION = 1459;
    public static int CURRENT_VERSION = 1459;

    public static String DOFUS_VERSION = "2.6.4.58169.2";
    
    public static String D2O_PATH = "C://Program Files (x86)//Dofus2//app//data//common";
    
    public static boolean USE_VERSION = false;
}
