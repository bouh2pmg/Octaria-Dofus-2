package utils;

import java.util.Calendar;

public class Time {
	private Calendar time = Calendar.getInstance();
	
	public Time() {
		time.setTimeInMillis(System.currentTimeMillis());
	}
	
	public void setTime(long date) {
		time.setTimeInMillis(date);
	}
	
	public void addTime(long date) {
		time.setTimeInMillis(time.getTimeInMillis() + date);
	}
	
	public double getTimeDouble() {
		return time.getTimeInMillis();
	}
}
