package reader.d2p.map;

import org.apache.mina.core.buffer.IoBuffer;

public class CellData {
	private int cellId = 0;
	private int x = 0;
	private int y = 0;
	//private int floor = 0;
	private short losmov = 0;
	private byte speed = 0;
	private short mapChangeData = 0;
	private short moveZone = 0;
	
	public CellData(int id) {
		cellId = id;
	}
	
	public void read(IoBuffer reader,int mapVersion) {
		int floor = reader.get() * 10;
		if(floor == -1280)
			return;
		losmov = reader.getUnsigned();
		speed = reader.get();
		mapChangeData = reader.getUnsigned();
		if(mapVersion > 5)
			moveZone = reader.getUnsigned();
	}
	
	public boolean getMov() {
		return (losmov & 1) == 1;
	}
	
	public boolean getLos() {
		return ((losmov & 2) >> 1) == 1;
	}
	
	public boolean isNonWalkableDuringFight() {
		return ((losmov & 4) >> 2) == 1;
	}
	
	public boolean isRedFight() {
		return ((losmov & 8) >> 3) == 1;
	}
	
	public boolean isBlueFight() {
		return ((losmov & 16) >> 4) == 1;
	}
	
	public boolean isFarmCell() {
		return ((losmov & 32) >> 5) == 1;
	}
	
	public boolean isVisible() {
		return ((losmov & 64) >> 6) == 1;
	}
	
	public boolean isNonWalkableDuringRP() {
		return ((losmov & 128) >> 7) == 1;
	}

	public int getCellId() {
		return cellId;
	}

	public void setCellId(int cellId) {
		this.cellId = cellId;
	}

	/*public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}*/

	public short getLosmov() {
		return losmov;
	}

	public void setLosmov(short losmov) {
		this.losmov = losmov;
	}

	public byte getSpeed() {
		return speed;
	}

	public void setSpeed(byte speed) {
		this.speed = speed;
	}

	public short getMapChangeData() {
		return mapChangeData;
	}

	public void setMapChangeData(short mapChangeData) {
		this.mapChangeData = mapChangeData;
	}

	public short getMoveZone() {
		return moveZone;
	}

	public void setMoveZone(short moveZone) {
		this.moveZone = moveZone;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void setCoord(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
