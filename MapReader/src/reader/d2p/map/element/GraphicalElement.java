package reader.d2p.map.element;

import org.apache.mina.core.buffer.IoBuffer;

public class GraphicalElement {
	/*private long elementId = 0;
	private ColorMultiplier hue;
	private ColorMultiplier shadow;
	private Point offset = new Point();
	private Point pixelOffset = new Point();
	private byte altitude = 0;
	private long identifier = 0;
	private ColorMultiplier finalTeint;
	private int cell_half_width = 43;
	private double cell_half_height = 21.5;*/
	
	/*public void read(IoBuffer reader,int mapVersion) {
		elementId = reader.getUnsignedInt();
		hue = new ColorMultiplier(reader.get(),reader.get(),reader.get());
		shadow = new ColorMultiplier(reader.get(),reader.get(),reader.get());
		if(mapVersion <= 4) {
			offset.x = reader.get();
			offset.y = reader.get();
			pixelOffset.x = (offset.x * cell_half_width);
			pixelOffset.y = (int) (offset.y * cell_half_height);
		}else{
			offset.x = reader.getShort();
			offset.y = reader.getShort();
			pixelOffset.x = (offset.x / cell_half_width);
			pixelOffset.y = (int) (offset.y / cell_half_height);
		}
		altitude = reader.get();
		identifier = reader.getUnsignedInt();
	}*/
	
	public void read(IoBuffer reader,int mapVersion) {
		reader.getUnsignedInt();
		reader.get();
		reader.get();
		reader.get();
		reader.get();
		reader.get();
		reader.get();
		if(mapVersion <= 4) {
			reader.get();
			reader.get();
		}else{
			reader.getShort();
			reader.getShort();
		}
		reader.get();
		reader.getUnsignedInt();
	}
	
	/*public void calculeFinalTeint() {
		byte loc1 = (byte) (hue.getRed() + shadow.getRed());
		byte loc2 = (byte) (hue.getGreen() + shadow.getGreen());
		byte loc3 = (byte) (hue.getBlue() + shadow.getBlue());
		loc1 = (byte) ColorMultiplier.clamp((loc1 + 128) * 2, 0, 512);
		loc2 = (byte) ColorMultiplier.clamp((loc2 + 128) * 2, 0, 512);
		loc3 = (byte) ColorMultiplier.clamp((loc3 + 128) * 2, 0, 512);
		finalTeint = new ColorMultiplier(loc1,loc2,loc3);
	}

	public long getElementId() {
		return elementId;
	}

	public void setElementId(long elementId) {
		this.elementId = elementId;
	}

	public ColorMultiplier getHue() {
		return hue;
	}

	public void setHue(ColorMultiplier hue) {
		this.hue = hue;
	}

	public ColorMultiplier getShadow() {
		return shadow;
	}

	public void setShadow(ColorMultiplier shadow) {
		this.shadow = shadow;
	}

	public Point getOffset() {
		return offset;
	}

	public void setOffset(Point offset) {
		this.offset = offset;
	}

	public Point getPixelOffset() {
		return pixelOffset;
	}

	public void setPixelOffset(Point pixelOffset) {
		this.pixelOffset = pixelOffset;
	}

	public byte getAltitude() {
		return altitude;
	}

	public void setAltitude(byte altitude) {
		this.altitude = altitude;
	}

	public long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(long identifier) {
		this.identifier = identifier;
	}

	public ColorMultiplier getFinalTeint() {
		return finalTeint;
	}

	public void setFinalTeint(ColorMultiplier finalTeint) {
		this.finalTeint = finalTeint;
	}

	public int getCell_half_width() {
		return cell_half_width;
	}

	public void setCell_half_width(int cell_half_width) {
		this.cell_half_width = cell_half_width;
	}

	public double getCell_half_height() {
		return cell_half_height;
	}

	public void setCell_half_height(double cell_half_height) {
		this.cell_half_height = cell_half_height;
	}*/
}
