package reader.d2p.map.element;

public class ColorMultiplier {
	private byte red;
	private byte green;
	private byte blue;
	
	public ColorMultiplier(byte red,byte green,byte blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
	
	public static int clamp(int arg1,int arg2,int arg3) {
		if(arg1 > arg3)
			return arg3;
		if(arg1 < arg2)
			return arg2;
		return arg1;
	}

	public byte getRed() {
		return red;
	}

	public void setRed(byte red) {
		this.red = red;
	}

	public byte getGreen() {
		return green;
	}

	public void setGreen(byte green) {
		this.green = green;
	}

	public byte getBlue() {
		return blue;
	}

	public void setBlue(byte blue) {
		this.blue = blue;
	}
}
