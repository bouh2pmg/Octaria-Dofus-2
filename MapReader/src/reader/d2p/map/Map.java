package reader.d2p.map;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import org.apache.mina.core.buffer.IoBuffer;

import reader.d2p.CompressedMap;

public class Map {
	private static byte[] decryptionKey = "649ae451ca33ec53bbcbcc33becf15f4".getBytes();
	
	private IoBuffer reader;
	private CompressedMap map;
	
	private long mapId = 0;
	private int subAreaId = 0;
	private int topNeighbourId = 0;
	private int botNeighbourId = 0;
	private int leftNeighbourId = 0;
	private int rightNeighbourId = 0;
	/*private int shadowBonusOnEntity = 0;
	private byte backgroundColorRed = 0;
	private byte backgroundColorGreen = 0;
	private byte backgroundColorBlue = 0;
	private int backgroundColor = 0;
	private int zoomScale = 0;
	private short zoomOffsetX = 0;
	private short zoomOffsetY = 0;
	private boolean useLowPassFilter = false;
	private boolean useReverb = false;
	private int presetId = -1;
	private List<Fixture> backgrounds = new ArrayList<Fixture>();
	private List<Fixture> foregrounds = new ArrayList<Fixture>();
	private int groundCRC = 0;
	private List<Layer> layers = new ArrayList<Layer>();*/
	private static int cellCount = 560;
	private List<CellData> cells = new ArrayList<CellData>();
	private static int mapWidth = 14;
	private static int mapHeight = 20;
	
	public Map(CompressedMap map) {
		this.map = map;
		reader = map.getRaw();
		initialize();
		decomp();
		reader.free();
		reader = null;
		map = null;
	}
	
	private void initialize() {
		reader.position((int) map.getOffset());
		byte[] flux = new byte[(int) map.getByteCount()];
		reader.get(flux, 0, (int) map.getByteCount());
		
		Inflater decompressor = new Inflater(false);
		decompressor.setInput(flux,0,flux.length);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] dataInf = new byte[flux.length];
		try {
			while(!decompressor.finished()) {
				decompressor.inflate(dataInf);
				bos.write(dataInf, 0, dataInf.length);
			}
		} catch (DataFormatException e) {
			e.printStackTrace();
		}
		byte[] decompressed = bos.toByteArray();
		try {
			bos.close();
			decompressor.end();
		} catch (IOException e) {
			e.printStackTrace();
		}
		reader = IoBuffer.wrap(decompressed);
		bos = null;
		decompressor = null;
		flux = null;
		dataInf = null;
		decompressed = null;
	}
	
	private void decomp() {
		byte header = reader.get();
		if(header != 77) {
			return;
		}
		byte mapVersion = reader.get();
		mapId = reader.getUnsignedInt();
		
		if(mapVersion >= 7) {
			boolean encrypted = reader.get() == 1;
			reader.get();
			int dataLen = reader.getInt();
			if(encrypted) {
				if(!(decryptionKey == null)) {
					byte[] mapData = new byte[dataLen];
					reader.get(mapData, 0, dataLen);
					for(int i = 0; i < dataLen; i++) {
						mapData[i] = (byte) (mapData[i] ^ decryptionKey[i % decryptionKey.length]);
					}
					reader = IoBuffer.wrap(mapData);
					mapData = null;
				}
			}
		}
		
		reader.getUnsignedInt();
		reader.get();
		subAreaId = reader.getInt();
		topNeighbourId = reader.getInt();
		botNeighbourId = reader.getInt();
		leftNeighbourId = reader.getInt();
		rightNeighbourId = reader.getInt();
		//Le plus important est lu, le reste est useless (Pour l'instant) 
		
		/*shadowBonusOnEntity = reader.getInt();
		if(mapVersion >= 3) {
			backgroundColorRed = reader.get();
			backgroundColorGreen = reader.get();
			backgroundColorBlue = reader.get();
			backgroundColor = (backgroundColorRed & 255) << 16 | (backgroundColorGreen & 255) << 8 | (backgroundColorBlue & 255);
		}
		if(mapVersion >= 4) {
			zoomScale = (reader.getUnsignedShort() / 100);
			zoomOffsetX = reader.getShort();
			zoomOffsetY = reader.getShort();
		}
		useLowPassFilter = (reader.get() == 1);
		useReverb = (reader.get() == 1);
		if(useReverb)
			presetId = reader.getInt();
		byte backgroundCount = reader.get();
		for(int i = 0; i < backgroundCount; i++) {
			Fixture fixture = new Fixture();
			fixture.read(reader);
			backgrounds.add(fixture);
		}
		byte foregroundCount = reader.get();
		for(int i = 0; i < foregroundCount; i++) {
			Fixture fixture = new Fixture();
			fixture.read(reader);
			foregrounds.add(fixture);
		}
		reader.getInt();
		groundCRC = reader.getInt();
		byte layerCount = reader.get();
		for(int i = 0; i < layerCount; i++) {
			Layer layer = new Layer();
			layer.read(reader, mapVersion);
			layers.add(layer);
		}*/
		reader.getInt();
		if(mapVersion >= 3) {
			reader.get();
			reader.get();
			reader.get();
		}
		if(mapVersion >= 4) {
			reader.getUnsignedShort();
			reader.getShort();
			reader.getShort();
		}
		reader.get();
		boolean useReverb = reader.get() == 1;
		if(useReverb)
			reader.getInt();
		byte backgroundCount = reader.get();
		for(int i = 0; i < backgroundCount; i++) {
			Fixture fixture = new Fixture();
			fixture.read(reader);
		}
		byte foregroundCount = reader.get();
		for(int i = 0; i < foregroundCount; i++) {
			Fixture fixture = new Fixture();
			fixture.read(reader);
		}
		reader.getInt();
		reader.getInt();
		byte layerCount = reader.get();
		for(int i = 0; i < layerCount; i++) {
			Layer layer = new Layer();
			layer.read(reader, mapVersion);
		}
		for(int i = 0; i < cellCount; i++) {
			CellData cell = new CellData(i);
			cell.read(reader, mapVersion);
			cells.add(cell);
		}
		int loc1 = 0;
		int loc2 = 0;
		int cellId = 0;
		int loc4 = 0;
		int loc5 = 0;
		while(loc5 < mapHeight) {
			loc4 = 0;
			while(loc4 < mapWidth) {
				cells.get(cellId).setCoord(loc1 + loc4, loc2 + loc4);
				cellId++;
				loc4++;
			}
			loc1++;
			loc4 = 0;
			while(loc4 < mapWidth) {
				cells.get(cellId).setCoord(loc1 + loc4, loc2 + loc4);
				cellId++;
				loc4++;
			}
			loc2--;
			loc5++;
		}
	}

	public long getMapId() {
		return mapId;
	}

	public void setMapId(long mapId) {
		this.mapId = mapId;
	}

	public int getSubAreaId() {
		return subAreaId;
	}

	public void setSubAreaId(int subAreaId) {
		this.subAreaId = subAreaId;
	}

	public int getTopNeighbourId() {
		return topNeighbourId;
	}

	public void setTopNeighbourId(int topNeighbourId) {
		this.topNeighbourId = topNeighbourId;
	}

	public int getBotNeighbourId() {
		return botNeighbourId;
	}

	public void setBotNeighbourId(int botNeighbourId) {
		this.botNeighbourId = botNeighbourId;
	}

	public int getLeftNeighbourId() {
		return leftNeighbourId;
	}

	public void setLeftNeighbourId(int leftNeighbourId) {
		this.leftNeighbourId = leftNeighbourId;
	}

	public int getRightNeighbourId() {
		return rightNeighbourId;
	}

	public void setRightNeighbourId(int rightNeighbourId) {
		this.rightNeighbourId = rightNeighbourId;
	}

	/*public int getShadowBonusOnEntity() {
		return shadowBonusOnEntity;
	}

	public void setShadowBonusOnEntity(int shadowBonusOnEntity) {
		this.shadowBonusOnEntity = shadowBonusOnEntity;
	}

	public byte getBackgroundColorRed() {
		return backgroundColorRed;
	}

	public void setBackgroundColorRed(byte backgroundColorRed) {
		this.backgroundColorRed = backgroundColorRed;
	}

	public byte getBackgroundColorGreen() {
		return backgroundColorGreen;
	}

	public void setBackgroundColorGreen(byte backgroundColorGreen) {
		this.backgroundColorGreen = backgroundColorGreen;
	}

	public byte getBackgroundColorBlue() {
		return backgroundColorBlue;
	}

	public void setBackgroundColorBlue(byte backgroundColorBlue) {
		this.backgroundColorBlue = backgroundColorBlue;
	}

	public int getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public int getZoomScale() {
		return zoomScale;
	}

	public void setZoomScale(int zoomScale) {
		this.zoomScale = zoomScale;
	}

	public short getZoomOffsetX() {
		return zoomOffsetX;
	}

	public void setZoomOffsetX(short zoomOffsetX) {
		this.zoomOffsetX = zoomOffsetX;
	}

	public short getZoomOffsetY() {
		return zoomOffsetY;
	}

	public void setZoomOffsetY(short zoomOffsetY) {
		this.zoomOffsetY = zoomOffsetY;
	}

	public boolean isUseLowPassFilter() {
		return useLowPassFilter;
	}

	public void setUseLowPassFilter(boolean useLowPassFilter) {
		this.useLowPassFilter = useLowPassFilter;
	}

	public boolean isUseReverb() {
		return useReverb;
	}

	public void setUseReverb(boolean useReverb) {
		this.useReverb = useReverb;
	}

	public int getPresetId() {
		return presetId;
	}

	public void setPresetId(int presetId) {
		this.presetId = presetId;
	}

	public List<Fixture> getBackgrounds() {
		return backgrounds;
	}

	public void setBackgrounds(List<Fixture> backgrounds) {
		this.backgrounds = backgrounds;
	}

	public List<Fixture> getForegrounds() {
		return foregrounds;
	}

	public void setForegrounds(List<Fixture> foregrounds) {
		this.foregrounds = foregrounds;
	}*/

	/*public int getGroundCRC() {
		return groundCRC;
	}

	public void setGroundCRC(int groundCRC) {
		this.groundCRC = groundCRC;
	}

	public List<Layer> getLayers() {
		return layers;
	}

	public void setLayers(List<Layer> layers) {
		this.layers = layers;
	}*/

	public List<CellData> getCells() {
		return cells;
	}

	public void setCells(List<CellData> cells) {
		this.cells = cells;
	}
}
