package reader.d2p.map;

import org.apache.mina.core.buffer.IoBuffer;

import reader.d2p.map.element.GraphicalElement;
import reader.d2p.map.element.SoundElement;

public class Cell {
	/*private short cellId = 0;
	private List<GraphicalElement> graphElement = new ArrayList<GraphicalElement>();//2
	private List<SoundElement> soundElement = new ArrayList<SoundElement>();//33
	
	public void read(IoBuffer reader,int mapVersion) {
		cellId = reader.getShort();
		short elementCount = reader.getShort();
		for(int i = 0; i < elementCount; i++) {
			byte type = reader.get();
			switch(type) {
			case 2:
				GraphicalElement gElement = new GraphicalElement();
				gElement.read(reader, mapVersion);
				graphElement.add(gElement);
				break;
			case 33:
				SoundElement sElement = new SoundElement();
				sElement.read(reader, mapVersion);
				soundElement.add(sElement);
				break;
			}
		}
	}*/
	
	public void read(IoBuffer reader,int mapVersion) {
		reader.getShort();
		short elementCount = reader.getShort();
		for(int i = 0; i < elementCount; i++) {
			byte type = reader.get();
			switch(type) {
			case 2:
				GraphicalElement gElement = new GraphicalElement();
				gElement.read(reader, mapVersion);
				break;
			case 33:
				SoundElement sElement = new SoundElement();
				sElement.read(reader, mapVersion);
				break;
			}
		}
	}

	/*public short getCellId() {
		return cellId;
	}

	public void setCellId(short cellId) {
		this.cellId = cellId;
	}

	public List<GraphicalElement> getGraphElement() {
		return graphElement;
	}

	public void setGraphElement(List<GraphicalElement> graphElement) {
		this.graphElement = graphElement;
	}

	public List<SoundElement> getSoundElement() {
		return soundElement;
	}

	public void setSoundElement(List<SoundElement> soundElement) {
		this.soundElement = soundElement;
	}*/
}
