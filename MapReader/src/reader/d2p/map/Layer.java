package reader.d2p.map;

import org.apache.mina.core.buffer.IoBuffer;

public class Layer {
	/*private int layerId = 0;
	private List<Cell> cells = new ArrayList<Cell>();
	
	public void read(IoBuffer reader,int mapVersion) {
		layerId = reader.getInt();
		cellCount = reader.getShort();
		for(int i = 0; i < cellCount; i++) {
			Cell cell = new Cell();
			cell.read(reader,mapVersion);
			cells.add(cell);
		}
	}*/
	
	public void read(IoBuffer reader,int mapVersion) {
		reader.getInt();
		short cellCount = reader.getShort();
		for(int i = 0; i < cellCount; i++) {
			Cell cell = new Cell();
			cell.read(reader,mapVersion);
		}
	}

	/*public int getLayerId() {
		return layerId;
	}

	public void setLayerId(int layerId) {
		this.layerId = layerId;
	}

	public List<Cell> getCells() {
		return cells;
	}

	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}*/
}
