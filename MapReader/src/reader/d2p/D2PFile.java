package reader.d2p;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.mina.core.buffer.IoBuffer;

import reader.d2p.map.Map;

public class D2PFile {
	private IoBuffer reader;
	private List<CompressedMap> compMaps = new ArrayList<CompressedMap>();
	private List<Map> maps = new ArrayList<Map>();
	
	public D2PFile(File file) {
		if(!file.getName().contains(".d2p")) {
			try {
				throw new Exception("Not a d2p file");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		getReader(file);
		getContent();
		reader.free();
		reader = null;
		compMaps = null;
	}
	
	private void getReader(File file) {
		try {
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) fis.getChannel().size()];
			fis.read(data);
			fis.close();
			reader = IoBuffer.wrap(data);
			data = null;
			fis = null;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void getContent() {
		byte param1 = (byte) reader.getUnsigned();
		byte param2 = (byte) reader.getUnsigned();
		if(param1 != 2 || param2 != 1)
			try {
				throw new Exception("Can't read the map");
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		reader.position(reader.limit() - 24);
		int offsetPlus = reader.getInt();
		//int loc9 = reader.getInt();
		reader.getInt();
		int positionComp = reader.getInt();
		int countComp = reader.getInt();
		
		reader.position(positionComp);
		
		for(int i = 0; i < countComp; i++) {
			
			compMaps.add(new CompressedMap(reader,offsetPlus));
		}
		for(CompressedMap map : compMaps) {
			if(map.isValid())
			    maps.add(new Map(map));
			else
				continue;
		}
	}
	
	public List<Map> getMap() {
		return maps;
	}
}
