package reader.d2p;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import reader.d2p.map.Map;

public class D2PFolder {
	public List<Map> maps = new ArrayList<Map>();
	
	public D2PFolder(String filename) {
		File file = new File(filename);
		if(file.isDirectory()) {
			for(File child : file.listFiles()) {
				if(child.getName().contains("d2p")) {
					D2PFile d2p = new D2PFile(child);
					for(Map map : d2p.getMap()) {
						maps.add(map);
					}
				}
			}
		}
	}
}
