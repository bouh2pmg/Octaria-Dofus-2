package reader.d2p;

import org.apache.mina.core.buffer.IoBuffer;

public class CompressedMap {
	private IoBuffer reader;
	private String index = "";
	private long offset;
	private long byteCount;
	private boolean valid = true;
	
	public CompressedMap(IoBuffer raw, int offsetPlus) {
		reader = raw;
		readMapInfo(offsetPlus);
	}
	
	public void readMapInfo(int offsetPlus) {
		int lenghtInd = reader.getShort();
		for(int i = 0; i < lenghtInd; i++) {
			index += (char)reader.get();
		}
		if(index.contains("link")) {
			setValid(false);
			return;
		}
		setOffset(reader.getUnsignedInt() + offsetPlus);
		setByteCount(reader.getUnsignedInt());
	}
	
	public int getMapId() {
		return Integer.parseInt(index.substring(index.indexOf("/")).replace(".dlm", ""));
	}

	public long getByteCount() {
		return byteCount;
	}

	public void setByteCount(long byteCount) {
		this.byteCount = byteCount;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}
	
	public String getIndex() {
		return index;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public IoBuffer getRaw() {
		return reader;
	}
	
	public void setRaw(IoBuffer buffer) {
		this.reader = buffer;
	}
}
