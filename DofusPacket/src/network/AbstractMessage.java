package network;

import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;

import org.apache.mina.core.buffer.IoBuffer;

public abstract class AbstractMessage {
	public abstract void doPacket();
	
	private IoBuffer packetIn = IoBuffer.allocate(4096);
	private IoBuffer packetOut = IoBuffer.allocate(4096);
	private Header header;
	
	public AbstractMessage(Header header) {
		setHeader(header);
		setPacketIn(header.getData());
	}
	
	public void writeUTF(String texte) {
		try {
			packetOut.putPrefixedString(texte, Charset.forName("ISO-8859-1").newEncoder());
		} catch (CharacterCodingException e) {
			e.printStackTrace();
		}
	}
	
	public String readUTF() {
		String texte = "";
		try {
			texte = packetIn.getPrefixedString(Charset.forName("ISO-8859-1").newDecoder());
		} catch (CharacterCodingException e) {
			e.printStackTrace();
		}
		return texte;
	}
	public void writeNoPrefixedUTF(String texte) {
		getPacketOut().putUnsignedShort(texte.length());
		getPacketOut().put(texte.getBytes());
	}
	
	public String readNoPrefixedUTF() {
		StringBuilder utf = new StringBuilder();
		int length = getPacketIn().getUnsignedShort();
		for(int i = 0; i < length; i++) {
			utf.append((char)getPacketIn().get());
		}
		return utf.toString();
	}

	public IoBuffer getPacketIn() {
		return packetIn;
	}

	public void setPacketIn(IoBuffer packetIn) {
		this.packetIn = packetIn;
	}

	public IoBuffer getPacketOut() {
		return packetOut;
	}

	public void setPacketOut(IoBuffer packetOut) {
		this.packetOut = packetOut;
	}

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}
}

