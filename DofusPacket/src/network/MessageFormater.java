package network;

import org.apache.mina.core.buffer.IoBuffer;

public class MessageFormater {
	private IoBuffer toSend = IoBuffer.allocate(2048);
	
	public MessageFormater(AbstractMessage packet) {
		format(packet);
	}
	
	public void format(AbstractMessage packet) {
		int compute = compute(packet.getPacketOut());
		getToSend().putUnsignedShort((packet.getHeader().getID().getID() << 2 | compute));
		switch(compute) {
			case 1: getToSend().put((byte)packet.getPacketOut().limit()); break;
			case 2: getToSend().putUnsignedShort(packet.getPacketOut().limit()); break;
			case 3: getToSend().put((byte)(packet.getPacketOut().limit() >> 16 & 255));
					getToSend().putUnsignedShort(packet.getPacketOut().limit()); break;
		}
		getToSend().put(packet.getPacketOut());
	}
	
	private int compute(IoBuffer packet) {
        if (packet.limit() > 65535) { return 3; }
        if (packet.limit() > 255) { return 2; }
        if (packet.limit() > 0) { return 1; }
        return 0;
    }

	public IoBuffer getToSend() {
		return toSend;
	}

	public void setToSend(IoBuffer toSend) {
		this.toSend = toSend;
	}
}

