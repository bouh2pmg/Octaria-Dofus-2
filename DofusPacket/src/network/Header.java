package network;

import org.apache.mina.core.buffer.IoBuffer;

public class Header {
	private MessageID ID = null;
	private int lenType = 0;
	private int packetLenght = 0;
	private IoBuffer data = IoBuffer.allocate(2048);
	
	public Header(IoBuffer packet) {
		setData(packet);
		unpackHeader();
	}
	
	public Header(MessageID ID) {
		setID(ID);
	}
	
	private void unpackHeader() {
		int hiHeader = getData().getUnsignedShort();
		setID(MessageID.getPacket((hiHeader >> 2)));
		setLenType((hiHeader & 3));
		getLength();
	}
	
	private void getLength() {
		switch (lenType) {
            case 1: packetLenght = getData().getUnsigned(); break;
            case 2: packetLenght = getData().getUnsignedShort(); break;
            case 3: packetLenght = ((getData().getUnsigned() & 255) >> 16) + ((getData().getUnsigned() & 255) >> 8) + (getData().getUnsigned() & 255); break;
        }
	}
	
	@Override
	public String toString() {
		StringBuilder string = new StringBuilder();
		string.append("ID : ").append(ID)
		.append("\n")
		.append("Lenght : ").append(packetLenght);
		return string.toString();
	}

	public MessageID getID() {
		return ID;
	}

	public void setID(MessageID iD) {
		ID = iD;
	}

	public IoBuffer getData() {
		return data;
	}

	public void setData(IoBuffer data) {
		this.data = data;
	}

	public int getLenType() {
		return lenType;
	}

	public void setLenType(int lenType) {
		this.lenType = lenType;
	}

	public int getPacketLenght() {
		return packetLenght;
	}

	public void setPacketLenght(int packetLenght) {
		this.packetLenght = packetLenght;
	}
}

