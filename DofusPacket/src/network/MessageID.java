package network;

public enum MessageID {
    /* LOGIN */
    MSG_PROTOCOL_REQUIRED(1),
    MSG_HELLO_CONNECT(3),
    MSG_IDENTIFICATION(4),
    MSG_E_BAD_VERSION(21),
    MSG_IDENTIFICATION_FAILED(20),
    MSG_IDENTIFICATION_SUCCESS(22),
    MSG_IDENTIFICATION_SUCCESS_LOGIN_TOKEN(6209),
    MSG_SERVERLIST(30),
    MSG_SERVER_SELECTED(40),
    MSG_SERVER_DATA(42),
    MSG_E_FAILED_BANNED(6174),

    /* GAME */
    MSG_CONSOLE_SERVER(75),
    MSG_CONSOLE(76),
    MSG_HELLO_GAME(101),
    MSG_AUTH_TICKET(110),
    MSG_AUTH_SUCCESS(111),
    MSG_PERSO_R_LIST(150),
    MSG_PERSO_LIST(151),
    MSG_PERSO_SELECT(152),
    MSG_PERSO_SELECT_SUCCESS(153),
    MSG_PERSO_CREATION_REQUEST(160),
    MSG_PERSO_CREATION_RESULT(161),
    MSG_PERSO_NAME_SUGGESTION(162),
    MSG_PERSO_NAME_SUGGESTION_FAILURE(164),
    MSG_PERSO_DELETION_REQUEST(165),
    MSG_PERSO_DELETION_ERROR(166),
    MSG_BASIC_TIME(175),
    MSG_BASIC_NO_OPERATION(176),
    MSG_BASIC_PING(182),
    MSG_BASIC_PONG(183),
    MSG_SYSTEM_MESSAGE_DISPLAY_MESSAGE(189),
    MSG_GAME_CONTEXT_CREATE(200),
    MSG_GAME_CONTEXT_DESTROY(201),
    MSG_CURRENT_MAP(220),
    MSG_CHANGE_MAP(221),
    MSG_MAP_INFO_REQUEST(225),
    MSG_MAP_COMP_INFO(226),
    MSG_GAME_CONTEXT_CREATE_R(250),
    MSG_MAP_REM_ELEMENT(251),
    MSG_PERSO_STATS_LIST(500),
    MSG_FIGHT_STARTING(700),
    MSG_FIGHT_JOIN(702),
    MSG_FIGHT_PLACEMENT_POSSIBLE_POSITION(703),
    MSG_TEXT_INFORMATION(780),
    MSG_CHAT_CLIENT_PRIVATE_MESSAGE(851),
    MSG_CHAT_MESSAGE(861),
    MSG_CHAT_ERROR_MESSAGE(870),
    MSG_CHAT_SERVER_MESSAGE(881),
    MSG_CHAT_SERVER_COPY_MESSAGE(882),
    MSG_CHAT_ENABLE_CHANNEL(891),
    MSG_MAP_MOVEMENT_REQUEST(950),
    MSG_MAP_MOVEMENT(951),
    MSG_MAP_MOVEMENT_CONFIRM(952),
    MSG_SPELL_LIST(1200),
    MSG_INVENTORY_WEIGHT(3009),
    MSG_INVENTORY_CONTENT(3016),
    MSG_FRIEND_R_LIST(4001),
    MSG_FRIEND_LIST(4002),
    MSG_PERSO_NAME_SUGGESTION_SUCCESS(5544),
    MSG_GUILD_INFO_GENERAL(5557),
    MSG_GUILD_INFO_MEMBERS(5558),
    MSG_SECURITY_CLIENT_KEY(5607),
    MSG_STATS_UPGRADE_RESULT(5609),
    MSG_STATS_UPGRADE_R(5610),
    MSG_QUEST_LIST_REQUEST(5623),
    MSG_QUEST_LIST(5626),
    MSG_SHOW_ACTOR(5632),
    MSG_IGNORED_LIST(5674),
    MSG_IGRORED_R_LIST(5676),
    MSG_ACCOUNT_CAPABILITIES(6216),
    MSG_PERSO_LIST_WITH_MODIFICATION(6120),
    MSG_SERVER_OPTIONAL_FEATURES(6305),
    MSG_PERSO_LEVELUP_MESSAGE(5670);




    private int ID;

    private MessageID(int ID) {
	setID(ID);
    }

    public static MessageID getPacket(int id) 
    {
	switch(id)
	{
	case 1:
	    return MSG_PROTOCOL_REQUIRED;

	case 3:
	    return MSG_HELLO_CONNECT;

	case 4:
	    return MSG_IDENTIFICATION;

	case 20:
	    return MSG_IDENTIFICATION_FAILED;

	case 21:
	    return MSG_E_BAD_VERSION;

	case 22:
	    return MSG_IDENTIFICATION_SUCCESS;

	case 30:
	    return MSG_SERVERLIST;

	case 40:
	    return MSG_SERVER_SELECTED;

	case 42:
	    return MSG_SERVER_DATA;

	case 75:
	    return MSG_CONSOLE_SERVER;

	case 76:
	    return MSG_CONSOLE;

	case 101:
	    return MSG_HELLO_GAME;

	case 110:
	    return MSG_AUTH_TICKET;

	case 111:
	    return MSG_AUTH_SUCCESS;

	case 150:
	    return MSG_PERSO_R_LIST;

	case 151:
	    return MSG_PERSO_LIST;

	case 152:
	    return MSG_PERSO_SELECT;

	case 153:
	    return MSG_PERSO_SELECT_SUCCESS; 

	case 160:
	    return MSG_PERSO_CREATION_REQUEST;

	case 161:
	    return MSG_PERSO_CREATION_RESULT;

	case 162:
	    return MSG_PERSO_NAME_SUGGESTION;

	case 164:
	    return MSG_PERSO_NAME_SUGGESTION_FAILURE;

	case 165:
	    return MSG_PERSO_DELETION_REQUEST;

	case 166:
	    return MSG_PERSO_DELETION_ERROR;

	case 175:
	    return MSG_BASIC_TIME;

	case 176:
	    return MSG_BASIC_NO_OPERATION;

	case 182:
	    return MSG_BASIC_PING;

	case 183:
	    return  MSG_BASIC_PONG;

	case 189:
	    return MSG_SYSTEM_MESSAGE_DISPLAY_MESSAGE;

	case 200:
	    return MSG_GAME_CONTEXT_CREATE;

	case 201:
	    return MSG_GAME_CONTEXT_DESTROY;

	case 220:
	    return MSG_CURRENT_MAP;

	case 221:
	    return MSG_CHANGE_MAP;

	case 225:
	    return MSG_MAP_INFO_REQUEST;

	case 226:
	    return MSG_MAP_COMP_INFO;

	case 250:
	    return MSG_GAME_CONTEXT_CREATE_R;	

	case 251:
	    return MSG_MAP_REM_ELEMENT;

	case 500:
	    return MSG_PERSO_STATS_LIST;

	case 700:
	    return MSG_FIGHT_STARTING;

	case 702:
	    return MSG_FIGHT_JOIN;

	case 703:
	    return MSG_FIGHT_PLACEMENT_POSSIBLE_POSITION;

	case 780:
	    return MSG_TEXT_INFORMATION;
	    
	case 851:
	    return MSG_CHAT_CLIENT_PRIVATE_MESSAGE;
	    
	case 861:
	    return MSG_CHAT_MESSAGE;

	case 870:
	    return MSG_CHAT_ERROR_MESSAGE;

	case 881:
	    return MSG_CHAT_SERVER_MESSAGE;
	    
	case 882:
	    return  MSG_CHAT_SERVER_COPY_MESSAGE;
	    
	case 891:
	    return MSG_CHAT_ENABLE_CHANNEL;

	case 950:
	    return MSG_MAP_MOVEMENT_REQUEST;

	case 951:
	    return MSG_MAP_MOVEMENT;

	case 952:
	    return MSG_MAP_MOVEMENT_CONFIRM;

	case 1200:
	    return MSG_SPELL_LIST;

	case 3009:
	    return MSG_INVENTORY_WEIGHT;

	case 3016:
	    return MSG_INVENTORY_CONTENT;

	case 4001:
	    return MSG_FRIEND_R_LIST;

	case 4002:
	    return MSG_FRIEND_LIST;

	case 5544:
	    return MSG_PERSO_NAME_SUGGESTION_SUCCESS;    
	    
	case 5557:
	    return MSG_GUILD_INFO_GENERAL;
	    
	case 5558:
	    return MSG_GUILD_INFO_MEMBERS;
	    
	case 5607:
	    return MSG_SECURITY_CLIENT_KEY;

	case 5609:
	    return MSG_STATS_UPGRADE_RESULT;

	case 5610:
	    return MSG_STATS_UPGRADE_R;

	case 5623:
	    return MSG_QUEST_LIST_REQUEST;

	case 5626:
	    return MSG_QUEST_LIST;

	case 5632:
	    return MSG_SHOW_ACTOR;

	case 5674:
	    return MSG_IGNORED_LIST;

	case 5676:
	    return MSG_IGRORED_R_LIST;

	case 6216:
	    return MSG_ACCOUNT_CAPABILITIES;

	case 6120:
	    return MSG_PERSO_LIST_WITH_MODIFICATION;

	case 6174:
	    return MSG_E_FAILED_BANNED;
	    
	case 6209:
	    return MSG_IDENTIFICATION_SUCCESS_LOGIN_TOKEN;

	case 6305:
	    return MSG_SERVER_OPTIONAL_FEATURES;

	case 5670:
	    return MSG_PERSO_LEVELUP_MESSAGE;

	default:
	    System.err.println("Error > Packet unknow(" + id + ")");
	    return null;
	}

    }

    public int getID() {
	return ID;
    }

    public void setID(int ID) {
	this.ID = ID;
    }
}

