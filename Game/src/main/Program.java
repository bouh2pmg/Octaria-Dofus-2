package main;

import org.slf4j.bridge.SLF4JBridgeHandler;
import server.game.GameServer;
import server.inter.InterServer;
import utils.GameConfig;
import utils.SqlUtils;
import utils.Utils;

public class Program
{
	public static void main(String[] args) throws Exception 
	{
		SLF4JBridgeHandler.install();
		GameConfig.readConfig("config.xml");
		SqlUtils.initContext();
		Utils.loadStaticData();
		GameServer.getInstance().start();
		InterServer.getInstance().start();
		System.out.println("OctoriaG was start !");
	}
}
