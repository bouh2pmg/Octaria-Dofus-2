package server.game.client;

import network.AbstractMessage;
import network.Header;
import network.MessageFormater;
import network.MessageID;
import network.game.message.game.appraoch.HelloGame;
import org.apache.mina.core.session.IoSession;
import utils.SqlUtils;
import utils.Utils;
import data.sql.entity.Account;
import data.sql.entity.Player;

public class GameClient {
    private IoSession session;
    private GameParser parser;
    private Account acc = null;
    private Player player = null;
    private String key = "";

    public GameClient(IoSession session) {
	setSession(session);
	setParser(new GameParser(this));
	new HelloGame(new Header(MessageID.MSG_HELLO_GAME),this);
    }

    public void send(AbstractMessage packet) 
    {
	packet.getPacketOut().flip();
	MessageFormater mf = new MessageFormater(packet);
	session.write(mf.getToSend().flip());
    }
    public IoSession getSession() {
	return session;
    }

    public void setSession(IoSession session) {
	this.session = session;
    }

    public GameParser getParser() {
	return parser;
    }

    public void setParser(GameParser parser) {
	this.parser = parser;
    }

    public Account getAcc() {
	return acc;
    }

    public void setAcc(Account acc) {
	this.acc = acc;
    }

    public Player getPlayer() {
	return player;
    }

    public void setPlayer(Player player) {
	this.player = player;
    }

    public String getKey() {
	return key;
    }

    public void setKey(String key) {
	this.key = key;
    }

    public void exit()
    {
	if(player != null) // Si il avais un personnage de connecter (Cr�ation de perso par Exemple.)
	{
        	if(Utils.maps.get(player.getActualMap()) != null) // Si il �tait sur une map.
        	{
        	    Utils.maps.get(player.getActualMap()).removePlayerOnMap(player); // On le remove de la map.
        	}
	}
	SqlUtils.getAccountService().save(acc);
	for(Player player : acc.getCharacters())
	{
	    SqlUtils.getPlayerService().save(player);
	}
    }
}
