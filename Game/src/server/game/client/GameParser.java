package server.game.client;

import server.game.GameServer;
import network.Header;
import network.MessageID;
import network.game.message.authorized.Console;
import network.game.message.game.appraoch.AuthenticationTicket;
import network.game.message.game.character.CharacterList;
import network.game.message.game.character.CharacterSelected;
import network.game.message.game.character.creation.CharacterCreationRequest;
import network.game.message.game.character.creation.CharacterNameSuggestionRequest;
import network.game.message.game.character.deletion.CharacterDeletionRequest;
import network.game.message.game.chat.ChatClientPrivate;
import network.game.message.game.chat.ChatMessage;
import network.game.message.game.context.GameContext;
import network.game.message.game.context.GameContext.Context;
import network.game.message.game.context.map.MapMovementConfirm;
import network.game.message.game.context.map.MapMovementRequest;
import network.game.message.game.context.roleplay.ChangeMap;
import network.game.message.game.context.roleplay.MapInformationsRequest;
import network.game.message.game.context.roleplay.quest.QuestListRequest;
import network.game.message.game.context.roleplay.stats.StatsUpgradeRequest;
import network.game.message.game.friend.FriendList;
import network.game.message.game.friend.IgnoredList;
import network.game.message.security.ClientKey;
import network.game.message.server.basic.BasicNoOperation;
import network.game.message.server.basic.BasicPing;

public class GameParser {
	private GameClient client;
	
	public GameParser(GameClient client) {
		setClient(client);
	}
	
	public void parse(Header header) {
		if(header.getID() == null) {
			return;
		}else{
			GameServer.getLogger().info("Game << " + header.toString());
		}
		switch(header.getID()) {
		case MSG_AUTH_TICKET:
			new AuthenticationTicket(header,client);
			break;
		case MSG_PERSO_R_LIST:
			new CharacterList(new Header(MessageID.MSG_PERSO_LIST),client);
			break;
		case MSG_PERSO_SELECT:
			new CharacterSelected(header,client);
			break;
		case MSG_GAME_CONTEXT_CREATE_R:
			new GameContext(new Header(MessageID.MSG_GAME_CONTEXT_CREATE),client,Context.ROLEPLAY);
			break;
		case MSG_FRIEND_R_LIST:
			new FriendList(new Header(MessageID.MSG_FRIEND_LIST),client);
			break;
		case MSG_SECURITY_CLIENT_KEY:
			new ClientKey(header,client);
			break;
		case MSG_IGRORED_R_LIST:
			new IgnoredList(new Header(MessageID.MSG_IGNORED_LIST),client);
			break;
		case MSG_CHAT_MESSAGE:
			new ChatMessage(header,client);
			break;
		case MSG_MAP_INFO_REQUEST:
			new MapInformationsRequest(header,client);
			break;
		case MSG_PERSO_CREATION_REQUEST:
			new CharacterCreationRequest(header,client);
			break;
		case MSG_PERSO_DELETION_REQUEST:
			new CharacterDeletionRequest(header,client);
			break;
		case MSG_PERSO_NAME_SUGGESTION:
			new CharacterNameSuggestionRequest(header,client);
			break;
		case MSG_QUEST_LIST_REQUEST:
			new QuestListRequest(header,client); 
			break;
		case MSG_BASIC_PING:
			new BasicPing(header,client);
			break;
		case MSG_MAP_MOVEMENT_REQUEST: 
			new MapMovementRequest(header,client);
			break;
		case MSG_MAP_MOVEMENT_CONFIRM:
			new MapMovementConfirm(header,client);
			break;
		case MSG_CHANGE_MAP:
			new ChangeMap(header,client);
			break;
		case MSG_STATS_UPGRADE_R:
			new StatsUpgradeRequest(header,client);
			break;
		case MSG_CONSOLE:
			new Console(header,client);
			break;
		case MSG_CHAT_CLIENT_PRIVATE_MESSAGE:
		    	new ChatClientPrivate(header,client);
		    	break;
		default:
			new BasicNoOperation(new Header(MessageID.MSG_BASIC_NO_OPERATION),client);
			break;
		}
		
	}

	public GameClient getClient() {
		return client;
	}

	public void setClient(GameClient client) {
		this.client = client;
	}
}
