package server.game;

import network.Header;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import server.game.client.GameClient;

public class GameHandler extends IoHandlerAdapter {
	@Override
    public void sessionCreated(IoSession session) throws Exception {
		GameServer.getLogger().info("New connection : " + session.getRemoteAddress());
		session.setAttribute("client", new GameClient(session));
    }
	
	@Override
    public void sessionClosed(IoSession session) throws Exception {
		GameServer.getLogger().info("Deconnection : " + session.getRemoteAddress());
		GameClient client = (GameClient) session.getAttribute("client");
		client.exit();
		if(session.isConnected())
			session.close(true);
    }
	
	@Override
	public void exceptionCaught(IoSession session, Throwable cause) {
		cause.printStackTrace();
	}
	
	@Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		GameServer.getLogger().info("Idle : " + session.getRemoteAddress() + " ->" + session.getIdleCount(status));
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
    	if(message instanceof IoBuffer) {
    		IoBuffer packet = IoBuffer.allocate(((IoBuffer)message).capacity());
    		packet.put((IoBuffer)message);
    		packet.flip();
    		Header header = new Header(packet);
    		if(session.getAttribute("client") instanceof GameClient) {
    			GameClient client = (GameClient) session.getAttribute("client");
    			try{
    				client.getParser().parse(header);
    			}catch(Exception e) {
    				GameServer.getLogger().error(e.getLocalizedMessage());
    			}
    		} 
    		packet.free();
    	}
    }
    
    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
    	if(message instanceof IoBuffer) {
    		IoBuffer packet = IoBuffer.allocate(((IoBuffer)message).capacity());
    		packet.put((IoBuffer)message);
    		packet.flip();
    		Header header = new Header(packet);
    		GameServer.getLogger().info("Game >> " + header.toString());
    		packet.free();
    	}
    }
}
