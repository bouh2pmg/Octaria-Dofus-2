package server.game;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.sql.entity.Player;

import utils.GameConfig;

public class GameServer {
	private NioSocketAcceptor acceptor;
	private Map<String,Integer> waitingAcc = new TreeMap<String,Integer>();
	private static GameServer instance;
	private static Logger logger = LoggerFactory.getLogger(GameServer.class);
	private int isOnline = 0;
	public static ArrayList<Player> Persos = new ArrayList<Player>();
	
	private GameServer() {
		setAcceptor(new NioSocketAcceptor());
		getAcceptor().getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, GameConfig.TIME_OUT);
		getAcceptor().setHandler(new GameHandler());
	}
	
	public static GameServer getInstance() {
		if(instance == null){
			instance = new GameServer();
		}
		return instance;
	}
	public boolean isOnline()
	{
		if(this.isOnline == 1)
			return true;
		else
			return false;
	}
	public void start() {
		try {
			getAcceptor().bind(new InetSocketAddress(GameConfig.GAME_IP, GameConfig.GAME_PORT));
			this.isOnline = 1;
		} catch (IOException e) {
			
			logger.error("Can't start Game server !");
			this.isOnline = 0;
		}
	}
	
	public void stop() {
		getAcceptor().unbind();
		getAcceptor().dispose();
		this.isOnline = 0;
	}

	public NioSocketAcceptor getAcceptor() {
		return acceptor;
	}

	public void setAcceptor(NioSocketAcceptor acceptor) {
		this.acceptor = acceptor;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		GameServer.logger = logger;
	}
	
	public void addAccWaiting(String ticket,int id) {
		waitingAcc.put(ticket, id);
	}
	
	public int getAccWaiting(String ticket) {
		return waitingAcc.get(ticket);
	}
	
	public void removeAccWaiting(String ticket) {
		waitingAcc.remove(ticket);
	}
	
	public boolean containAccWaiting(String ticket) {
		if(waitingAcc.containsKey(ticket)) {
			return true;
		}else{
			return false;
		}
	}

	public static ArrayList<Player> getPersos() {
		return Persos;
	}

	public static void setPersos(ArrayList<Player> persos) {
		Persos = persos;
	}
}
