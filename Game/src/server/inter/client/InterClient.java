package server.inter.client;

import network.inter.InterPacket;
import network.inter.message.connection.Information;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

public class InterClient {
	public static InterClient instance = null;
	
	private IoSession session;
	private InterParser parser;
	
	private byte status = 0;
	private byte completion = 0;//?
	private boolean isSelectable = false;
	
	public InterClient(IoSession session) {
		setSession(session);
		setParser(new InterParser(this));
		setCompletion((byte)1);
		//Load by database?
	}
	public void refreshStat()
	{
		new Information(this);
	}
	public void send(InterPacket packet) {
		session.write(packet.packetOut.flip());
	}
	
	
	public void toParse(IoBuffer packet) {
		parser.parse(packet);
	}

	public IoSession getSession() {
		return session;
	}

	public void setSession(IoSession session) {
		this.session = session;
	}

	public InterParser getParser() {
		return parser;
	}

	public void setParser(InterParser parser) {
		this.parser = parser;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) 
	{
		this.status = status;
		this.refreshStat();
	}

	public byte getCompletion() {
		return completion;
	}

	public void setCompletion(byte completion) {
		this.completion = completion;
	}

	public boolean isSelectable() {
		return isSelectable;
	}

	public void setSelectable(boolean isSelectable) {
		this.isSelectable = isSelectable;
	}
}
