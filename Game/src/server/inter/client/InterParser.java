package server.inter.client;

import network.inter.InterPacketID;
import network.inter.message.account.AccountWaiting;

import org.apache.mina.core.buffer.IoBuffer;

import server.inter.InterServer;

public class InterParser {
	private InterClient client;
	private InterPacketID id;
	public InterParser(InterClient client) {
		setClient(client);
	}
	
	public void parse(IoBuffer buffer) {
		id = InterPacketID.getPacket(buffer.get());
		InterServer.getLogger().info("Inter << " + id);
		switch(id) {
		case CONNECT:
			InterClient.instance.setStatus((byte)3);
			break;
		case INFO_AGREED:
			InterClient.instance.setSelectable(true);
			break;
		case ACC_WAITING:
			new AccountWaiting(InterClient.instance,buffer);
			break;
		default:
			break;
		}
	}

	public InterClient getClient() {
		return client;
	}

	public void setClient(InterClient client) {
		this.client = client;
	}
}
