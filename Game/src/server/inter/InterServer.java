package server.inter;

import java.net.InetSocketAddress;

import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.GameConfig;

public class InterServer {
	private NioSocketConnector connector;
	private static InterServer instance;
	private static Logger logger = LoggerFactory.getLogger(InterServer.class);
	private int isOnline = 0;

	private InterServer() {
		setConnector(new NioSocketConnector());
		getConnector().setHandler(new InterHandler());
	}
	
	public static InterServer getInstance() {
		if (instance == null) {
			instance = new InterServer();
		}
		return instance;
	}
	public boolean isOnline()
	{
		if(this.isOnline == 1)
			return true;
		else
			return false;
	}
	public void start() 
	{
		try
		{
			getConnector().connect(new InetSocketAddress(GameConfig.INTER_IP, GameConfig.INTER_PORT));
			this.isOnline = 1;
		}
		catch(Exception e)
		{
			logger.error("Can't connect to the Realm'Server !");
			this.isOnline = 0;
		}
		
	}
	
	public void stop() {
		getConnector().dispose();
	}
	
	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		InterServer.logger = logger;
	}

	public NioSocketConnector getConnector() {
		return connector;
	}

	public void setConnector(NioSocketConnector connector) {
		this.connector = connector;
	}
}
