package server.inter;

import network.inter.InterPacketID;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import server.inter.client.InterClient;

public class InterHandler extends IoHandlerAdapter {
	@Override
    public void sessionCreated(IoSession session) throws Exception {
		InterClient.instance = new InterClient(session);
    }
	
	@Override
    public void sessionClosed(IoSession session) throws Exception {
		session.close(true);
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
    	if(message != null && message instanceof IoBuffer) {
    		IoBuffer packet = IoBuffer.allocate(((IoBuffer)message).capacity());
    		packet.put((IoBuffer)message);
    		packet.flip();
    		InterClient.instance.toParse(packet);
    		packet.free();
    	}
    }
    
    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
    	if(message != null && message instanceof IoBuffer) {
    		IoBuffer packet = IoBuffer.allocate(((IoBuffer)message).capacity());
    		packet.put((IoBuffer)message);
    		packet.flip();
    		InterServer.getLogger().info("Inter >> " + InterPacketID.getPacket(packet.get()));
    		packet.free();
    	}
    }
}
