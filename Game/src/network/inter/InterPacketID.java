package network.inter;

public enum InterPacketID {
	CONNECT((byte)1),
	INFO((byte)2),
	INFO_AGREED((byte)3),
	ACC_WAITING((byte)4);
	
	private byte id = 0;
	
	private InterPacketID(byte id){
		setId(id);
	}
	
	public static InterPacketID getPacket(byte id){
		if(id == 1) {
			return CONNECT;
		}
		if(id == 2) {
			return INFO;
		}
		else if(id == 3) {
			return INFO_AGREED;
		}
		else if(id == 4) {
			return ACC_WAITING;
		}
		return null;
	}

	public byte getId() {
		return id;
	}

	public void setId(byte id) {
		this.id = id;
	}
}
