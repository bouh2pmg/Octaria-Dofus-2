package network.inter;

import org.apache.mina.core.buffer.IoBuffer;

import server.inter.client.InterClient;

public abstract class InterPacket {
	public abstract void unpack(IoBuffer buffer);
	public abstract void pack(IoBuffer buffer);
	public abstract void doPacket(IoBuffer buffer);
	public InterClient client = null;
	public IoBuffer packetOut = IoBuffer.allocate(2048);
	
	public InterPacket(InterClient client, IoBuffer packet){//Received
		setClient(client);
	}
	public InterPacket(InterClient client){//To send
		this.client = client;
	}
	
	private void setClient(InterClient client){
		this.client = client;
	}
	
	public void writeString(String message){
		packetOut.putInt(message.length());
		packetOut.put(message.getBytes());
	}
	
	public String readString(IoBuffer buffer){
		int length = buffer.getInt();
		StringBuilder string = new StringBuilder();
		for(int i = 0; i < length; i++){
			char c = (char)buffer.get();
			string.append(c);
		}
		return string.toString();
	}
}
