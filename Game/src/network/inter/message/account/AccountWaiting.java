package network.inter.message.account;

import org.apache.mina.core.buffer.IoBuffer;

import server.game.GameServer;
import server.inter.client.InterClient;
import network.inter.InterPacket;

public class AccountWaiting extends InterPacket {
	public String ticket = "";
	public int accId = 0;

	public AccountWaiting(InterClient client, IoBuffer packet) {
		super(client);
		unpack(packet);
		doPacket(null);
	}

	@Override
	public void unpack(IoBuffer buffer) {
		ticket = readString(buffer);
		accId = buffer.getInt();
	}

	@Override
	public void pack(IoBuffer buffer) {
	}

	@Override
	public void doPacket(IoBuffer buffer) {
		GameServer.getInstance().addAccWaiting(ticket, accId);
	}

}
