package network.inter.message.connection;

import org.apache.mina.core.buffer.IoBuffer;

import server.inter.client.InterClient;
import utils.GameConfig;
import network.inter.InterPacket;
import network.inter.InterPacketID;

public class Information extends InterPacket {
	public Information(InterClient client) {
		super(client);
		pack(null);
		doPacket(null);
	}

	@Override
	public void unpack(IoBuffer buffer) {
	}

	@Override
	public void pack(IoBuffer buffer) {
		packetOut.put(InterPacketID.INFO.getId());
		packetOut.put(Byte.parseByte(System.getProperty("ID")));
		packetOut.put(client.getCompletion());
		writeString(GameConfig.GAME_IP);
		packetOut.putShort((short) GameConfig.GAME_PORT);
		packetOut.putInt(InterClient.instance.getStatus());
	}

	@Override
	public void doPacket(IoBuffer buffer) {
		client.send(this);
	}
}
