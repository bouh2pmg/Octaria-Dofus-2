package network.game.message.security;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;
import network.game.message.game.character.spells.SpellsList;
import network.game.message.game.character.stats.CharacterStatsList;
import network.game.message.game.chat.ChannelEnablingChange;
import network.game.message.game.context.GameContextDestroy;
import network.game.message.game.inventory.InventoryContent;
import network.game.message.game.inventory.InventoryWeight;

public class ClientKey extends AbstractMessage implements MessageReceived {
	private GameClient client = null;

	public ClientKey(Header header, GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() {
		client.setKey(readUTF());
	}

	@Override
	public void doPacket() 
	{
		new GameContextDestroy(new Header(MessageID.MSG_GAME_CONTEXT_DESTROY),client);
		new InventoryContent(new Header(MessageID.MSG_INVENTORY_CONTENT),client);
		new InventoryWeight(new Header(MessageID.MSG_INVENTORY_WEIGHT),client);
		new CharacterStatsList(new Header(MessageID.MSG_PERSO_STATS_LIST),client);
		new ChannelEnablingChange(new Header(MessageID.MSG_CHAT_ENABLE_CHANNEL),client,(byte) 0,true);
		new SpellsList(new Header(MessageID.MSG_SPELL_LIST),client,false);
		client.getPlayer().onJoinGame();
	}
}