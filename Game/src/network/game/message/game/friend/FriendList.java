package network.game.message.game.friend;

import data.sql.entity.Account;
import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class FriendList extends AbstractMessage implements MessageToSend {
	private GameClient client = null;

	public FriendList(Header header, GameClient client) {
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putShort((short)client.getAcc().getFriends().size());
		for(Account acc : client.getAcc().getFriends()) {
			getPacketOut().putUnsignedShort(78);//TypeId
			getPacketOut().put(acc.toFriendInfo());
		}
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}
