package network.game.message.game.friend;

import data.sql.entity.Account;
import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class IgnoredList extends AbstractMessage implements MessageToSend {
	private GameClient client = null;

	public IgnoredList(Header header, GameClient client) {
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putShort((short)client.getAcc().getIgnored().size());
		for(Account acc : client.getAcc().getIgnored()) {
			getPacketOut().putUnsignedShort(106);//TypeId
			getPacketOut().put(acc.toIgnoredInfo());
		}
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
