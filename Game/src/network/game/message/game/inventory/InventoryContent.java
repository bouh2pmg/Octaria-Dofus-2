package network.game.message.game.inventory;

import data.sql.entity.Item;
import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class InventoryContent extends AbstractMessage implements MessageToSend {
	private GameClient client;
	
	public InventoryContent(Header header,GameClient client) {
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack()
	{
		getPacketOut().putShort((short) client.getPlayer().getInventory().size());
		for(Item item : client.getPlayer().getInventory()) 
		{
			getPacketOut().put(item.toObjectInformation());
		}
		getPacketOut().putInt(client.getPlayer().getKama());
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
