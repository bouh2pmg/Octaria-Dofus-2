package network.game.message.game.inventory;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class InventoryWeight extends AbstractMessage implements MessageToSend {
	private GameClient client;

	public InventoryWeight(Header header,GameClient client) {
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putInt(client.getPlayer().getPods());
		getPacketOut().putInt(client.getPlayer().getPodsMax());
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
