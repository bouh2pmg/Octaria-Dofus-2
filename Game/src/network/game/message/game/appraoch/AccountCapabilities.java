package network.game.message.game.appraoch;

import java.util.BitSet;

import server.game.client.GameClient;
import utils.BitsUtils;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;
public class AccountCapabilities extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private boolean tutorial = true;

	public AccountCapabilities(GameClient client) 
	{
		super(new Header(MessageID.MSG_ACCOUNT_CAPABILITIES));
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().putInt(client.getAcc().getGuid());
		getPacketOut().put((byte) ((tutorial) ? 1 : 0));//Tutorial available
		BitSet bits = new BitSet(15);
		for(int i = 0; i < 15; i++) {
			bits.set(i, true);
		}
		int b = BitsUtils.bitSetToInt(bits);
		getPacketOut().putUnsignedShort(b);
		getPacketOut().putUnsignedShort(b);
		getPacketOut().put((byte) 1);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}
