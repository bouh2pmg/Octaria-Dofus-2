package network.game.message.game.appraoch;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;
public class ServerOptionalFeatures extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private short[] args;

	public ServerOptionalFeatures(GameClient client,short[] args) {
		super(new Header(MessageID.MSG_SERVER_OPTIONAL_FEATURES));
		this.client = client; 
		this.args = args;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().putShort((short) this.args.length);
		if(this.args.length > 0)
		{
			for(short args : this.args)
			{
				getPacketOut().putShort(args);
			}
		}
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}
