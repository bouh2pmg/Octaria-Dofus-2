package network.game.message.game.appraoch;

import data.sql.entity.Account;
import server.game.GameServer;
import server.game.client.GameClient;
import utils.SqlUtils;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class AuthenticationTicket extends AbstractMessage implements
		MessageReceived {
	private String lang = "";
	private String ticket = "";
	private GameClient client = null;

	public AuthenticationTicket(Header header,GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() {
		lang = readUTF();
		ticket = readUTF();
	}

	@Override
	public void doPacket() {
		if(!lang.equals("fr")) 
		{
			//TODO Do Anything.
			return;
		}
		if(GameServer.getInstance().containAccWaiting(ticket)) {
			int idAcc = GameServer.getInstance().getAccWaiting(ticket);
			Account acc = SqlUtils.getAccountService().findById(idAcc);
			if(acc != null) {
				client.setAcc(acc);
				new AuthenticationSucces(new Header(MessageID.MSG_AUTH_SUCCESS),client);
				GameServer.getInstance().removeAccWaiting(ticket);
			}else{
				return;
			}
		}else{
			return;
		}
	}
}
