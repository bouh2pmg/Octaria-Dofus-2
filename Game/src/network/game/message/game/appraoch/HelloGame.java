package network.game.message.game.appraoch;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
public class HelloGame extends AbstractMessage implements MessageToSend {
	private GameClient client;

	public HelloGame(Header header,GameClient client) {
		super(header);
		this.client = client;
		//Why that i've implements MessageToSend, if I don't pack it... My mind is weird
		doPacket();
	}

	@Override
	public void pack() {
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}
