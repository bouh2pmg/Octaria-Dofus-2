package network.game.message.game.appraoch;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
import network.game.message.server.basic.BasicTime;

public class AuthenticationSucces extends AbstractMessage implements
		MessageToSend {
	private GameClient client = null;

	public AuthenticationSucces(Header header,GameClient client) {
		super(header);
		this.client = client;
		doPacket();
	}

	@Override
	public void pack() 
	{
		
	}

	@Override
	public void doPacket() {
		client.send(this);
		new BasicTime(client);
		new ServerOptionalFeatures(client,new short[0]);
		new AccountCapabilities(client);
	}
}
