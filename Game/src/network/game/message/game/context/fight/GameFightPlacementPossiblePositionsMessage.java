package network.game.message.game.context.fight;

import data.stat.fight.Fight;
import data.stat.map.TmpMap;
import reader.d2p.map.CellData;
import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class GameFightPlacementPossiblePositionsMessage extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private Fight fight;
	private TmpMap map;
	
	public GameFightPlacementPossiblePositionsMessage(Header header,GameClient client,Fight fight) {
		super(header);
		this.client = client;
		this.fight = fight;
		this.map = fight.getMap();
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		
		/* Ce truc marche
		getPacketOut().putShort((short) 1);
		getPacketOut().putShort((short) 444);
		 */
		
		getPacketOut().putShort((short) map.getRedCells().size());
		System.out.println(map.getRedCells().size());
		for(CellData cell : map.getRedCells().values()) {
			getPacketOut().putShort((short) cell.getCellId());
		}
		getPacketOut().putShort((short) map.getBlueCells().size());
		for(CellData cell : map.getBlueCells().values()) {
			getPacketOut().putShort((short) cell.getCellId());
		}
		getPacketOut().put((byte) fight.getTeams().size());
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
