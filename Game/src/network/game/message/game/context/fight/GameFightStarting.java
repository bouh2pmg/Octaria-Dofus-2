package network.game.message.game.context.fight;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class GameFightStarting extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private byte fightType;

	public GameFightStarting(Header header,GameClient client,byte fightType) {
		super(header);
		this.client = client;
		this.fightType = fightType;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put(fightType);
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
	
	public static class FightType {
		public static final byte CHALLENGE = 0;
		public static final byte AGRESSION = 1;
		public static final byte PvMA = 2;
		public static final byte MXvM = 3;
		public static final byte PvM = 4;
		public static final byte PvT = 5;
		public static final byte PvMU = 6;
		public static final byte ARENA = 7;
		
	}
}
