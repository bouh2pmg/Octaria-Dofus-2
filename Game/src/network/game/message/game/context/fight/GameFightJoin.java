package network.game.message.game.context.fight;

import java.util.BitSet;

import data.stat.fight.Fight;

import server.game.client.GameClient;
import utils.BitsUtils;
import utils.GameConfig;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class GameFightJoin extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private boolean isSpectator;
	private Fight fight;
	
	public GameFightJoin(Header header,GameClient client,boolean isSpectator,Fight fight) {
		super(header);
		this.client = client;
		this.isSpectator = isSpectator;
		this.fight = fight;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		BitSet bits = new BitSet(8);//Byte = 2 octets = 8 bits
		bits.set(0, fight.isCanBeCancelled());//CanBeCanceled
		bits.set(1, fight.isCanSayReady());//CanSayReady
		bits.set(2, isSpectator);//isSpectator
		bits.set(3, fight.isFightStarted());//isFightStarted
		getPacketOut().put(BitsUtils.toByte(bits));
		getPacketOut().putInt(GameConfig.TURN_TIME);
		getPacketOut().put(fight.getFightType());
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
