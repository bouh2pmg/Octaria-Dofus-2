package network.game.message.game.context;

import server.game.client.GameClient;
import utils.Utils;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class GameContext extends AbstractMessage implements MessageToSend {
	private GameClient client = null;
	private byte ctx;
	
	public GameContext(Header header, GameClient client,byte ctx) {
		super(header);
		this.client = client;
		this.ctx = ctx;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put(ctx);//Context
	}

	@Override
	public void doPacket() {
		client.send(this);
		Utils.maps.get(client.getPlayer().getActualMap()).addPlayerOnMap(client.getPlayer());
	}
	
	public static class Context {
		public static byte ROLEPLAY = 1;
		public static byte FIGHT = 2;
	}
}
