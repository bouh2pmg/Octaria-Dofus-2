package network.game.message.game.context.roleplay;

import data.stat.map.TmpMap;
import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class CurrentMap extends AbstractMessage implements MessageToSend {
	private GameClient client;

	public CurrentMap(Header header,GameClient client) {
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putInt((int) client.getPlayer().getActualMap());
		writeUTF(TmpMap.decryptionKey);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}
