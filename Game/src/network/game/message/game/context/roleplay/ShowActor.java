package network.game.message.game.context.roleplay;

import server.game.client.GameClient;
import data.sql.entity.Player;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class ShowActor extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private Player player;

	public ShowActor(Header header,GameClient client, Player player) {
		super(header);
		this.client = client;
		this.player = player;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put(player.toActorInformation());
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
