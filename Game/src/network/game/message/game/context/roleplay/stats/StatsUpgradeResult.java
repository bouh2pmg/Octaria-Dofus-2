package network.game.message.game.context.roleplay.stats;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class StatsUpgradeResult extends AbstractMessage implements MessageToSend 
{
	private GameClient client;
	private short nbCharacBoost;

	public StatsUpgradeResult(Header header,GameClient client, short nbCharacBoost) 
	{
		super(header);
		this.client = client;
		this.nbCharacBoost = nbCharacBoost;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().putShort(nbCharacBoost);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}
