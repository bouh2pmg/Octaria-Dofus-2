package network.game.message.game.context.roleplay;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class MapRemoveElement extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private int actorId;

	public MapRemoveElement(Header header,GameClient client, int actorId) {
		super(header);
		this.client = client;
		this.actorId = actorId;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putInt(actorId);
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
