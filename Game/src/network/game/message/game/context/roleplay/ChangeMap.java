package network.game.message.game.context.roleplay;

import server.game.client.GameClient;
import utils.Utils;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class ChangeMap extends AbstractMessage implements
		MessageReceived {
	private GameClient client;
	private long newMap;
	private long oldMap;

	public ChangeMap(Header header,GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() {
		newMap = getPacketIn().getInt();
		oldMap = client.getPlayer().getActualMap();
	}

	@Override
	public void doPacket() {
		int cell = client.getPlayer().getCell();
		if(Utils.maps.get(oldMap).getMap().getBotNeighbourId() == newMap) {
			cell -= 532;
		}
		if(Utils.maps.get(oldMap).getMap().getTopNeighbourId() == newMap) {
			cell += 532;
		}
		if(Utils.maps.get(oldMap).getMap().getLeftNeighbourId() == newMap) {
			cell += 13;
		}
		if(Utils.maps.get(oldMap).getMap().getRightNeighbourId() == newMap) {
			cell -= 13;
		}
		client.getPlayer().setCell(cell);
		Utils.maps.get(oldMap).removePlayerOnMap(client.getPlayer());
		client.getPlayer().setActualMap(newMap);
		Utils.maps.get(newMap).addPlayerOnMap(client.getPlayer());
		
		new MapComplementaryData(new Header(MessageID.MSG_MAP_COMP_INFO),client,newMap);
	}
}
