package network.game.message.game.context.roleplay.quest;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class QuestListRequest extends AbstractMessage implements MessageReceived {
	
	private GameClient client;

	public QuestListRequest(Header header,GameClient client) 
	{
		super(header);
		this.client = client;
		doPacket();
	}

	@Override
	public void unpack() 
	{
	}

	@Override
	public void doPacket() 
	{
		new QuestListMessage(new Header(MessageID.MSG_QUEST_LIST), client);
	}
}
