package network.game.message.game.context.roleplay.stats;

import data.sql.entity.Stats;
import server.game.client.GameClient;
import utils.enums.StatsID;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;
import network.game.message.game.character.stats.CharacterStatsList;

public class StatsUpgradeRequest extends AbstractMessage implements MessageReceived {
	
	private GameClient client;
	private byte statId;
	private short boostPoint;
	public StatsUpgradeRequest(Header header,GameClient client) 
	{
		super(header);
		this.client = client;
		try{
		unpack();
		doPacket();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void unpack() 
	{
		statId = getPacketIn().get();
		boostPoint = getPacketIn().getShort();
	}

	@Override
	public void doPacket() 
	{		
		switch(statId)
		{
			case 10:
				statId = (byte) StatsID.STRENGTH.getID();
				break;
			case 11:
				statId = (byte) StatsID.VITALITY.getID();
				break;
			case 12:
				statId = (byte) StatsID.WISDOM.getID();
				break;
			case 13:
				statId = (byte) StatsID.CHANCE.getID();
				break;
			case 14:
				statId = (byte) StatsID.AGILITY.getID();
				break;
			case 15:
				statId = (byte) StatsID.INTELLIGENCE.getID();
				break;
		}
		Stats stat = client.getPlayer().getStat((int)statId);
		short remaining = (short) client.getPlayer().boostStat(stat,boostPoint);
		client.getPlayer().setStatsPoints(client.getPlayer().getStatsPoints()-boostPoint);
		new StatsUpgradeResult(new Header(MessageID.MSG_STATS_UPGRADE_RESULT),client,remaining);
		new CharacterStatsList(new Header(MessageID.MSG_PERSO_STATS_LIST),client);
	}
}
