package network.game.message.game.context.roleplay;

import data.sql.entity.Player;
import data.stat.actor.Actor;
import data.stat.map.TmpMap;
import server.game.client.GameClient;
import utils.Utils;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class MapComplementaryData extends AbstractMessage implements
		MessageToSend {
	private GameClient client;
	private TmpMap map;
	
	public MapComplementaryData(Header header,GameClient client, long mapId) {
		super(header);
		this.client = client;
		map = Utils.maps.get(mapId);
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putShort((short) map.getMap().getSubAreaId());
		getPacketOut().putInt((int) map.getMap().getMapId());
		getPacketOut().put((byte) 0);//TODO: after, Alignment subarea side
		getPacketOut().putShort((short) 0);//TODO: House length + house info
		getPacketOut().putShort((short)(map.getPlayerOnMap().size() + map.getActors().size()));//TODO: Actor length + actor info
		//getPacketOut().putShort((short)(map.getPlayerOnMap().size()));
		for(Player player : map.getPlayerOnMap()) {
			getPacketOut().put(player.toActorInformation());
		}
		for(Actor actor : map.getActors().values()) {
			getPacketOut().put(actor.toMapInfo());
		}
		getPacketOut().putShort((short) 0);//TODO: Interactive object length + interactive object info
		getPacketOut().putShort((short) 0);//TODO: Stated element length + stated element info
		getPacketOut().putShort((short) 0);//TODO: Obstacle length + obstacle info
		getPacketOut().putShort((short) 0);//TODO: Fight length + fight info
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
