package network.game.message.game.context.roleplay;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class MapInformationsRequest extends AbstractMessage implements MessageReceived {
	
	private GameClient client;
	private long mapId;

	public MapInformationsRequest(Header header,GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() {
		this.mapId = getPacketIn().getInt();
	}

	@Override
	public void doPacket() 
	{
		new MapComplementaryData(new Header(MessageID.MSG_MAP_COMP_INFO),client,mapId);
	}
}
