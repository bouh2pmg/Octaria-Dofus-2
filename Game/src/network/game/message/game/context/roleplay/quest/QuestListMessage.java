package network.game.message.game.context.roleplay.quest;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class QuestListMessage extends AbstractMessage implements MessageToSend {
	private GameClient client;
	
	public QuestListMessage(Header header,GameClient client) {
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().putShort((short) 0); // Quest finished.
		getPacketOut().putShort((short) 0); // FinishedQuestsCounts.
		getPacketOut().putShort((short) 0); // ActiveQuests.
		
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}
