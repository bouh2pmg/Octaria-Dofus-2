package network.game.message.game.context.map;

import java.util.List;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class MapMovement extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private List<Integer> keyMovement;
	private int actorId;

	public MapMovement(Header header,GameClient client,List<Integer> keyMovement,int actorId) {
		super(header);
		this.client = client;
		this.keyMovement = keyMovement;
		this.actorId = actorId;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().putShort((short) keyMovement.size());
		for(int i : keyMovement) {
			getPacketOut().putShort((short) i);
		}
		getPacketOut().putInt(actorId);
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
