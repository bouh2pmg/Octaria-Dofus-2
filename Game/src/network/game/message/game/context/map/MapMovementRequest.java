package network.game.message.game.context.map;

import java.util.ArrayList;
import java.util.List;
import data.sql.entity.Player;
import data.stat.map.TmpMap;
import server.game.client.GameClient;
import utils.PathUtils;
import utils.Utils;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class MapMovementRequest extends AbstractMessage implements
		MessageReceived {
	private GameClient client;
	private List<Integer> keyMovement = new ArrayList<Integer>();
	private int actorId;
	private TmpMap map;

	public MapMovementRequest(Header header,GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() {
		int keyLen = getPacketIn().getShort();
		for(int i = 0; i < keyLen; i++) {
			keyMovement.add((int) getPacketIn().getShort());
		}
		long mapId = getPacketIn().getInt();
		map = Utils.maps.get(mapId);
		actorId = client.getPlayer().getGuid();
	}

	@Override
	public void doPacket() {
		int newCell = (keyMovement.get(keyMovement.size() - 1) & 4095);
		client.getPlayer().setCell(newCell);
		client.getPlayer().setDisposition(PathUtils.getFinalDirection(keyMovement, map));
		for(Player player : map.getPlayerOnMap()) {
			new MapMovement(new Header(MessageID.MSG_MAP_MOVEMENT),player.getGameClient(),keyMovement,actorId);
		}
	}
}
