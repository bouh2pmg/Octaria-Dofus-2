package network.game.message.game.context.map;

import server.game.client.GameClient;
import utils.Utils;
import network.AbstractMessage;
import network.Header;
import network.MessageReceived;

public class MapMovementConfirm extends AbstractMessage implements
		MessageReceived {
	private GameClient client;

	public MapMovementConfirm(Header header,GameClient client) {
		super(header);
		this.client = client;
		doPacket();
	}

	@Override
	public void unpack() {
	}

	@Override
	public void doPacket() {
		Utils.maps.get(client.getPlayer().getActualMap()).getCells().get(client.getPlayer().getCell()).doAction(client);
	}
}
