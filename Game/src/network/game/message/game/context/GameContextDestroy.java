package network.game.message.game.context;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class GameContextDestroy extends AbstractMessage implements
		MessageToSend {
	private GameClient client;

	public GameContextDestroy(Header header, GameClient client) {
		super(header);
		this.client = client;
		doPacket();
	}

	@Override
	public void pack() {
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
