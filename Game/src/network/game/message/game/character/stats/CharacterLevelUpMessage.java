package network.game.message.game.character.stats;


import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

import server.game.client.GameClient;



public class CharacterLevelUpMessage extends AbstractMessage implements MessageToSend  {
	

	  
	  public CharacterLevelUpMessage(Header header,GameClient client,int level)
	  {
		  super(header);
		  	getPacketOut().put((byte) level);
			client.send(this);
	  }

	@Override
	public void pack() {
		 
	}
	@Override
	public void doPacket() {
		
	}
}

