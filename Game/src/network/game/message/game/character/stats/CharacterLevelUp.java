package network.game.message.game.character.stats;


import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

import server.game.client.GameClient;



public class CharacterLevelUp extends AbstractMessage implements MessageToSend  {
	private GameClient client;
	private int level;

	public CharacterLevelUp(Header header,GameClient client,int level) {
		super(header);
		this.client = client;
		this.level = level;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put((byte) level);
	}
	@Override
	public void doPacket() {
		client.send(this);
	}
}

