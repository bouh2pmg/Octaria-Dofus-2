package network.game.message.game.character.stats;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class LifeRegenBegin extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private byte rate;

	public LifeRegenBegin(Header header, GameClient client,byte rate) {
		super(header);
		this.client = client;
		this.rate = (byte) (rate * 10);
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put(rate);
	}

	@Override
	public void doPacket() {
		//TODO: Life Regen
		client.send(this);
	}
}
