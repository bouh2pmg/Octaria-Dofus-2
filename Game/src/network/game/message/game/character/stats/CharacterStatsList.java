package network.game.message.game.character.stats;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class CharacterStatsList extends AbstractMessage implements MessageToSend {
	private GameClient client = null;

	public CharacterStatsList(Header header, GameClient client) 
	{
		super(header);
		try{
		this.client = client;
		pack();
		doPacket();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void pack() 
	{ 
		getPacketOut().put(client.getPlayer().getStatsInfo());
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}
