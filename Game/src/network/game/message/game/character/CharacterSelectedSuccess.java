package network.game.message.game.character;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class CharacterSelectedSuccess extends AbstractMessage implements
		MessageToSend {
	private GameClient client = null;

	public CharacterSelectedSuccess(Header header, GameClient client) {
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put(client.getPlayer().toBaseInformation());
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}