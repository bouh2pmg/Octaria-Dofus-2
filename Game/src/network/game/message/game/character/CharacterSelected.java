package network.game.message.game.character;

import data.sql.entity.Player;
import server.game.GameServer;
import server.game.client.GameClient;
import utils.SqlUtils;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class CharacterSelected extends AbstractMessage implements
		MessageReceived {
	private GameClient client = null;
	private int characId = 0;

	public CharacterSelected(Header header, GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() {
		characId = getPacketIn().getInt();
	}

	@Override
	public void doPacket() {
		Player player = SqlUtils.getPlayerService().findById(characId);
		if(player != null) {
			player.setGameClient(client);
			client.setPlayer(player);
			new CharacterSelectedSuccess(new Header(MessageID.MSG_PERSO_SELECT_SUCCESS),client);
			GameServer.Persos.add(client.getPlayer());
		}else{
			return; // BUG ?
		}
	}
}
