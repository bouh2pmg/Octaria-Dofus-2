package network.game.message.game.character.deletion;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class CharacterDeletionError extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private int errorId;

	public CharacterDeletionError(Header header,GameClient client,int errorId) {
		super(header);
		this.client = client;
		this.errorId = errorId;
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().put((byte)errorId);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}
