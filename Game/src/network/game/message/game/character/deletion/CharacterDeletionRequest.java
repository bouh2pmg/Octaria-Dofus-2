package network.game.message.game.character.deletion;

import data.sql.entity.Player;
import server.game.client.GameClient;
import utils.SqlUtils;
import utils.Utils;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;
import network.game.message.game.character.CharacterList;

public class CharacterDeletionRequest extends AbstractMessage implements MessageReceived {
	private GameClient client;
	private int playerId;
	private String secretAnswerHash;

	public CharacterDeletionRequest(Header header,GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() 
	{
		
		playerId = getPacketIn().getInt();
		secretAnswerHash = readUTF();
		
	}

	@Override
	public void doPacket() 
	{
		Player player = client.getAcc().getPlayerById(playerId);
		if(player != null)
		{
			String toHash = playerId + "~" + client.getAcc().getAnswer();
			if(player.getLevel() <= 10)
			{
				client.getAcc().getCharacters().remove(player);
				SqlUtils.getPlayerService().delete(player);
				SqlUtils.getAccountService().save(client.getAcc());
				
				new CharacterList(new Header(MessageID.MSG_PERSO_LIST),this.client);
			}
			else if(Utils.cryptHash(toHash).equals(secretAnswerHash))
			{
				client.getAcc().getCharacters().remove(player);
				SqlUtils.getPlayerService().delete(player);
				SqlUtils.getAccountService().save(client.getAcc());
				
				new CharacterList(new Header(MessageID.MSG_PERSO_LIST),this.client);
			}
			else
			{
				new CharacterDeletionError(new Header(MessageID.MSG_PERSO_DELETION_ERROR),client,3);
			}
			
		}
		else
		{
			new CharacterDeletionError(new Header(MessageID.MSG_PERSO_DELETION_ERROR),client,1);
		}
	}
}
