package network.game.message.game.character;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class CharacterListWithModifications extends AbstractMessage implements
		MessageToSend {
	private GameClient client = null;

	public CharacterListWithModifications(Header header, GameClient client) {
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().putShort((short)0);
		getPacketOut().putShort((short)1);
		getPacketOut().putInt(1);
		getPacketOut().putShort((short)0);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}