package network.game.message.game.character.spells;

import data.sql.entity.Spell;
import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class SpellsList extends AbstractMessage implements MessageToSend 
{
	private GameClient client;
	private boolean previzualisation;

	public SpellsList(Header header,GameClient client,boolean previzualisation) 
	{
		super(header);
		this.client = client;
		this.previzualisation = previzualisation;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().put((byte) (previzualisation ? 1 : 0));
		getPacketOut().putUnsignedShort(client.getPlayer().getSpells().size());
		for(Spell spell : client.getPlayer().getSpells()) 
		{
			getPacketOut().put(spell.toList());
		}
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}
