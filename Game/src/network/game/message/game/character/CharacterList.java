package network.game.message.game.character;

import java.util.ArrayList;
import java.util.List;

import data.sql.entity.Player;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class CharacterList extends AbstractMessage implements MessageToSend {
	private GameClient client = null;

	public CharacterList(Header header, GameClient client) {
		super(header);
		this.client = client;
		try{
		pack();
		}catch(Exception e) {
			e.printStackTrace();
		}
		doPacket();
	}

	@Override
	public void pack() {
		boolean hasStartupActions = false; // Todo cadeaux !
		List<Player> data = new ArrayList<Player>();
		for (Player player: client.getAcc().getCharacters()) {
			if(player.getGameGuid() == Integer.parseInt(System.getProperty("ID"))) {
				data.add(player);
			}
        }
		getPacketOut().put((byte)(hasStartupActions ? 1 : 0));
		getPacketOut().putUnsignedShort(data.size());
		for(Player player : data) 
		{
			getPacketOut().putUnsignedShort(45); //Todo constants player type
			getPacketOut().put(player.toBaseInformation());
		}
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
