package network.game.message.game.character.creation;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class CharacterNameSuggestionRequest extends AbstractMessage implements MessageReceived{
	
	private GameClient client;
	public CharacterNameSuggestionRequest(Header header,GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}
	@Override
	public void doPacket() 
	{
		new CharacterNameSuggestionSuccess(new Header(MessageID.MSG_PERSO_NAME_SUGGESTION_SUCCESS), client);
	}
	@Override
	public void unpack() 
	{
		
	}
}
