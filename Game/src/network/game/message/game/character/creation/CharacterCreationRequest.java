package network.game.message.game.character.creation;

import java.util.ArrayList;
import java.util.List;

import data.sql.entity.Player;

import reader.d2o.Definition;
import server.game.GameServer;
import server.game.client.GameClient;
import utils.GameConfig;
import utils.SqlUtils;
import utils.Utils;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;
import network.game.message.game.character.CharacterSelectedSuccess;

public class CharacterCreationRequest extends AbstractMessage implements MessageReceived{
	private GameClient client;
	private String name ;
	private byte breed;
	private boolean sex = true;
	private List<String> colors = new ArrayList<String>();
	public CharacterCreationRequest(Header header,GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void unpack() 
	{
		name = readUTF();
		breed = getPacketIn().get();
		sex = getPacketIn().get() == 1 ? true : false;
		Definition def = Utils.d2o.files.get("Breeds").read(breed,false);
		List<Long> colorsBreed;
		if(!sex)
			colorsBreed = (List<Long>) def.getField("maleColors").value;
		else
			colorsBreed = (List<Long>) def.getField("femaleColors").value;
		for(int i = 0; i < 5; i++) {
			long color = getPacketIn().getInt();
			if(color == -1) {
				color = colorsBreed.get(i).longValue();
			}
			colors.add(Long.toString(color));
		}
	}

	@Override
	public void doPacket() 
	{
		if(client.getAcc().getCharacters().size() == GameConfig.MAX_PERSO_BY_ACCOUNT) // Si il as trop de perso sur le compte , ont lui dis de se faire foutre.
		{
			new CharacterCreationResult(new Header(MessageID.MSG_PERSO_CREATION_RESULT),client,4);
			return;
		}
		if(SqlUtils.getPlayerService().findByName(name) != null) // Si le nom est d�j� pris.
		{
			new CharacterCreationResult(new Header(MessageID.MSG_PERSO_CREATION_RESULT),client,3);
			return;
		}
		//Sinon , ont cr�e le perso
		Player player = Player.createNew(name, breed, sex, colors);
		client.getAcc().getCharacters().add(player); // ont ajoute le perso au compte
		player.setGameClient(client);//On li le gameclient au perso
		client.setPlayer(player);//Ont lie le perso au gameclient
		SqlUtils.getPlayerService().create(player); // ont le cr�e puis on le sauvegarde.
		SqlUtils.getAccountService().save(client.getAcc());
		new CharacterCreationResult(new Header(MessageID.MSG_PERSO_CREATION_RESULT),client,0); 
		new CharacterSelectedSuccess(new Header(MessageID.MSG_PERSO_SELECT_SUCCESS),client);
		GameServer.Persos.add(client.getPlayer()); //ont l'ajoute dans la liste de tous les Persos connect�e.
	}
}
