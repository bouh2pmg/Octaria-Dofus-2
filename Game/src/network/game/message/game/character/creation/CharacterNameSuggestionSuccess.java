package network.game.message.game.character.creation;

import server.game.client.GameClient;
import utils.Utils;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
public class CharacterNameSuggestionSuccess extends AbstractMessage implements MessageToSend 
{
	private GameClient client;
	
	public CharacterNameSuggestionSuccess(Header header,GameClient client) 
	{
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		String Pseudo = Utils.generatePseudo(Utils.generateNumber(5,10));
		writeUTF(Pseudo);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}
