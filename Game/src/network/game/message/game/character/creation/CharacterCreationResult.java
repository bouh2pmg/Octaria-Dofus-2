package network.game.message.game.character.creation;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class CharacterCreationResult extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private int result;
	public CharacterCreationResult(Header header,GameClient client,int result) 
	{
		super(header);
		this.client = client;
		this.result = result;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().put((byte)result);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}
