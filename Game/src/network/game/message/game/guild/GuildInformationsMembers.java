package network.game.message.game.guild;

import org.apache.mina.core.buffer.IoBuffer;
import data.sql.entity.Player;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
import server.game.client.GameClient;


/**
 * @author Yokaze
 *
 */

public class GuildInformationsMembers extends AbstractMessage implements MessageToSend {
	
	private GameClient client;
	
	public GuildInformationsMembers(Header header , GameClient client)
	{
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
	    IoBuffer buff = getPacketOut();
	    Player member = client.getPlayer();
	    buff.put(member.getBreed()); 	// Classe
	    buff.put(member.getSexe());  	// Sexe
	    buff.putShort((short) 5); 		// Rank
	    buff.putDouble(1000);		// ExperienceGiven
	    buff.put((byte) 5);			// ExperienceGivenPercent
	    buff.putUnsignedInt(5);		// Rights
	    buff.put((byte) 1);			// Connected
	    buff.put(member.getAlignmentSide());//AlignementSide
	    buff.putShort((short) 24);		//HourSiceLastConnection
	    buff.put((byte) 0);			//MoudSmilerId
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}

