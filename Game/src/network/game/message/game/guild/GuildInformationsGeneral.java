package network.game.message.game.guild;

import java.util.BitSet;

import server.game.client.GameClient;
import utils.BitsUtils;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

/**
 * @author Yokaze
 */
public class GuildInformationsGeneral extends AbstractMessage implements MessageToSend {
	
	private GameClient client;
	
	public GuildInformationsGeneral(Header header , GameClient client)
	{
		super(header);
		this.client = client; 
		pack();
		doPacket();
	}
	@Override
	public void pack()
	{
		BitSet bitSet = new BitSet();
		bitSet.set(1, true);
		bitSet.set(2, false);
		getPacketOut().put(BitsUtils.toByte(bitSet));
		getPacketOut().put((byte) 15);
		getPacketOut().putDouble(589);
		getPacketOut().putDouble(589);
		getPacketOut().putDouble(589);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}
