package network.game.message.game.chat;

import data.sql.entity.Player;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;
import server.game.client.GameClient;
import utils.SqlUtils;

/**
 * @author Yokaze
 *
 */
public class ChatClientPrivate extends AbstractMessage implements MessageReceived 
{
    private GameClient client;
    private String content;
    private String receiverName;

    public ChatClientPrivate(Header header,GameClient client) 
    {
	super(header);
	this.client = client;
	unpack();
	doPacket();
    }

    @Override
    public void unpack() 
    {
	content = readNoPrefixedUTF();
	receiverName = readNoPrefixedUTF();
	System.out.println("Content : " + content);
	System.out.println("Name : " + content);
    }

    @Override
    public void doPacket() 
    {
	Player player = SqlUtils.getPlayerService().findByName(receiverName);
	if(player != null)
	{
	    if(player.getGameClient() != null)
	    {
		new ChatServerMessage(new Header(MessageID.MSG_CHAT_SERVER_MESSAGE),player.getGameClient(),9,content,System.currentTimeMillis(),"",client.getPlayer().getGuid(),client.getPlayer().getName(),client.getAcc().getGuid());
		new ChatServerCopy(new Header(MessageID.MSG_CHAT_SERVER_COPY_MESSAGE),player.getGameClient(),9,content,System.currentTimeMillis(),"",client.getPlayer().getGuid(),player.getName());
	    }
	    else // Player not connected !
	    {
		new ChatError(new Header(MessageID.MSG_CHAT_ERROR_MESSAGE),client,1); // RECEIVER_NOT_FOUND
	    }
	} 
	else // Player not exist !
	{
	    new ChatError(new Header(MessageID.MSG_CHAT_ERROR_MESSAGE),client,1); // RECEIVER_NOT_FOUND
	}

    }
}

