package network.game.message.game.chat;

import data.sql.entity.Player;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;
import server.game.GameServer;
import server.game.client.GameClient;
import utils.Utils;

public class ChatMessage extends AbstractMessage implements MessageReceived {
    private GameClient client;
    private byte channel;
    private String content;

    public ChatMessage(Header header,GameClient client) 
    {
	super(header);
	this.client = client;
	unpack();
	doPacket();
    }

    @Override
    public void unpack() 
    {
	content = readUTF();
	channel = getPacketIn().get();
    }

    @Override
    public void doPacket() 
    {
	if(content.startsWith("!"))
	{
	    System.out.println("Command not found !");
	}
	switch(channel)
	{
		case 0:
		    for(Player player : Utils.maps.get(client.getPlayer().getActualMap()).getPlayerOnMap())
		    {
			GameClient gameClient = player.getGameClient();
			new ChatServerMessage(new Header(MessageID.MSG_CHAT_SERVER_MESSAGE),gameClient,channel,content,System.currentTimeMillis(),"",client.getPlayer().getGuid(),client.getPlayer().getName(),client.getAcc().getGuid());
		    }
		    break;
		case 2:
		    //Guild
		    new ChatError(new Header(MessageID.MSG_CHAT_ERROR_MESSAGE),client,3);
		    break;
		case 4:
		    //Group
		    new ChatError(new Header(MessageID.MSG_CHAT_ERROR_MESSAGE),client,4);
		    break;
		case 5:
		    //Commerce
		    for(Player player : GameServer.getPersos())
		    {
			GameClient gameClient = player.getGameClient();
			new ChatServerMessage(new Header(MessageID.MSG_CHAT_SERVER_MESSAGE),gameClient,channel,content,System.currentTimeMillis(),"",client.getPlayer().getGuid(),client.getPlayer().getName(),client.getAcc().getGuid());
		    }
		    break;
		case 6:
		    //Recrutement
		    for(Player player : GameServer.getPersos())
		    {
			GameClient gameClient = player.getGameClient();
			new ChatServerMessage(new Header(MessageID.MSG_CHAT_SERVER_MESSAGE),gameClient,channel,content,System.currentTimeMillis(),"",client.getPlayer().getGuid(),client.getPlayer().getName(),client.getAcc().getGuid());
		    }
		    break;

	default:
	    System.out.println("Channel not found : " + channel);
	    break;
	}
    }
}

