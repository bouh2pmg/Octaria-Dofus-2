package network.game.message.game.chat;

import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
import server.game.client.GameClient;

public class ChatServerMessage extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private int channel;
	private String content;
	private double timestamp;
	private String fingerprint;
	private int senderId;
	private String senderName;
	private int senderAccountId;

	public ChatServerMessage(Header header, GameClient client,int channel,String content,long timestamp,String fingerprint,int senderId,String senderName,int senderAccountId) 
	{
		super(header);
		this.channel = channel;
		this.content = content;
		this.fingerprint = fingerprint;
		this.timestamp = timestamp;
		this.client = client;       
		this.senderId = senderId;
		this.senderName = senderName;
		this.senderAccountId = senderAccountId;
		pack();
		doPacket();
	}
	
	@Override
	public void pack() 
	{
		getPacketOut().put((byte) channel);
		writeUTF(content);
		getPacketOut().putInt((int) timestamp);
		writeUTF(fingerprint);
		getPacketOut().putInt(senderId);
		writeUTF(senderName);
		getPacketOut().putInt(senderAccountId);

	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}


