package network.game.message.game.chat;

import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
import server.game.client.GameClient;

public class ChatError extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private int reason;

	public ChatError(Header header,GameClient client,int reason) 
	{
		super(header);
		this.client = client;
		this.reason = reason;
		pack();
		doPacket();
	}
	
	@Override
	public void pack() 
	{
		getPacketOut().put((byte) reason);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}


