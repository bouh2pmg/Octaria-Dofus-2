package network.game.message.game.chat;

import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
import server.game.client.GameClient;

/**
 * @author Yokaze
 *
 */
public class ChatServerCopy extends AbstractMessage implements MessageToSend {
    
	private GameClient client;
	private int channel;
	private String content;
	private double timestamp;
	private String fingerprint;
	private int receiverId;
	private String receiverName;

	public ChatServerCopy(Header header, GameClient client,int channel,String content,long timestamp,String fingerprint,int receiverId,String receiverName) 
	{
		super(header);
		this.channel = channel;
		this.content = content;
		this.fingerprint = fingerprint;
		this.timestamp = timestamp;
		this.client = client;    
		this.receiverId = receiverId;
		this.receiverName = receiverName;
		pack();
		doPacket();
	}
	
	@Override
	public void pack() 
	{
		getPacketOut().put((byte) channel);
		writeUTF(content);
		getPacketOut().putInt((int) timestamp);
		writeUTF(fingerprint);
		getPacketOut().putInt(receiverId);
		writeUTF(receiverName);

	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}


