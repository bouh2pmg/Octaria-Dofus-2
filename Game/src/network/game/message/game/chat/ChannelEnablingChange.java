package network.game.message.game.chat;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class ChannelEnablingChange extends AbstractMessage implements
		MessageToSend {
	private GameClient client;
	private byte channel;
	private boolean value;

	public ChannelEnablingChange(Header header,GameClient client, byte channel, boolean value) {
		super(header);
		this.client = client;
		this.channel = channel;
		this.value = value;
		pack();
		doPacket();
	}

	@Override
	public void pack() {
		getPacketOut().put(channel);
		getPacketOut().put((byte)(value ? 1 : 0));
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
