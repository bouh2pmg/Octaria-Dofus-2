package network.game.message.game.basic;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

/**
 * @author Yokaze
 */
public class TextInformation extends AbstractMessage implements MessageToSend
{
    private GameClient client;
    private byte msgType;
    private short msgId;
    private String[] args;
    
    public TextInformation(Header header , GameClient client , byte msgType , short msgId , String[]args)
    {
	super(header);
	this.client = client;
	this.msgType = msgType;
	this.msgId = msgId;
	this.args = args;
	pack();
	doPacket();
	
    }
    @Override
    public void pack() 
    {
	getPacketOut().put(msgType);
	getPacketOut().putShort(msgId);
	getPacketOut().putShort((short) args.length);
	for(String str : args)
	{
	    writeUTF(str);
	}
    }
    @Override
    public void doPacket() 
    {
	client.send(this);
    }
}


