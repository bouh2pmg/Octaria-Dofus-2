package network.game.message.authorized;

import java.util.Arrays;

import network.AbstractMessage;
import network.Header;
import network.MessageReceived;

import server.game.client.GameClient;

/**
 * @author Yokaze
 */
 
public class Console extends AbstractMessage implements MessageReceived  {
		private GameClient client;
		private String content;
	  
	  public Console(Header header,GameClient client)
	  {
		  super(header);
		  this.client = client;
		  unpack();
		  doPacket();
	
	  }

	@Override
	public void unpack() {
		 content = readUTF();
	}
	@Override
	public void doPacket() 
	{
	    //String cmd = content.split(" ")[0];
		//new CommandParser(cmd,Arrays.asList(content.substring(cmd.length()).split(" ")) , client).parseCommand();	
	}
}