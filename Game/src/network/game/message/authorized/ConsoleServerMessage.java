package network.game.message.authorized;

import network.AbstractMessage;
import network.Header;
import network.MessageToSend;
import server.game.client.GameClient;

/**
 * @author Yokaze
 */
 
public class ConsoleServerMessage extends AbstractMessage implements MessageToSend {
	private GameClient client;
	private byte Type;
	private String content;

	public ConsoleServerMessage(Header header, GameClient client,byte Type,String content) 
	{
		super(header);
		this.content = content;
		this.client = client;       
		pack();
		doPacket();
	}
	
	@Override
	public void pack() 
	{
		getPacketOut().put((byte) Type);
		writeUTF(content);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);
	}
}


