package network.game.message.server.basic;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class SystemMessageDisplayMessage extends AbstractMessage implements MessageToSend {
	private GameClient client = null;
	private boolean hangUp;
	private int msgId;
	private String[] args;

	public SystemMessageDisplayMessage(Header header,GameClient client,boolean handUp,int msgId,String[]args) {
		super(header);
		this.client = client;
		this.hangUp = handUp;
		this.msgId = msgId;
		this.args = args;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().put((byte) ((boolean) hangUp ? 1 : 0));
		getPacketOut().putShort((short)msgId);
		getPacketOut().putShort((short) args.length);
		for(String str : args)
		{
			writeUTF(str);
		}
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
