package network.game.message.server.basic;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class TextInformationMessage extends AbstractMessage implements MessageToSend {
	private GameClient client = null;
	private byte msgType;
	private int msgId;
	private String[] args;

	public TextInformationMessage(Header header,GameClient client,byte msgType, int msgId, String[]args) {
		super(header);
		this.client = client;
		this.msgType = msgType;
		this.msgId = msgId;
		this.args = args;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().put((byte) this.msgType);
		getPacketOut().putShort((short)msgId);
		getPacketOut().putShort((short) args.length);
		for(String str : args)
		{
			writeUTF(str);
		}
	}

	@Override
	public void doPacket() {
		client.send(this);
	}
}
