package network.game.message.server.basic;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageToSend;

public class BasicTime extends AbstractMessage implements MessageToSend {
	
	private GameClient client;

	public BasicTime(GameClient client) {
		super(new Header(MessageID.MSG_BASIC_TIME));
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().putInt((int)System.currentTimeMillis());
		getPacketOut().putInt(7200);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}
