package network.game.message.server.basic;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageID;
import network.MessageReceived;

public class BasicPing extends AbstractMessage implements MessageReceived {
	
	private GameClient client;
	private byte quiet;

	public BasicPing(Header header,GameClient client) {
		super(header);
		this.client = client;
		unpack();
		doPacket();
	}

	@Override
	public void unpack() 
	{
		quiet = getPacketIn().get();
	}

	@Override
	public void doPacket() 
	{
		new BasicPong(new Header(MessageID.MSG_BASIC_PONG),client,quiet);
	}
}
