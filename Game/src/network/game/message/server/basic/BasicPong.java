package network.game.message.server.basic;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class BasicPong extends AbstractMessage implements MessageToSend {
	
	private GameClient client;
	private byte quiet;
	
	public BasicPong(Header header , GameClient client , byte quiet)
	{
		super(header);
		this.quiet = quiet;
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		getPacketOut().put((byte) quiet);
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}
