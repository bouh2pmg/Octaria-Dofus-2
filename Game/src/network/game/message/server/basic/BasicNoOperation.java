package network.game.message.server.basic;

import server.game.client.GameClient;
import network.AbstractMessage;
import network.Header;
import network.MessageToSend;

public class BasicNoOperation extends AbstractMessage implements MessageToSend {
	
	private GameClient client;

	public BasicNoOperation(Header header , GameClient client)
	{
		super(header);
		this.client = client;
		pack();
		doPacket();
	}

	@Override
	public void pack() 
	{
		
	}

	@Override
	public void doPacket() 
	{
		client.send(this);	
	}
}
