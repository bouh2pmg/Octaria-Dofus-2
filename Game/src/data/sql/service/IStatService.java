package data.sql.service;

import data.sql.entity.Stats;

public interface IStatService {
	public void save(Stats stat);
	public int getNextId();
}
