package data.sql.service;

import java.util.List;
import data.sql.entity.Guild;

/**
 * @author Yokaze
 */

public interface IGuildService 
{
    public void save(Guild guild);
    public Guild findById(int id);
    public List<Guild> findAll();
    public void delete(Guild guild);
    public void create(Guild guild);
}
