package data.sql.service.impl;

import org.hibernate.Query;

import data.sql.dao.StatDAO;
import data.sql.entity.Stats;
import data.sql.service.IStatService;

public class StatService implements IStatService {
	private StatDAO statDao;

	public StatDAO getStatDao() {
		return statDao;
	}

	public void setStatDao(StatDAO statDao) {
		this.statDao = statDao;
	}

	@Override
	public void save(Stats stat) {
		statDao.getSession().saveOrUpdate(stat);
	}

	@Override
	public int getNextId() {
		Query query = statDao.getSession().getNamedQuery("findNextStatsId");
        Number size = (Number) query.uniqueResult();
        return size.intValue() + 1;
	}
}
