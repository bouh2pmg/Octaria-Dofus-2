package data.sql.service.impl;

import java.util.List;

import org.hibernate.Query;

import data.sql.dao.PlayerDAO;
import data.sql.entity.Player;
import data.sql.service.IPlayerService;


public class PlayerService implements IPlayerService{
    private PlayerDAO playerDao;

    public void setPlayerDao(PlayerDAO playerDao) {
	this.playerDao = playerDao;
    }

    @Override
    public Player findById(int id) {
	Player player = (Player)playerDao.getSession().get(Player.class,id);
	return player;
    }

    @Override
    public Player findByName(String name) {
	Query query = playerDao.getSession().getNamedQuery("findPlayerByName").setParameter("name", name);
	Player player = (Player) query.uniqueResult();
	return player;
    }

    @Override
    public List<Player> findAll() {
	return null;
    }
    public void save(Player player) {
	playerDao.getSession().update(player);
    }

    @Override
    public void create(Player player) {
	playerDao.getSession().save(player);
    } 

    @Override
    public int getNextId() 
    {
	Query query = playerDao.getSession().getNamedQuery("findSimpleNextPlayerId");
	Number simpleSize = (Number) query.uniqueResult();
	if(simpleSize.intValue() == 0)
	{
	    return 1;
	}
	else
	{
	    query = playerDao.getSession().getNamedQuery("findNextPlayerId");
	    Number size = (Number) query.uniqueResult();
	    return size.intValue() + 1;
	}
    }



    @Override
    public void delete(Player player) {
	playerDao.getSession().delete(player);
    }
}
