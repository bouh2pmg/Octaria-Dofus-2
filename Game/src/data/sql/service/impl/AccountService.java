package data.sql.service.impl;

import java.util.List;
import org.hibernate.Query;
import data.sql.dao.AccountDAO;
import data.sql.entity.Account;
import data.sql.service.IAccountService;

public class AccountService implements IAccountService{
    private AccountDAO accountDao;

    public void setAccountDao(AccountDAO accountDao) {
	this.accountDao = accountDao;
    }

    @Override
    public Account findById(int id) {
	Account acc = (Account) accountDao.getSession().get(Account.class,id);
	return acc;
    }

    @Override
    public Account findByName(String name) {
	Query query = accountDao.getSession().getNamedQuery("findAccountByName").setParameter("name", name);
	Account acc = (Account) query.uniqueResult();
	return acc;
    }
    @Override
    public List<Account> findAll() {
	Query query = accountDao.getSession().getNamedQuery("findAllAccount");
	@SuppressWarnings("unchecked")
	List<Account> allAccount= (List<Account>) query.list();
	return allAccount;
    }

    @Override
    public void save(Account acc) {
	accountDao.getSession().update(acc);
    }
}
