package data.sql.service.impl;

import java.util.List;
import org.hibernate.Query;
import data.sql.dao.GuildDAO;
import data.sql.entity.Guild;
import data.sql.service.IGuildService;

/**
 * @author Yokaze
 */
public class GuildService implements IGuildService 
{
    private GuildDAO guildDao;

    @Override
    public void delete(Guild guild) {
	getGuildDao().getSession().delete(guild);
    }
    @Override
    public void save(Guild guild) {
	getGuildDao().getSession().update(guild);
    }
    @Override
    public void create(Guild guild) {
	getGuildDao().getSession().save(guild);
    }
    @Override
    public Guild findById(int id) {
	Guild guild = (Guild)getGuildDao().getSession().get(Guild.class, id);
	return guild;
    }
    @Override
    public List<Guild> findAll() {
	Query query = guildDao.getSession().getNamedQuery("findAllGuild");
	@SuppressWarnings("unchecked")
	List<Guild> allGuild = (List<Guild>) query.list();
	return allGuild;
    }
    public GuildDAO getGuildDao() {
	return guildDao;
    }
    public void setGuildDao(GuildDAO guildDao) {
	this.guildDao = guildDao;
    }
}
