package data.sql.service;

import java.util.List;

import data.sql.entity.Player;


public interface IPlayerService {
	public void save(Player player);
	public Player findById(int id);
    public Player findByName(String name);
    public List<Player> findAll();
    public void create(Player player);
    public int getNextId();
    public void delete(Player player);
}
