package data.sql.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;

import data.sql.entity.Entity;

public abstract class DAO<T extends Entity> {
    private SessionFactory sessionFactory;;

    public DAO () {
    }
    
    public Session getSession() {
        boolean allowCreate = true;
        return SessionFactoryUtils.getSession(sessionFactory, allowCreate);
    }
    
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
