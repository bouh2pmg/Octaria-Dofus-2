package data.sql.entity;

import org.apache.mina.core.buffer.IoBuffer;

public class Spell implements Entity {
	private byte position;
	private int id;
	private byte level;
	
	public byte getPosition() {
		return position;
	}
	public void setPosition(byte position) {
		this.position = position;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public byte getLevel() {
		return level;
	}
	public void setLevel(byte level) {
		this.level = level;
	}
	
	public IoBuffer toList() {
		IoBuffer data = IoBuffer.allocate(10);
		data.put(position);
		data.putInt(id);
		data.put(level);
		return data.flip();
	}
}
