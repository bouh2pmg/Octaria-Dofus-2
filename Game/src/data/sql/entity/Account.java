package data.sql.entity;

import java.util.ArrayList;
import java.util.List;

import org.apache.mina.core.buffer.IoBuffer;

public class Account implements Entity {
    private int guid = 0;
    private String name = new String();
    private String password = new String();
    private String pseudo = new String();
    private String question = new String();
    private String answer = new String();
    private double endDate = 0.0;
    private byte community = 0;
    private byte gmLevel = 0;
    private boolean isConnected = false;
    private List<Player> characters = new ArrayList<Player>();
    private List<Account> friends = new ArrayList<Account>();
    private List<Account> ignored = new ArrayList<Account>();


    public Account() {
    }

    public int getGuid() {
	return this.guid;
    }

    public void setGuid(int guid) {
	this.guid = guid;
    }
    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }
    public String getPassword() {
	return this.password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public boolean hasRight() {
	return (gmLevel > 3 ? true : false);
    }

    public String getAnswer() {
	return answer;
    }

    public void setAnswer(String answer) {
	this.answer = answer;
    }

    public byte getCommunity() {
	return community;
    }

    public void setCommunity(byte community) {
	this.community = community;
    }

    public double getEndDate() {
	return endDate;
    }

    public void setEndDate(double endDate) {
	this.endDate = endDate;
    }

    public byte getGmLevel() {
	return gmLevel;
    }

    public void setGmLevel(byte gmLevel) {
	this.gmLevel = gmLevel;
    }

    public boolean isConnected() {
	return isConnected;
    }

    public void setIsConnected(boolean isConnected) {
	this.isConnected = isConnected;
    }

    public String getPseudo() {
	return pseudo;
    }

    public void setPseudo(String pseudo) {
	this.pseudo = pseudo;
    }

    public String getQuestion() {
	return question;
    }

    public void setQuestion(String question) {
	this.question = question;
    }

    public List<Player> getCharacters() {
	return characters;
    }

    public void setCharacters(List<Player> characters) {
	this.characters = characters;
    }

    public List<Account> getFriends() {
	return friends;
    }

    public void setFriends(List<Account> friends) {
	this.friends = friends;
    }

    public List<Account> getIgnored() {
	return ignored;
    }

    public void setIgnored(List<Account> ignored) {
	this.ignored = ignored;
    }

    public IoBuffer toFriendInfo() {
	IoBuffer buff = IoBuffer.allocate(2048);
	buff.putInt(getGuid());
	buff.putUnsignedShort(getName().length());
	buff.put(getName().getBytes());
	buff.put((byte)0);//Player State
	buff.putInt(55);// Last connection
	return buff.flip();
    }

    public IoBuffer toIgnoredInfo() {
	IoBuffer buff = IoBuffer.allocate(2048);
	buff.putInt(getGuid());
	buff.putUnsignedShort(getName().length());
	buff.put(getName().getBytes());
	return buff.flip();
    }

    public Player getPlayerById(int id) {
	for(Player player : characters) {
	    if(player.getGuid() == id) {
		return player;
	    }
	}
	return null;
    }
}


