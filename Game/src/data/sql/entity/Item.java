package data.sql.entity;

import java.util.ArrayList;
import java.util.List;

import org.apache.mina.core.buffer.IoBuffer;

import data.stat.Effect;

public class Item implements Entity {
	private byte position = 0;
	private short objectGID = 0;
	private short powerRate = 0;
	private boolean overMax = false;
	private List<Effect> effects = new ArrayList<Effect>();
	private int objectUID = 0;
	private int quantity = 0;
	
	public byte getPosition() {
		return position;
	}
	public void setPosition(byte position) {
		this.position = position;
	}
	public short getObjectGID() {
		return objectGID;
	}
	public void setObjectGID(short objectGID) {
		this.objectGID = objectGID;
	}
	public short getPowerRate() {
		return powerRate;
	}
	public void setPowerRate(short powerRate) {
		this.powerRate = powerRate;
	}
	public boolean isOverMax() {
		return overMax;
	}
	public void setOverMax(boolean overMax) {
		this.overMax = overMax;
	}
	public List<Effect> getEffects() {
		return effects;
	}
	public void setEffects(List<Effect> effects) {
		this.effects = effects;
	}
	public int getObjectUID() {
		return objectUID;
	}
	public void setObjectUID(int objectUID) {
		this.objectUID = objectUID;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public IoBuffer toObjectInformation() {
		IoBuffer buff = IoBuffer.allocate(2048);
		buff.put(position);
		buff.putShort(objectGID);
		buff.putShort(powerRate);
		buff.put((byte)(overMax ? 1 : 0));
		buff.putShort((short) effects.size());
		if(effects.size() > 0) 
		{
			for(Effect effect : effects) {
				buff.putShort((short) 76);//TypeId of Effects
				buff.put(effect.toInformation());
			}
		}
		buff.putInt(objectUID);
		buff.putInt(quantity);
		return buff.flip();
	}
}
