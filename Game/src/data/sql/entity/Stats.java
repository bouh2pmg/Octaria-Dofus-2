package data.sql.entity;

import org.apache.mina.core.buffer.IoBuffer;

public class Stats implements Entity {
	private int id = 0;
	private int statId = 0;
	private short base = 0;
	private short objectsAndMountBonus = 0;
	private short alignGiftBonus = 0;
	private short contextModif = 0;
	
	public Stats() {
	}
	
	public Stats(int id,int statId,short base) {
		this.id = id;
		this.statId = statId;
		this.base = base;
	}
	
	public IoBuffer toInfo() {
		IoBuffer buff = IoBuffer.allocate(8);
		buff.putShort(base);
		buff.putShort(objectsAndMountBonus);
		buff.putShort(alignGiftBonus);
		buff.putShort(contextModif);
		return buff.flip();
	}
	
	public short getBase() {
		return base;
	}
	public void setBase(short base) {
		this.base = base;
	}
	public short getObjectsAndMountBonus() {
		return objectsAndMountBonus;
	}
	public void setObjectsAndMountBonus(short objectsAndMountBonus) {
		this.objectsAndMountBonus = objectsAndMountBonus;
	}
	public short getAlignGiftBonus() {
		return alignGiftBonus;
	}
	public void setAlignGiftBonus(short alignGiftBonus) {
		this.alignGiftBonus = alignGiftBonus;
	}
	public short getContextModif() {
		return contextModif;
	}
	public void setContextModif(short contextModif) {
		this.contextModif = contextModif;
	}

	public int getStatId() {
		return statId;
	}

	public void setStatId(int statId) {
		this.statId = statId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}