package data.sql.entity;

import java.util.ArrayList;
import java.util.List;

import org.apache.mina.core.buffer.IoBuffer;

/**
 * @author Yokaze
 *
 */
public class Guild implements Entity
{
    private int id;
    private int level;
    private int experience;
    /* Emblem */
    private int symbolShape;
    private int symbolColor;
    private int backgroundShape;
    private int backgroundColor;
    /* End Emblem */
    private List<Player> members = new ArrayList<Player>();
    
    public IoBuffer toGuildMember()
    {
	IoBuffer buff = IoBuffer.allocate(4096);
	for(Player member : members)
	{
	    buff.put(member.getBreed()); 	// Classe
	    buff.put(member.getSexe());  	// Sexe
	    buff.putShort((short) 5); 		// Rank
	    buff.putDouble(1000);		// ExperienceGiven
	    buff.put((byte) 5);			// ExperienceGivenPercent
	    buff.putUnsignedInt(5);		// Rights
	    buff.put((byte) 1);			// Connected
	    buff.put(member.getAlignmentSide());//AlignementSide
	    buff.putShort((short) 24);		//HourSiceLastConnection
	    buff.put((byte) 0);			//MoudSmilerId
	}
	return buff;
    }
    public List<Player> getMembers() {
	return members;
    }
    public void setMembers(List<Player> members) {
	this.members = members;
    }
    public int getId() {
	return id;
    }
    public void setId(int id) {
	this.id = id;
    }
    public int getSymbolColor() {
	return symbolColor;
    }
    public void setSymbolColor(int symbolColor) {
	this.symbolColor = symbolColor;
    }
    public int getBackgroundShape() {
	return backgroundShape;
    }
    public void setBackgroundShape(int backgroundShape) {
	this.backgroundShape = backgroundShape;
    }
    public int getSymbolShape() {
	return symbolShape;
    }
    public void setSymbolShape(int symbolShape) {
	this.symbolShape = symbolShape;
    }
    public int getBackgroundColor() {
	return backgroundColor;
    }
    public void setBackgroundColor(int backgroundColor) {
	this.backgroundColor = backgroundColor;
    }
    public int getLevel() {
	return level;
    }
    public void setLevel(int level) {
	this.level = level;
    }
    public int getExperience() {
	return experience;
    }
    public void setExperience(int experience) {
	this.experience = experience;
    }
}
