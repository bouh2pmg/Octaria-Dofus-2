package data.sql.entity;

import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import network.Header;
import network.MessageID;
import network.game.message.game.basic.TextInformation;

import org.apache.mina.core.buffer.IoBuffer;


import reader.d2o.Definition;
import server.game.client.GameClient;
import utils.GameConfig;
import utils.SqlUtils;
import utils.Utils;
import utils.enums.Breed;
import utils.enums.StatsID;
import utils.experience.PersoExperience;

public class Player implements Entity 
{
    private int guid = 0;
    private int gameGuid = 0;
    private short level = 1;
    private String name = "";
    private byte breed = 0;
    private boolean sex = false;
    private long experience = 0L;
    private String colorsTmp = "";
    private List<String> colors = new ArrayList<String>();
    private long actualMap = 0;
    private GameClient client = null;
    private List<Spell> spells = new ArrayList<Spell>();
    private int kama = 0;
    private List<Item> inventory = new ArrayList<Item>();
    private int pods = 0;
    private int podsMax = 0;
    private int spellsPoints = 0;
    private int statsPoints = 0;
    private int cell = 0;
    private int disposition = 0;
    private boolean pvpEnabled = false;
    private List<Stats> stats = new ArrayList<Stats>();
    private byte alignmentSide = 0;
    private int honor = 0;
    private int life = 0;
    private int guild_rank = 0; // TODO add in dao.
    private double guild_givenExperience = 0; // TODO add in dao.
    private double guild_experienceGivenPercent = 0; // TODO add in dao.
    private int guild_rights = 0; // TODO add in dao.
    private int energy = 10000;
    private int title = 0;
    private String titleParams = "";

    public Player() {
    }

    public static Player createNew(String name,byte breed,boolean sex, List<String> colors) {
	Player p = new Player();
	p.guid = SqlUtils.getPlayerService().getNextId();
	p.gameGuid = Byte.parseByte(System.getProperty("ID"));
	p.name = name;
	p.breed = breed;
	p.sex = sex;
	p.colors = colors;
	boolean isFirst = true;
	for(String color : colors) {
	    if(!isFirst) {
		p.colorsTmp += ",";
	    }
	    p.colorsTmp += color;
	    isFirst = false;
	}
	p.actualMap = 143384;
	p.cell = 250;
	p.disposition = 3;
	p.honor = 0;
	p.level = 1;
	p.kama = 0;
	p.energy = 10000;
	p.experience = 0L;
	p.pvpEnabled = false;
	p.title = 0;
	p.titleParams = "";
	
	p.generateBaseStats();	

	return p;
    }


    @SuppressWarnings("unchecked")
    public IoBuffer toBaseInformation() {
	IoBuffer buff = IoBuffer.allocate(250);
	//Minimal
	buff.putUnsignedInt(getGuid());
	buff.putUnsigned(getLevel());
	buff.putUnsignedShort(name.length());
	buff.put(name.getBytes());
	Definition def = Utils.d2o.files.get("Breeds").read(breed,false);

	//Look
	buff.putUnsignedShort(1);//BonesId

	List<Long> skins;
	if(!isSex())
	    skins = (List<Long>) def.getField("alternativeMaleSkin").value;
	else
	    skins = (List<Long>) def.getField("alternativeFemaleSkin").value;
	buff.putUnsignedShort(skins.size());//Number of skins
	for(Long i : skins) {
	    buff.putUnsignedShort(i.shortValue());
	}
	buff.putUnsignedShort(5);//Number of colors
	for(int i = 0; i < 5; i++) {
	    buff.putUnsignedInt(Integer.parseInt(getColors().get(i)) | (i + 1) * 0x1000000);
	}
	buff.putUnsignedShort(1);//Number of scales
	buff.putUnsignedShort(150);//Default scale
	buff.putUnsignedShort(0);//SubEntities

	//Base
	buff.put(getBreed());
	buff.put(getSexe());
	return buff.flip();
    }

    public IoBuffer getStatsInfo() {
	IoBuffer buff = IoBuffer.allocate(4096);
	if(stats.isEmpty()) {
	    generateBaseStats();
	}

	// 		=========================== 	//
	// CharacterCharacteristicsInformations //
	// 		=========================== 	//

	buff.putDouble(experience);
	buff.putDouble(PersoExperience.getExperience(level)); //experienceLevelFloor
	buff.putDouble(PersoExperience.getExperience(level + 1)); //experienceNextLevelFloor
	buff.putInt(kama); //kamas
	buff.putInt(statsPoints); //StatsPoints
	buff.putInt(spellsPoints); // Spell Point

	// 		=========================== 	//
	// 		ActorAlignmentInformations 		//
	// 		=========================== 	//

	buff.put((byte) alignmentSide); //alignmentSide
	buff.put((byte) 0); //alignmentValue
	buff.put((byte) 0); //alignmentGrade
	buff.putShort((short) 0); //dishonor
	buff.putInt(0); //characterPower //FIXME

	// 		=========================== 		//
	// 	 ActorExtendedAlignmentInformations 	//
	// 		===========================			//

	buff.putShort((short) honor); //honor
	buff.putShort((short) 100); //honorGradeFloor
	buff.putShort((short) 150); // honorNextGradeFloor
	buff.put((byte)(pvpEnabled ? 1 : 0)); //pvpEnabled

	// 		=========================== 		//
	// 	CharacterCharacteristicsInformations 	//
	// 		=========================== 		//

	buff.putInt(life); //lifePoints
	buff.putInt(getAllLifePoints()); //maxLifePoints
	buff.putShort((short) energy); //energyPoints
	buff.putShort((short) GameConfig.MAX_ENERGY); //maxEnergyPoints
	buff.putShort(getStatById(StatsID.ACTION_POINTS.getID()).getBase());	//actionPointsCurrent
	buff.putShort(getStatById(StatsID.MOVEMENT_POINTS.getID()).getBase()); //movementPointsCurrent

	//Stats
	buff.put(getStatById(StatsID.INITIATIVE.getID()).toInfo());
	buff.put(getStatById(StatsID.PROSPECTION.getID()).toInfo());
	buff.put(getStatById(StatsID.ACTION_POINTS.getID()).toInfo());
	buff.put(getStatById(StatsID.MOVEMENT_POINTS.getID()).toInfo());
	buff.put(getStatById(StatsID.STRENGTH.getID()).toInfo());
	buff.put(getStatById(StatsID.VITALITY.getID()).toInfo());
	buff.put(getStatById(StatsID.WISDOM.getID()).toInfo());
	buff.put(getStatById(StatsID.CHANCE.getID()).toInfo());
	buff.put(getStatById(StatsID.AGILITY.getID()).toInfo());
	buff.put(getStatById(StatsID.INTELLIGENCE.getID()).toInfo());
	buff.put(getStatById(StatsID.RANGE.getID()).toInfo());
	buff.put(getStatById(StatsID.SUMMUNABLE_CREATURE.getID()).toInfo());
	buff.put(getStatById(StatsID.REFLECT.getID()).toInfo());
	buff.put(getStatById(StatsID.CRITICAL_HIT.getID()).toInfo());

	buff.putShort((short)0);//criticalHitWeapon

	buff.put(getStatById(StatsID.CRITICAL_MISS.getID()).toInfo());
	buff.put(getStatById(StatsID.HEAL_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.ALL_DAMAGE_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.WEAPON_PERCENT_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.DAMAGE_PERCENT_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.TRAP_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.TRAP_PERCENT_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.PERMANENT_PERCENT_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.TACKLE_BLOCK.getID()).toInfo());
	buff.put(getStatById(StatsID.TACKLE_EVADE.getID()).toInfo());
	buff.put(getStatById(StatsID.PA_ATTACK.getID()).toInfo());
	buff.put(getStatById(StatsID.PM_ATTACK.getID()).toInfo());
	buff.put(getStatById(StatsID.PUSH_DAMAGE_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.CRITICAL_DAMAGE_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.NEUTRAL_DAMAGE_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.EARTH_DAMAGE_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.WATER_DAMAGE_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.AIR_DAMAGE_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.FIRE_DAMAGE_BONUS.getID()).toInfo());
	buff.put(getStatById(StatsID.DODGE_PA_LOST_PROBABILITY.getID()).toInfo());
	buff.put(getStatById(StatsID.DODGE_PM_LOST_PROBABILITY.getID()).toInfo());
	buff.put(getStatById(StatsID.NEUTRAL_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.EARTH_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.WATER_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.AIR_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.FIRE_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.NEUTRAL_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.EARTH_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.WATER_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.AIR_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.FIRE_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.PUSH_DAMAGE_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.CRITICAL_DAMAGE_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.NEUTRAL_PVP_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.EARTH_PVP_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.WATER_PVP_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.AIR_PVP_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.FIRE_PVP_RESIST_PERCENT.getID()).toInfo());
	buff.put(getStatById(StatsID.NEUTRAL_PVP_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.EARTH_PVP_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.WATER_PVP_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.AIR_PVP_REDUCTION.getID()).toInfo());
	buff.put(getStatById(StatsID.FIRE_PVP_REDUCTION.getID()).toInfo());


	buff.putShort((short) 0); //spellModifications.length
	return buff.flip();
    }

    public void generateBaseStats() {
	int id = SqlUtils.getStatService().getNextId();
	stats.add(new Stats(id++,StatsID.INITIATIVE.getID(),(short) 100));
	stats.add(new Stats(id++,StatsID.PROSPECTION.getID(),(short)100));
	stats.add(new Stats(id++,StatsID.ACTION_POINTS.getID(),(short)6));
	stats.add(new Stats(id++,StatsID.MOVEMENT_POINTS.getID(),(short)3));
	stats.add(new Stats(id++,StatsID.STRENGTH.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.VITALITY.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.WISDOM.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.CHANCE.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.AGILITY.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.INTELLIGENCE.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.RANGE.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.SUMMUNABLE_CREATURE.getID(),(short)1));
	stats.add(new Stats(id++,StatsID.REFLECT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.CRITICAL_HIT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.CRITICAL_MISS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.HEAL_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.ALL_DAMAGE_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.WEAPON_PERCENT_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.DAMAGE_PERCENT_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.TRAP_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.TRAP_PERCENT_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.PERMANENT_PERCENT_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.TACKLE_BLOCK.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.TACKLE_EVADE.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.PA_ATTACK.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.PM_ATTACK.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.PUSH_DAMAGE_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.CRITICAL_DAMAGE_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.NEUTRAL_DAMAGE_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.EARTH_DAMAGE_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.WATER_DAMAGE_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.AIR_DAMAGE_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.FIRE_DAMAGE_BONUS.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.DODGE_PA_LOST_PROBABILITY.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.DODGE_PM_LOST_PROBABILITY.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.NEUTRAL_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.EARTH_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.WATER_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.AIR_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.FIRE_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.NEUTRAL_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.EARTH_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.WATER_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.AIR_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.FIRE_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.PUSH_DAMAGE_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.CRITICAL_DAMAGE_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.NEUTRAL_PVP_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.EARTH_PVP_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.WATER_PVP_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.AIR_PVP_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.FIRE_PVP_RESIST_PERCENT.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.NEUTRAL_PVP_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.EARTH_PVP_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.WATER_PVP_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.AIR_PVP_REDUCTION.getID(),(short)0));
	stats.add(new Stats(id++,StatsID.FIRE_PVP_REDUCTION.getID(),(short)0));

	life = getAllLifePoints();
    }
    public void onJoinGame()
    {
	new TextInformation(new Header(MessageID.MSG_TEXT_INFORMATION) , client , (byte) 1 , (byte) 89 , new String[0]); // Message de bienvenue "Bienvenue sur DOFUS, dans le Monde des Douze ! ... "
    }
    @SuppressWarnings("unchecked")
    public IoBuffer toActorInformation() {
	IoBuffer buff = IoBuffer.allocate(4096);
	//TypeId
	buff.putShort((short) 36);

	//Guid
	buff.putInt(guid);

	Definition def = Utils.d2o.files.get("Breeds").read(breed,false);

	//Look
	buff.putUnsignedShort(1);//BonesId
	List<Long> skins;
	if(!isSex())
	    skins = (List<Long>) def.getField("alternativeMaleSkin").value;
	else
	    skins = (List<Long>) def.getField("alternativeFemaleSkin").value;
	buff.putUnsignedShort(skins.size());//Number of skins
	for(Long i : skins) {
	    buff.putUnsignedShort(i.shortValue());
	}
	buff.putUnsignedShort(5);//Number of colors
	for(int i = 0; i < 5; i++) {
	    buff.putUnsignedInt(Integer.parseInt(getColors().get(i)) | (i + 1) * 0x1000000);
	}
	buff.putUnsignedShort(1);//Number of scales
	buff.putUnsignedShort(150);//Default scale
	buff.putUnsignedShort(0);//SubEntities

	//EntityDisposition
	buff.putShort((short) 60);//Type
	buff.putShort((short) cell);
	buff.put((byte) disposition);

	//NamedActor
	try {
	    buff.putPrefixedString(name, Charset.forName("ISO-8859-1").newEncoder());
	} catch (CharacterCodingException e) {
	    e.printStackTrace();
	}

	//Humanoid
	buff.putShort((short) 157);//Type
	buff.putShort((short) 0);//Following characters (Size + data)
	buff.put((byte) 0);//EmoteId
	buff.putDouble(0);//EmoteStartTime

	//Actor restriction
	buff.put((byte) 0);//Flags
	buff.put((byte) 0);//Flags
	buff.put((byte) 0);//Flags

	//TitleId
	buff.putShort((short) title);

	//TitleParam
	buff.putShort((short) titleParams.length());
	buff.put(titleParams.getBytes());

	//Alignment
	buff.put((byte) alignmentSide);//Side
	buff.put((byte) 0);//Value
	buff.put((byte) 0);//Grade
	buff.putShort((short) 0);//Dishonor
	buff.putInt(0);//CharacterPower

	return buff.flip();
    }
    public IoBuffer getGuildInformation()
    {
	IoBuffer buff = IoBuffer.allocate(4096);
	/* MinimalInformations */
	buff.putInt(guid);
	buff.put((byte) level);
	try {

	    buff.putPrefixedString(name, Charset.forName("ISO-8859-1").newEncoder());
	} catch (CharacterCodingException e) {
	    e.printStackTrace();
	}
	/* end MinimalInformations */
	buff.put(breed);
	buff.put((byte) (sex ? 1 : 0));
	buff.putShort((short) guild_rank);
	buff.putDouble(guild_givenExperience);
	buff.put((byte) guild_experienceGivenPercent);
	buff.putUnsignedInt(guild_rights);
	buff.put((byte) (client.getAcc().isConnected() ? 1 : 0));
	//buff.put((byte)alignmentSide

	return buff;
    }
    public Stats getStatById(int id) {
	for(Stats stat : stats) {
	    if(stat.getStatId() == id)
		return stat;
	}
	return null;
    }

    public void levelUp(int level) {
	statsPoints += (5 * level);
	spellsPoints += level;
	this.level += level;
    }

    public GameClient getGameClient()
    {
	return client;
    }
    public void setGameClient(GameClient client)
    {
	this.client = client;
    }
    public int getGuid() {
	return guid;
    }
    public long getActualMap()
    {
	return actualMap;
    }
    public void setActualMap(long map)
    {
	this.actualMap = map;
    }
    public void setGuid(int guid) {
	this.guid = guid;
    }

    public int getGameGuid() {
	return gameGuid;
    }

    public void setGameGuid(int gameGuid) {
	this.gameGuid = gameGuid;
    }

    public byte getBreed() {
	return breed;
    }

    public void setBreed(byte breed) {
	this.breed = breed;
    }

    public boolean isSex() {
	return sex;
    }

    public byte getSexe() {
	return (byte)(isSex() ? 1 : 0);
    }

    public void setSex(boolean sex) {
	this.sex = sex;
    }

    public short getLevel() {
	return level;
    }

    public void setLevel(short level) {
	this.level = level;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getColorsTmp() {
	return colorsTmp;
    }

    public void setColorsTmp(String colorsTmp) {
	this.colorsTmp = colorsTmp;
    }

    public List<String> getColors() {
	if(colors.isEmpty()) {
	    colors = Arrays.asList(colorsTmp.split(","));
	}
	return colors;
    }

    public void setColors(List<String> colors) {
	this.colors = colors;
    }

    public List<Spell> getSpells() {
	return spells;
    }

    public void setSpells(List<Spell> spells) {
	this.spells = spells;
    }

    public int getKama() {
	return kama;
    }

    public void setKama(int kama) {
	this.kama = kama;
    }

    public List<Item> getInventory() {
	return inventory;
    }

    public void setInventory(List<Item> inventory) {
	this.inventory = inventory;
    }

    public int getPods() {
	return pods;
    }

    public void setPods(int pods) {
	this.pods = pods;
    }

    public int getPodsMax() {
	return podsMax;
    }

    public void setPodsMax(int podsMax) {
	this.podsMax = podsMax;
    }

    public int getCell() {
	return cell;
    }

    public void setCell(int cell) {
	this.cell = cell;
    }

    public int getDisposition() {
	return disposition;
    }

    public void setDisposition(int disposition) {
	this.disposition = disposition;
    }

    public long getExperience() {
	return experience;
    }

    public void addExperience(long experienceToAdd) {

	do
	{
	    if((experience + experienceToAdd) > PersoExperience.getExperience(getLevel() + 1))
	    {
		experienceToAdd = (experienceToAdd - (PersoExperience.getExperience(level + 1) + getExperience()));
		if(experienceToAdd < 0)
		{
		    experienceToAdd = -experienceToAdd;
		}
		setExperience(PersoExperience.getExperience(getLevel() + 1));
		levelUp(1);

	    }
	    else if((experience + experienceToAdd) == (PersoExperience.getExperience(getLevel() + 1)))
	    {
		setExperience(PersoExperience.getExperience(getLevel() + 1));
		levelUp(1);
		experienceToAdd = 0;
	    }
	    else
	    {
		setExperience(experience + experienceToAdd);
		experienceToAdd = 0;
	    }
	}
	while(experienceToAdd != 0);

    }
    public void setExperience(long experience) {
	this.experience = experience;
    } 

    public int getSpellsPoints() {
	return spellsPoints;
    }

    public void setSpellsPoints(int spellsPoints) {
	this.spellsPoints = spellsPoints;
    }

    public int getStatsPoints() {
	return statsPoints;
    }

    public void setStatsPoints(int statsPoints) {
	this.statsPoints = statsPoints;
    }
    public int getAllLifePoints()
    {
	int lifeByPerso = 5;
	if(breed == 11)
	    lifeByPerso = 10;
	//modif pour le byte , limit 127
	return (50+(lifeByPerso*(level & 0xFF)) + getStatById(StatsID.VITALITY.getID()).getBase());
	//return (50+(lifeByPerso*level) + getStatById(StatsID.VITALITY.getID()).getBase());
    }
    public int getAllInitiative()
    {
	int bonusStats = getStatById(StatsID.STRENGTH.getID()).getBase() +
		getStatById(StatsID.CHANCE.getID()).getBase() +
		getStatById(StatsID.INTELLIGENCE.getID()).getBase() +
		getStatById(StatsID.AGILITY.getID()).getBase() +
		getStatById(StatsID.INITIATIVE.getID()).getBase();
	return ((bonusStats /* TODO + Item.Initiative */) * (getStatById(StatsID.VITALITY.getID()).getBase() / getAllLifePoints()));
    }

    public boolean getPvpEnabled() {
	return pvpEnabled;
    }

    public void setPvpEnabled(boolean pvpEnabled) {
	this.pvpEnabled = pvpEnabled;
    }

    public int getHonor() {
	return honor;
    }

    public void setHonor(int honor) {
	this.honor = honor;
    }

    public int getLife() {
	return life;
    }

    public void setLife(int life) {
	this.life = life;
    }

    public List<Stats> getStats() {
	return stats;
    }

    public void setStats(List<Stats> stats) {
	this.stats = stats;
    }

    public Stats getStat(int statId) {
	for(Stats stat : stats) {
	    if(stat.getStatId() == statId)
		return stat;
	}
	return null;
    }
    public String getSexeString()
    {
	switch (this.getSexe())
	{
	case 0:
	    return "M";
	case 1:
	    return "F";
	default:
	    return "Unk";
	}
    }
    public String getClassName()
    {
	return Breed.getBreed(breed).name();
    }

    @SuppressWarnings("unchecked")
    public long boostStat(Stats stat, long boost)
    {
	/*
		statsPointsForStrength : [[0, 3], [100, 4], [150, 5]]
		statsPointsForIntelligence : [[0, 3], [100, 4], [150, 5]]
		statsPointsForChance : [[0, 3], [100, 4], [150, 5]]
		statsPointsForAgility : [[0, 3], [100, 4], [150, 5]]
		statsPointsForVitality : [[0, 1, 2]]
		statsPointsForWisdom : [[0, 3]]
	 */
	int statsBase = stat.getBase();

	Definition def = Utils.d2o.files.get("Breeds").read(breed,false);

	long remaining = boost;
	long toAdd = 0;
	long multiplier = 0;

	if(stat.getStatId() == StatsID.STRENGTH.getID()) {
	    List<List<Long>> statsCost = (List<List<Long>>) def.getField("statsPointsForStrength").value;
	    for(int i = 0; i < statsCost.size(); i++) {
		List<Long> cost = statsCost.get(i);
		if(i + 1 < statsCost.size()) {
		    List<Long> nextCost = statsCost.get(i + 1);
		    if((statsBase + remaining) >= nextCost.get(0)) {
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			break;
		    }else{
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			continue;
		    }
		}else{
		    toAdd += (remaining / cost.get(1));
		    remaining = (remaining % cost.get(1));
		    if(cost.size() == 3) {
			multiplier = cost.get(2);
		    }
		    break;
		}
	    }
	}
	else if(stat.getStatId() == StatsID.INTELLIGENCE.getID()) {
	    List<List<Long>> statsCost = (List<List<Long>>) def.getField("statsPointsForIntelligence").value;
	    for(int i = 0; i < statsCost.size(); i++) {
		List<Long> cost = statsCost.get(i);
		if(i + 1 < statsCost.size()) {
		    List<Long> nextCost = statsCost.get(i + 1);
		    if((statsBase + remaining) >= nextCost.get(0)) {
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			break;
		    }else{
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			continue;
		    }
		}else{
		    toAdd += (remaining / cost.get(1));
		    remaining = (remaining % cost.get(1));
		    if(cost.size() == 3) {
			multiplier = cost.get(2);
		    }
		    break;
		}
	    }
	}
	else if(stat.getStatId() == StatsID.CHANCE.getID()) {
	    List<List<Long>> statsCost = (List<List<Long>>) def.getField("statsPointsForChance").value;
	    for(int i = 0; i < statsCost.size(); i++) {
		List<Long> cost = statsCost.get(i);
		if(i + 1 < statsCost.size()) {
		    List<Long> nextCost = statsCost.get(i + 1);
		    if((statsBase + remaining) >= nextCost.get(0)) {
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			break;
		    }else{
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			continue;
		    }
		}else{
		    toAdd += (remaining / cost.get(1));
		    remaining = (remaining % cost.get(1));
		    if(cost.size() == 3) {
			multiplier = cost.get(2);
		    }
		    break;
		}
	    }
	}
	else if(stat.getStatId() == StatsID.AGILITY.getID()) {
	    List<List<Long>> statsCost = (List<List<Long>>) def.getField("statsPointsForAgility").value;
	    for(int i = 0; i < statsCost.size(); i++) {
		List<Long> cost = statsCost.get(i);
		if(i + 1 < statsCost.size()) {
		    List<Long> nextCost = statsCost.get(i + 1);
		    if((statsBase + remaining) >= nextCost.get(0)) {
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			break;
		    }else{
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			continue;
		    }
		}else{
		    toAdd += (remaining / cost.get(1));
		    remaining = (remaining % cost.get(1));
		    if(cost.size() == 3) {
			multiplier = cost.get(2);
		    }
		    break;
		}
	    }
	}
	else if(stat.getStatId() == StatsID.VITALITY.getID()) {
	    List<List<Long>> statsCost = (List<List<Long>>) def.getField("statsPointsForVitality").value;
	    for(int i = 0; i < statsCost.size(); i++) {
		List<Long> cost = statsCost.get(i);
		if(i + 1 < statsCost.size()) {
		    List<Long> nextCost = statsCost.get(i + 1);
		    if((statsBase + remaining) >= nextCost.get(0)) {
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			break;
		    }else{
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			continue;
		    }
		}else{
		    toAdd += (remaining / cost.get(1));
		    remaining = (remaining % cost.get(1));
		    if(cost.size() == 3) {
			multiplier = cost.get(2);
		    }
		    break;
		}
	    }
	}
	else if(stat.getStatId() == StatsID.WISDOM.getID()) {
	    List<List<Long>> statsCost = (List<List<Long>>) def.getField("statsPointsForWisdom").value;
	    for(int i = 0; i < statsCost.size(); i++) {
		List<Long> cost = statsCost.get(i);
		if(i + 1 < statsCost.size()) {
		    List<Long> nextCost = statsCost.get(i + 1);
		    if((statsBase + remaining) >= nextCost.get(0)) {
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			break;
		    }else{
			toAdd += (remaining / cost.get(1));
			remaining = (remaining % cost.get(1));
			if(cost.size() == 3) {
			    multiplier = cost.get(2);
			}
			continue;
		    }
		}else{
		    toAdd += (remaining / cost.get(1));
		    remaining = (remaining % cost.get(1));
		    if(cost.size() == 3) {
			multiplier = cost.get(2);
		    }
		    break;
		}
	    }
	}else{

	    if(stats.contains(stat)) {
		stats.remove(stat);
	    }
	    stat.setBase((short) (statsBase + boost));
	    stats.add(stat);
	    return statsPoints;
	}

	Breed breedClasse = Breed.getBreed(breed);
	switch(breedClasse) {//SpecialByClasse
	case ENUTROF:
	    if(stat.getStatId() == StatsID.CHANCE.getID()) {
		if(toAdd >= 10) {
		    boostStat(getStatById(StatsID.INITIATIVE.getID()),(toAdd/10));
		}
	    }
	    break;
	default:
	    break;
	}

	short newStatsBase = 0;
	if(multiplier > 0) {
	    toAdd = toAdd * multiplier;
	}
	newStatsBase = (short) (statsBase + toAdd);
	if(stats.contains(stat)) {
	    stats.remove(stat);
	}
	stat.setBase(newStatsBase);
	stats.add(stat);

	return newStatsBase;
    }
    public int getGuild_rank() {
	return guild_rank;
    }

    public void setGuild_rank(int guild_rank) {
	this.guild_rank = guild_rank;
    }
    public double getGivenExperience() {
	return guild_givenExperience;
    }
    public void setGivenExperience(double givenExperience) {
	this.guild_givenExperience = givenExperience;
    }

    /**
     * @return the experienceGivenPercent
     */
    public double getExperienceGivenPercent() {
	return guild_experienceGivenPercent;
    }

    /**
     * @param experienceGivenPercent the experienceGivenPercent to set
     */
    public void setExperienceGivenPercent(double experienceGivenPercent) {
	this.guild_experienceGivenPercent = experienceGivenPercent;
    }

    /**
     * @return the rights
     */
    public int getRights() {
	return guild_rights;
    }

    /**
     * @param rights the rights to set
     */
    public void setRights(int rights) {
	this.guild_rights = rights;
    }

    /**
     * @return the alignmentSide
     */
    public byte getAlignmentSide() {
	return alignmentSide;
    }

    /**
     * @param alignmentSide the alignmentSide to set
     */
    public void setAlignmentSide(byte alignmentSide) {
	this.alignmentSide = alignmentSide;
    }

    /**
     * @return the title
     */
    public int getTitle() {
	return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(int title) {
	this.title = title;
    }

    /**
     * @return the energy
     */
    public int getEnergy() {
	return energy;
    }

    /**
     * @param energy the energy to set
     */
    public void setEnergy(int energy) {
	this.energy = energy;
    }

    /**
     * @return the titleParams
     */
    public String getTitleParams() {
	return titleParams;
    }

    /**
     * @param titleParams the titleParams to set
     */
    public void setTitleParams(String titleParams) {
	this.titleParams = titleParams;
    }
}
