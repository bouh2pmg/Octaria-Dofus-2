package data.stat.map;

import data.stat.actor.GroupMonster;
import data.stat.fight.Fight;
import network.Header;
import network.MessageID;
import network.game.message.game.context.GameContext;
import network.game.message.game.context.GameContextDestroy;
import network.game.message.game.context.GameContext.Context;
import network.game.message.game.context.fight.GameFightJoin;
import network.game.message.game.context.fight.GameFightPlacementPossiblePositionsMessage;
import network.game.message.game.context.fight.GameFightStarting;
import network.game.message.game.context.fight.GameFightStarting.FightType;
import reader.d2p.map.CellData;
import server.game.client.GameClient;

public class TmpCell {
	private TmpMap map;
	private CellData cell;
	private GroupMonster monster;
	
	public TmpCell(TmpMap map,CellData cell) {
		this.map = map;
		this.cell = cell;
	}
	
	public void doAction(GameClient client) {
		if(monster != null) {//Fight!
			new GameContextDestroy(new Header(MessageID.MSG_GAME_CONTEXT_DESTROY),client);
			new GameContext(new Header(MessageID.MSG_GAME_CONTEXT_CREATE),client,Context.FIGHT);
			new GameFightStarting(new Header(MessageID.MSG_FIGHT_STARTING),client,FightType.PvM);
			map.removeActorOnMap(monster);
			Fight fight = Fight.getFightByType(FightType.PvM,map);
			new GameFightJoin(new Header(MessageID.MSG_FIGHT_JOIN),client,false,fight);
			new GameFightPlacementPossiblePositionsMessage(new Header(MessageID.MSG_FIGHT_PLACEMENT_POSSIBLE_POSITION),client,fight);
			monster = null;
			return;
		}
	}

	public CellData getCell() {
		return cell;
	}

	public void setCell(CellData cell) {
		this.cell = cell;
	}

	public GroupMonster getMonster() {
		return monster;
	}

	public void setMonster(GroupMonster monster) {
		this.monster = monster;
	}

	public TmpMap getMap() {
		return map;
	}

	public void setMap(TmpMap map) {
		this.map = map;
	}
}
