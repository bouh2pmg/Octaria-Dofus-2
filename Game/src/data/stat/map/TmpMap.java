package data.stat.map;

import java.util.Collection;
import java.util.List;
import java.util.TreeMap;

import network.Header;
import network.MessageID;
import network.game.message.game.context.roleplay.CurrentMap;
import network.game.message.game.context.roleplay.MapRemoveElement;
import network.game.message.game.context.roleplay.ShowActor;

import data.sql.entity.Player;
import data.stat.actor.Actor;
import data.stat.actor.GroupMonster;

import reader.d2o.Definition;
import reader.d2p.map.CellData;
import reader.d2p.map.Map;

public class TmpMap 
{
	public static String decryptionKey =  "649ae451ca33ec53bbcbcc33becf15f4";
	private Map map;
	private TreeMap<Integer,Actor> actors = new TreeMap<Integer,Actor>();
	private TreeMap<Integer,Player> players = new TreeMap<Integer,Player>();
	private TreeMap<Integer,TmpCell> cells = new TreeMap<Integer,TmpCell>();
	private TreeMap<Integer,CellData> redCells = new TreeMap<Integer,CellData>();
	private TreeMap<Integer,CellData> blueCells = new TreeMap<Integer,CellData>();
	
	public TmpMap(Map map)
	{ 
		this.setMap(map);
	}
	
	public void syncronizeCells() 
	{
		for(CellData cell : map.getCells()) 
		{
			if(cell.isBlueFight())
				blueCells.put(cell.getCellId(), cell);
			if(cell.isRedFight())
				redCells.put(cell.getCellId(), cell);
			
			cells.put(cell.getCellId(), new TmpCell(this,cell));
		}
	}
	
	public void generateGroupMonster(List<Definition> defs,int nb) 
	{
		if(redCells.isEmpty() && blueCells.isEmpty()) 
		{
			return;
		}
		for(int i = 0; i < nb; i++) 
		{
			int nextId = getNextId();
			GroupMonster group = new GroupMonster(nextId);
			group.generateMonsters(defs);
			cells.get((int)group.getCellId()).setMonster(group);
			actors.put(nextId, group);
			
		}
	}
	
	public int getNextId() 
	{
		int nextId = 0;
		for(int i = -1;; i--) 
		{
			if(!actors.containsKey(i)) {
				nextId = i;
				break;
			}
		}
		return nextId;
	}
	
	public Collection<Player> getPlayerOnMap()
	{
		return players.values();
	}

	public void addPlayerOnMap(Player player) 
	{
		for(Player p : players.values()) 
		{
			new ShowActor(new Header(MessageID.MSG_SHOW_ACTOR),p.getGameClient(),player);
		}
		new CurrentMap(new Header(MessageID.MSG_CURRENT_MAP),player.getGameClient());
		players.put(player.getGuid(), player);
	}
	
	public void removePlayerOnMap(Player player) 
	{
		if(!players.containsKey(player.getGuid()))
			return;
		
		players.remove(player.getGuid());
		for(Player p : players.values())
		{
			new MapRemoveElement(new Header(MessageID.MSG_MAP_REM_ELEMENT),p.getGameClient(),player.getGuid());
		}
	}
	
	public void removeActorOnMap(Actor actor)
	{
		actors.remove(actor.getContextualId());
		for(Player p : players.values())
		{
			new MapRemoveElement(new Header(MessageID.MSG_MAP_REM_ELEMENT),p.getGameClient(),actor.getContextualId());
		}
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public TreeMap<Integer,TmpCell> getCells() {
		return cells;
	}

	public void setCells(TreeMap<Integer,TmpCell> cells) {
		this.cells = cells;
	}

	public TreeMap<Integer,CellData> getRedCells() {
		return redCells;
	}

	public void setRedCells(TreeMap<Integer,CellData> redCells) {
		this.redCells = redCells;
	}

	public TreeMap<Integer,CellData> getBlueCells() {
		return blueCells;
	}

	public void setBlueCells(TreeMap<Integer,CellData> blueCells) {
		this.blueCells = blueCells;
	}

	public TreeMap<Integer,Actor> getActors() {
		return actors;
	}

	public void setActors(TreeMap<Integer,Actor> actors) {
		this.actors = actors;
	}
}
