package data.stat;

import org.apache.mina.core.buffer.IoBuffer;

public class Effect {
	private short actionId = 0;

	public short getActionId() {
		return actionId;
	}

	public void setActionId(short actionId) {
		this.actionId = actionId;
	}
	
	public IoBuffer toInformation() {
		IoBuffer buff = IoBuffer.allocate(2);
		buff.putShort(actionId);
		return buff.flip();
	}
}
