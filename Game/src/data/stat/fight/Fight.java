package data.stat.fight;

import java.util.HashMap;
import java.util.Map;

import data.stat.map.TmpMap;
import network.game.message.game.context.fight.GameFightStarting.FightType;

public class Fight {
	private TmpMap map;
	private byte fightType;
	private boolean canBeCancelled = false;
	private boolean canSayReady = true;
	private boolean isFightStarted = false;
	private Map<Integer,Team> teams = new HashMap<Integer,Team>();//1 = fighter(red), 2 = defender(blue)
	
	private Fight(TmpMap map,byte fightType,boolean canBeCancelled, boolean canSayReady) {
		this.map = map;
		this.fightType = fightType;
		this.canBeCancelled = canBeCancelled;
		this.canSayReady = canSayReady;
	}
	
	public static Fight getFightByType(byte type,TmpMap map) {
		Fight fight = null;
		switch(type) {
		case FightType.CHALLENGE:
			fight = new Fight(map,type,true,true);
			break;
		case FightType.AGRESSION:
			fight = new Fight(map,type,false,true);
			break;
		case FightType.PvM:
			fight = new Fight(map,type,false,true);
			break;
		}
		return fight;
	}
	
	public byte getFightType() {
		return fightType;
	}
	public void setFightType(byte fightType) {
		this.fightType = fightType;
	}
	public boolean isCanBeCancelled() {
		return canBeCancelled;
	}
	public void setCanBeCancelled(boolean canBeCancelled) {
		this.canBeCancelled = canBeCancelled;
	}
	public boolean isCanSayReady() {
		return canSayReady;
	}
	public void setCanSayReady(boolean canSayReady) {
		this.canSayReady = canSayReady;
	}
	public boolean isFightStarted() {
		return isFightStarted;
	}
	public void setFightStarted(boolean isFightStarted) {
		this.isFightStarted = isFightStarted;
	}

	public TmpMap getMap() {
		return map;
	}

	public void setMap(TmpMap map) {
		this.map = map;
	}

	public Map<Integer,Team> getTeams() {
		return teams;
	}

	public void setTeams(Map<Integer,Team> teams) {
		this.teams = teams;
	}
}
