package data.stat.actor;

import java.util.List;
import java.util.Random;
import java.util.TreeMap;

import org.apache.mina.core.buffer.IoBuffer;

import reader.d2o.Definition;
import utils.LookUtils;

public class GroupMonster extends Actor
{
	private TreeMap<Integer,Monster> monsters = new TreeMap<Integer,Monster>();
	private short cellId = 0;
	private byte disposition = 0;

	public GroupMonster(int contextualId) 
	{
		setContextualId(contextualId);
		Random rnd = new Random();
		cellId = (short) rnd.nextInt(550);
		disposition = (byte) rnd.nextInt(8);
	}
	
	public void generateMonsters(List<Definition> defs) 
	{
		for(int i = 0; i <defs.size(); i++) 
		{
			Definition def = defs.get(i);
			if(!def.classe.equals("com.ankamagames.dofus.datacenter.monsters.Monster"))
				defs.remove(i);
		}
		Random rnd = new Random();
		int size = rnd.nextInt(8) + 1;
		for(int i = 0; i < size; i++) 
		{
			Definition def = defs.get(rnd.nextInt(defs.size()));
			Monster monster = new Monster(def,i);
			monsters.put(i, monster);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public IoBuffer toMapInfo()
	{
		IoBuffer buff = IoBuffer.allocate(2048);
		Monster top = monsters.firstEntry().getValue();
		
		//TypeId
		buff.putShort((short) 160);
		
		//Actor Info
		buff.putInt(getContextualId());
		
		//Entity Look
		buff.put(top.toLook());
		
		//DIsposition
		buff.putShort((short) 60);
		buff.putShort(cellId);//CellId
		buff.put(disposition);//Disposition
		
		//Info
		buff.putShort((short)140);
		buff.putInt(((Integer) top.def.getField("id").value).intValue());
		Definition firstGrade = ((List<Definition>)(top.def.getField("grades").value)).get(0);
		buff.put(((Integer) firstGrade.getField("grade").value).byteValue());
		buff.putShort((short) (monsters.size() - 1));//Underlings
		for(int i = 0; i < monsters.size(); i++)
		{
			if(i == 0)
				continue;
			
			Monster monster = monsters.get(i);
			buff.putInt(((Integer) monster.def.getField("id").value).intValue());
			Definition grade = ((List<Definition>)(monster.def.getField("grades").value)).get(0);
			buff.put(((Integer) grade.getField("grade").value).byteValue());
			buff.put(monster.toLook());
		}
		buff.putShort((short) 0);//AgeBonus
		buff.put((byte)0);//lootShare
		buff.put((byte)0);//alignementSide
		buff.put((byte)0);//keyRingBonus
		return buff.flip();
	}

	public short getCellId()
	{
		return cellId;
	}

	public void setCellId(short cellId) 
	{
		this.cellId = cellId;
	}

	public byte getDisposition() 
	{
		return disposition;
	}

	public void setDisposition(byte disposition)
	{
		this.disposition = disposition;
	}

	public static class Monster
	{
		private int id;
		private Definition def;
		
		public Monster(Definition def,int id)
		{
			this.id = id;
			this.def = def;
		}
		
		public IoBuffer toLook() 
		{
			IoBuffer buff = IoBuffer.allocate(2048);
			LookUtils look = LookUtils.getByString((String) def.getField("look").value);
			buff.putShort(look.getBonesId());
			buff.putShort((short) (look.getSkins().size() + 1));
			buff.putShort(((Integer) def.getField("gfxId").value).shortValue());
			
			for(short skin : look.getSkins()) 
			{
				buff.putShort(skin);
			}
			
			buff.putShort((short) look.getColors().size());
			
			for(int i = 0; i < look.getColors().size(); i++) 
			{
				buff.putInt(look.getColors().get(i) | (i + 1) * 0x1000000);
			}
			buff.putShort((short) look.getScales().size());
			
			for(short scale : look.getScales()) 
			{
				buff.putShort(scale);
			}
			buff.putShort((short) 0);
			return buff.flip();
		}

		public int getId()
		{
			return id;
		}


		public void setId(int id) {
			this.id = id;
		}


		public Definition getDef() {
			return def;
		}


		public void setDef(Definition def) {
			this.def = def;
		}
	}
}
