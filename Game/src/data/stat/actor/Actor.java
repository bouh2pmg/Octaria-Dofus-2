package data.stat.actor;

import org.apache.mina.core.buffer.IoBuffer;

public abstract class Actor {
	private int contextualId = 0;;
	
	public abstract IoBuffer toMapInfo();
	public int getContextualId() {
		return contextualId;
	}
	public void setContextualId(int contextualId) {
		this.contextualId = contextualId;
	}
}
