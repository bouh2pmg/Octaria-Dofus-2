package utils;

import java.util.List;

import reader.d2p.map.CellData;

import data.stat.map.TmpMap;

public class PathUtils {
	public static int getFinalDirection(List<Integer> movement,TmpMap map) {
		int direction = 0;
		for(int i = 0; i < movement.size(); i++) {
			if(i + 1 < movement.size()) {
				CellData cell = map.getCells().get(movement.get(i) & 4095).getCell();
				CellData nextCell = map.getCells().get(movement.get(i + 1) & 4095).getCell();
				int x = nextCell.getX() - cell.getX();
				int y = nextCell.getY() - cell.getY();
				double tan = (Math.acos(x / Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2))) * 180 / Math.PI * (nextCell.getY() > cell.getY() ? -1 : 1));
				tan = Math.round(tan / 45) + 1;
				if(tan < 0)
					tan = tan + 8;
				direction = (int) tan;
			}//End point
		}
		return direction;
	}
}
