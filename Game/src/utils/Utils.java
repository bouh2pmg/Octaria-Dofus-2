package utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.TreeMap;
import reader.d2o.D2OFolder;
import reader.d2p.D2PFolder;
import reader.d2p.map.Map;
import utils.experience.PersoExperience;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;

import data.stat.map.TmpMap;

public class Utils {

	public interface CLibrary extends Library { 
		CLibrary INSTANCE = (CLibrary) 
				Native.loadLibrary((Platform.isWindows() ? "kernel32" : "c"), 
						CLibrary.class); 

		boolean SetConsoleTitleA(String title); 
	}

	public static TreeMap<Long,TmpMap> maps = new TreeMap<Long,TmpMap>();
	public static D2OFolder d2o;

	public static void initD2oFolder()
	{
		d2o = new D2OFolder(GameConfig.D2O_PATH);
	}
	public static void loadStaticData() 
	{
		initD2oFolder();
		
		System.out.print("LOAD EXPERIENCES : ");
		PersoExperience.initialize();
		System.out.println("100%");
		
		System.out.print("LOAD MAPS : ");
		D2PFolder d2p = new D2PFolder(GameConfig.MAPS_PATH);
		for(Map map : d2p.maps)
		{
			maps.put(map.getMapId(), new TmpMap(map));
		}
		System.out.println("100%");
		
		System.out.print("LOAD CELLS : ");
		for(TmpMap map : maps.values()) 
		{
			map.syncronizeCells();
		}
		System.out.println("100%");
		/*System.out.println("Spawning GroupMonster");
		Random rnd = new Random();
		List<Definition> defs = Utils.d2o.files.get("Monsters").readAll(false);
		for(TmpMap map : maps.values()) {
			map.generateGroupMonster(defs,rnd.nextInt(5) + 1);
		}*/
		//defs = null;

	}

	public static String cryptHash(String toHash)
	{
		String hash = "";
		try 
		{
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(toHash.getBytes());
			byte[] data = digest.digest();
			for (int i = 0; i < data.length; ++i) 
			{ 
				hash += (Integer.toHexString((data[i] & 0xFF) | 0x100).substring(1,3)); 
			}
		} 
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		}

		return hash;
	}
	public static String generatePseudo(int length)
	{
		String consonnes= "bcdfghjklmnpqrstvwxz";
		String voyelles= "aeiouy";

		String pass = "";
		int i;
		for(int x=0;x<length;x++)
		{
			if(pass.length() == 0)
			{
				i = (int)Math.floor(Math.random() * 6);
				pass += voyelles.charAt(i);
				pass = pass.toUpperCase();
			}
			switch(generateNumber(1, 3))
			{

			case 1:
				i = (int)Math.floor(Math.random() * 6);
				if(pass.length() < 1)
				{
					if(pass.toCharArray()[(pass.length()-2)] == voyelles.charAt(i))
					{
						i = (int)Math.floor(Math.random() * 6);
						if(pass.toCharArray()[(pass.length()-2)] == voyelles.charAt(i))
						{
							i = (int)Math.floor(Math.random() * 6);
						}
					}
				}
				pass += voyelles.charAt(i);
				break;
			case 2:
				i = (int)Math.floor(Math.random() * 20);
				if(pass.length() < 0)
				{
					if(pass.toCharArray()[(pass.length()-1)] == consonnes.charAt(i))
					{
						i = (int)Math.floor(Math.random() * 20);
						if(pass.toCharArray()[(pass.length()-1)] == consonnes.charAt(i))
						{
							i = (int)Math.floor(Math.random() * 20);
						}
					}
				}
				pass += consonnes.charAt(i);
				break;

			}
		}
		return pass;
	}
	public static int generateNumber(int lower,int higher)
	{
		return (int)(Math.random() * (higher-lower)) + lower;
	}
}
