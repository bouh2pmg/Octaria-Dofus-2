package utils;

import java.util.BitSet;

public class BitsUtils {
	public static BitSet fromInt(int b) {
		BitSet bits = new BitSet(32);
	    for (int i = 0; i < 32; i++) {
	        bits.set(i, (b & 1) == 1);
	        b >>= 1;
	    }
	    return bits;
	}
	
	public static BitSet fromByte(byte b) {
		BitSet bits = new BitSet(8);
	    for (int i = 0; i < 8; i++) {
	        bits.set(i, (b & 1) == 1);
	        b >>= 1;
	    }
	    return bits;
	}
	
	public static int bitSetToInt(BitSet bitSet) { 
	    int bitInteger = 0;
	    for(int i = 0 ; i < 32; i++) {
	    	if(bitSet.get(i)) {
	    		bitInteger |= (1 << i);
	    	}
	    }
	    return bitInteger;
	}
	
	public static byte toByte(BitSet bits) {
	    byte b = 0;
	    for (int i = 0; i < 8; i++) {
	        if (bits.get(i)) {
	            b |= (1 << i);
	        }
	    }
	    return b;
	}
}
