package utils.enums;

import server.game.GameServer;

public enum Breed
{
    UNDEFINED((byte)0),
    FECA((byte)1),
    OSAMODAS((byte)2),
    ENUTROF((byte)3),
    SRAM((byte)4),
    XELOR((byte)5),
    ECAFLIP((byte)6),
    ENIRIPSA((byte)7),
    IOP((byte)8),
    CRA((byte)9),
    SADIDA((byte)10),
    SACRIEUR((byte)11),
    PANDAWA((byte)12),
    ROUBLARD((byte)13),
    ZOBAL((byte)14),
    STREAMER((byte)15),
    SUMMONED((byte)-1),
    MONSTER((byte)-2),
    MONSTER_GROUP((byte)-3),
    NPC((byte)-4),
    HUMAN_VENDOR((byte)-5),
    TAX_COLLECTOR((byte)-6),
    MUTANT((byte)-7),
    MUTANT_IN_DUNGEON((byte)-8),
    MOUNT_OUTSIDE((byte)-9),
    PRISM((byte)-10);

    private byte ID;

    private Breed(byte ID) {
	setID(ID);
    }

    public static Breed getBreed(byte id)
    {
    	switch(id)
    	{
    		case -10:
    			return PRISM;
	    	
    		case -9:
    			return MOUNT_OUTSIDE;
	    	
    		case -8:
    			return MUTANT_IN_DUNGEON;
	    	
    		case -7:
    			return MUTANT;
 	    	
    		case -6:
    			return TAX_COLLECTOR;
 	    	
    		case -5:
    			return HUMAN_VENDOR;
  	    	
    		case -4:
    			return NPC;
  	    	
    		case -3:
    			return MONSTER_GROUP;
  	    	
    	    case -2:
    	    	return MONSTER;
    	    	
    		case -1:
    			return SUMMONED;
    			
    		case 0:
    			return UNDEFINED;
    			
    		case 1:
    			return FECA;
    			
    		case 2:
    			return OSAMODAS;
    		
    		case 3:
    			return ENUTROF;
    			
    		case 4:
    			return SRAM;
    			
    		case 5:
    			return XELOR;
    			
    		case 6:
    			return ECAFLIP;
    			
    		case 7:
    			return ENIRIPSA;
    			
    		case 8:
    			return IOP;
    			
    		case 9:
    			return CRA;
    			
    		case 10:
    			return SADIDA;
    			
    		case 11:
        		return SACRIEUR;
        			
    		case 12:
    			return PANDAWA;
    			
    		case 13:
    			return ROUBLARD;
    			
    		case 14:
    			return ZOBAL;
    		
    		case 15:
    			return STREAMER;
    			
    		default:
    			GameServer.getLogger().error("Error > Breed unknow(" + id + ")");
    			return null;
    	}
	}


    public byte getID() 
    {
    	return ID;
    }

    public void setID(byte ID)
    {
    	this.ID = ID;
    }
}
