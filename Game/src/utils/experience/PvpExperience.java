package utils.experience;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yokaze
 */
public class PvpExperience 
{
	private static Map<Integer,Long> palier = new HashMap<Integer,Long>();

	public static void initialize()
	{
		palier.put(1,0L);
		palier.put(2,500L);
		palier.put(3,1500L);
		palier.put(4,3000L);
		palier.put(5,5000L);
		palier.put(6,7500L);
		palier.put(7,10000L);
		palier.put(8,12500L);
		palier.put(9,15000L);
		palier.put(10,17500L);
	}
	
	public static long getExperience(int level)
	{
		return palier.get(level);
	}
}
