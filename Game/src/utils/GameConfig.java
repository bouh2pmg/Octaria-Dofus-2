package utils;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.ConfigurationException;

public class GameConfig {
	static XMLConfiguration config = null;
	
	public static boolean readConfig(String path) {
    	try {
        	config = new XMLConfiguration();
        	config.setFileName(path);
        	config.load();
        	TIME_OUT = (config.getInt("TIME_OUT")*60*1000);
        	TURN_TIME = (config.getInt("TURN_TIME")*60*1000);
        	USERNAME = config.getString("DB_USER");
        	PASSWORD = config.getString("DB_PASSWORD");
        	GAME_IP = config.getString("GAME_IP");
        	GAME_PORT = config.getInt("GAME_PORT");
        	INTER_IP = config.getString("INTER_IP");
        	INTER_PORT = config.getInt("INTER_PORT");
        	DOFUS_VERSION = config.getString("DOFUS_VERSION");
        	MAPS_PATH = config.getString("MAPS_PATH");
        	D2O_PATH = config.getString("D2O_PATH");
        	config = null;
    	}
    	catch (ConfigurationException ex) {
    		System.out.println("File \"" + config.getFileName() + "\" not found !");
    		return false;
    	}
    	return true;
    }
    
    public static int TIME_OUT = (10*1000*60);
    public static int TURN_TIME = (30*1000*60);
        
    public static String URL = "jdbc:mysql://localhost:3306/0xyk3?autoReconnect=true";
    public static String USERNAME = "root";
    public static String PASSWORD = "";
	
    public static String GAME_IP = "127.0.0.1";
    public static int GAME_PORT = 447;
    
    public static String INTER_IP = "127.0.0.1";
    public static int INTER_PORT = 446;
    
    public static int REQUIRED_VERSION = 1428;
    public static int CURRENT_VERSION = 1428;

    public static String DOFUS_VERSION = "2.5.3.54146.1";
    
    public static String MAPS_PATH = "C://Program Files (x86)//Dofus2//app//content//maps";
    public static String D2O_PATH = "C://Program Files (x86)//Dofus2//app//data//common";
    
    /* GamePlay */
    
    public static int MAX_PERSO_BY_ACCOUNT = 5; // TODO Read this.
    
    public static int MAX_ENERGY = 10000; // TODO Read this.
}
