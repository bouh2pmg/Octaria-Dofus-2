package utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class LookUtils {
	private short bonesId = 0;
	private List<Short> skins = new ArrayList<Short>();
	private List<Integer> colors = new ArrayList<Integer>();
	private List<Short> scales = new ArrayList<Short>();
	
	public static LookUtils getByString(String look) {
		StringBuilder build = new StringBuilder(look);
		build.deleteCharAt(build.indexOf("{"));
		build.deleteCharAt(build.indexOf("}"));
		LookUtils looks = new LookUtils();
		String[] infos = build.toString().split("\\|");
		try {
			if(!infos[0].isEmpty()) {
				looks.setBonesId(Short.parseShort(infos[0]));
			}
			if(!infos[1].isEmpty()) {
				String[] skin = infos[1].split(",");
				for(String s : skin) {
					looks.skins.add(Short.parseShort(s));
				}
			}
			if(!infos[2].isEmpty()) {
				if(infos[2].contains(",")) {
					String[] color = infos[2].split(",");
					for(String c : color) {
						c = c.substring(2);
						if(!c.contains("#"))
							looks.colors.add(Integer.parseInt(c));
						else
							looks.colors.add(Color.decode(c).getRGB());
					}
				}else{
					String c = infos[2].substring(2);
					
					if(!c.contains("#"))
						looks.colors.add(Integer.parseInt(c));
					else
						looks.colors.add(Color.decode(c).getRGB());
				}
			}
			if(!infos[3].isEmpty()) {
				String[] scale = infos[3].split(",");
				for(String s : scale) {
					looks.scales.add(Short.parseShort(s));
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return looks;
	}

	public List<Short> getSkins() {
		return skins;
	}

	public void setSkins(List<Short> skins) {
		this.skins = skins;
	}

	public List<Integer> getColors() {
		return colors;
	}

	public void setColors(List<Integer> colors) {
		this.colors = colors;
	}

	public List<Short> getScales() {
		return scales;
	}

	public void setScales(List<Short> scales) {
		this.scales = scales;
	}

	public short getBonesId() {
		return bonesId;
	}

	public void setBonesId(short bonesId) {
		this.bonesId = bonesId;
	}
}
