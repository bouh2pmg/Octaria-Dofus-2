package utils;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import data.sql.service.IAccountService;
import data.sql.service.IGuildService;
import data.sql.service.IPlayerService;
import data.sql.service.IStatService;


public class SqlUtils {
    //Service
    private static IAccountService accountService;
    private static IPlayerService playerService;
    private static IStatService statService;
    private static IGuildService guildService;

    public static void initContext(){
	String[] springFiles = { "Hibernate.xml", "dao.xml" };
	ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(springFiles);
	accountService = (IAccountService) applicationContext.getBean("accountService");
	playerService = (IPlayerService) applicationContext.getBean("playerService");
	statService = (IStatService) applicationContext.getBean("statService");
	guildService = (IGuildService) applicationContext.getBean("guildService");
    }

    public static IAccountService getAccountService() {
	return accountService;
    }

    public static IPlayerService getPlayerService() {
	return playerService;
    }

    public static IStatService getStatService() {
	return statService;
    }
    
    public static IGuildService getGuildService()
    {
	return guildService;
    }
    public static void setStatService(IStatService statService) {
	SqlUtils.statService = statService;
    }

}
