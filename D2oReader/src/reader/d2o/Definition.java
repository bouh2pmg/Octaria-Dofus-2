package reader.d2o;

import java.util.ArrayList;
import java.util.List;

public class Definition 
{
	public String classe;
	public List<Fields> fields = new ArrayList<Fields>();

	public Definition(String classe1, String classe2) {
		classe = (classe1 + "." + classe2);
	}
	
	public void addField(Fields field) {
		fields.add(field);
	}
	
	public Fields getField(String name) {
		for(Fields field : fields) {
			if(field.name.equalsIgnoreCase(name))
				return field;
		}
		return null;
	}
}