package reader.d2o;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.mina.core.buffer.IoBuffer;

public class Fields {
	public String name;
	private int type;
	public Object value;
	public String readMethod;
	private static int NULL = -1431655766;
	private List<String> innerTypeName;
	private List<String> innerReadMethod;

	public Fields(String name) {
		this.name = name;
	}

	public void getType(IoBuffer reader) {
		type = reader.getInt();
		readMethod = getReadMethod(type,reader);
	}

	public String getReadMethod(int type,IoBuffer reader)
	{
		switch(type) {
		case -1 :
			return "readInteger";
		case -2 :
			return "readBoolean";
		case -3 :
			return "readString";
		case -4 :
			return "readNumber";
		case -5 :
			return "readI18n";
		case -6 :
			return "readUnsignedInteger";
		case -99:
			if(innerReadMethod == null) {
				innerTypeName = new ArrayList<String>();
				innerReadMethod = new ArrayList<String>();
			}
			int nameLen = reader.getShort();
			String name = "";
			for(int i = 0; i < nameLen; i++) {
				name += (char)reader.get();
			}
			String toInv = name.substring(name.lastIndexOf("<") + 1, name.lastIndexOf(">"));
			if(toInv.contains(">")) {
				toInv = toInv.substring(0, toInv.lastIndexOf(">"));
			}
			switch(toInv) {
			case "uint":
				innerTypeName.add("readUnsignedInteger");
				break;
			case "int":
				innerTypeName.add("readInteger");
				break;
			case "String":
				innerTypeName.add("readString");
				break;
			}
			innerReadMethod.add(0,(this.getReadMethod(reader.getInt(),reader)));
			return "readVector";
		default :
			if(type > 0) {
				return "readObject";
			}
		}
		return readMethod;
	}

	public Object readVector(int arg,IoBuffer reader,D2OFile file) 
	{
		int loc1 = reader.getInt();
		List<Object> values = new ArrayList<Object>();
		for(int i = 0; i < loc1; i++) 
		{
			String toInv =  innerReadMethod.get(arg);
			try
			{
				Method method = Fields.class.getMethod(toInv,int.class,IoBuffer.class,D2OFile.class);
				values.add(method.invoke(this,arg + 1,reader,file));
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				System.exit(0);
			}
		}
		return values;
	}

	public Object readObject(int arg,IoBuffer reader,D2OFile file) 
	{
		int classId = reader.getInt();
		if(classId == NULL) {
			return null;
		}
		Definition def = file.getDefinition(classId);
		if(def != null) {
			for(Fields field : def.fields) {
				Class<Fields> clas = Fields.class;
				try {
					Method method = clas.getMethod(field.readMethod,int.class,IoBuffer.class,D2OFile.class);
					field.value = method.invoke(field, 0,reader,file);
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		return def;
	}

	public Object readInteger(int arg,IoBuffer reader,D2OFile file) {
		return reader.getInt();
	}

	public Object readBoolean(int arg,IoBuffer reader,D2OFile file) {
		return (reader.get() == 1 ? true : false);
	}

	public Object readString(int arg,IoBuffer reader,D2OFile file) {
		int nameLen = reader.getShort();
		String name = "";
		for(int i = 0; i < nameLen; i++) {
			name += (char) reader.get();
		}
		return name;
	}

	public Object readNumber(int arg,IoBuffer reader,D2OFile file) {
		return reader.getDouble();
	}

	public Object readI18n(int arg,IoBuffer reader,D2OFile file) {
		return reader.getInt();
	}

	public Object readUnsignedInteger(int arg,IoBuffer reader,D2OFile file) {
		return reader.getUnsignedInt();
	}
}
