package reader.d2o;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;

public class D2OFolder {
	public Map<String,D2OFile> files = new TreeMap<String,D2OFile>();

	public D2OFolder(String filename) 
	{
		File file = new File(filename);
		if(file.isDirectory()) 
		{
			for(File child : file.listFiles()) 
			{
				if(child.getName().contains("d2o")) 
				{
					String module = child.getName().substring(0, child.getName().indexOf('.'));
					files.put(module,new D2OFile(child));
				}
			}
		}
	}
}
