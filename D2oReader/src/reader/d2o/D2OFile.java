package reader.d2o;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.mina.core.buffer.IoBuffer;

public class D2OFile 
{
	public IoBuffer reader;
	public TreeMap<Integer,Integer> indexes = new TreeMap<Integer,Integer>();
	public TreeMap<Integer,Definition> definitionsByClassId = new TreeMap<Integer,Definition>();

	public D2OFile(File file) 
	{
		if(!file.getName().contains(".d2o")) 
		{
			try 
			{
				throw new Exception("Not a d2o file");
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		getReader(file);
		getContent();
	}
	
	private void getReader(File file) 
	{
		try {
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[(int) fis.getChannel().size()];
			fis.read(data);
			fis.close();
			reader = IoBuffer.wrap(data);
			fis = null;
			data = null;
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	private void getContent() 
	{
		String headers = "";
		for(int i = 0; i < 3; i++) 
		{
			headers += (char)reader.get();
		}
		if(headers.contentEquals("D2O"))
		{
			int indexPointers = reader.getInt();
			reader.position(indexPointers);
			int indexLen = reader.getInt();
			for(int i = 0; i < indexLen; i += 8)
			{
				int key = reader.getInt();
				int pointer = reader.getInt();
				indexes.put(key,pointer);
			}
			int classesLen = reader.getInt();
			for(int i = 0; i < classesLen; i++) 
			{
				int classIdentifier = reader.getInt();
				readClassDefinition(classIdentifier);
			}
		}
	}
	
	public void readClassDefinition(int classIdentifier) 
	{
		int loc1Len = reader.getShort();
		String loc1 = "";
		for(int i = 0; i < loc1Len; i++) 
		{
			loc1 += (char)reader.get();
		}
		int loc2Len = reader.getShort();
		String loc2 = "";
		for(int i = 0; i < loc2Len; i++) 
		{
			loc2 += (char)reader.get();
		}
		Definition def = new Definition(loc2,loc1);
		int fieldLen = reader.getInt();
		for(int i = 0; i < fieldLen; i++) 
		{
			int nameLen = reader.getShort();
			String name = "";
			for(int j = 0; j < nameLen; j++) 
			{
				char c = (char) reader.get();
				name += c;
			}
			Fields field = new Fields(name);
			field.getType(reader);
			def.addField(field);
		}
		definitionsByClassId.put(classIdentifier, def);
	}
	
	public Definition read(int key,boolean debug) 
	{
		if(!indexes.containsKey(key))
			return null;
		
		reader.position(indexes.get(key));
		int classId = reader.getInt();
		Definition def = definitionsByClassId.get(classId);
		if(def != null) 
		{
			for(Fields field : def.fields) 
			{
				Class<Fields> clas = Fields.class;
				try 
				{
					Method method = clas.getMethod(field.readMethod,int.class,IoBuffer.class,D2OFile.class);
					field.value = method.invoke(field, 0,reader,this);
					if(debug)
						System.out.println(field.name + " : " + field.value);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		}
		return def;
	}
	
	public List<Definition> readAll(boolean debug) 
	{
		List<Definition> defs = new ArrayList<Definition>();
		for(int key : indexes.keySet()) 
		{
			defs.add(read(key,debug));
		}
		return defs;
	}
	
	public Definition getDefinition(int classId)
	{
		return definitionsByClassId.get(classId);
	}
}
