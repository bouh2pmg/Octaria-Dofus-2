import java.util.Map.Entry;

import reader.d2o.D2OFolder;
import reader.d2o.Definition;
import reader.d2o.Fields;

public class Main 
{
	public static void main(String[] args)
	{
		D2OFolder d2o = new D2OFolder("C://Program Files (x86)//Dofus2//app//data//common");
		int ia = 0;
		for(Entry<Integer, Integer> i : d2o.files.get("Items").indexes.entrySet())
		{
			Definition def = d2o.files.get("Items").read(i.getKey(),false);
			System.out.println("===");
			
			for(Fields fields : def.fields)
			{
				System.out.println(fields.name);
				ia++;
			}
			System.out.println("SIZE : " + ia);
			ia = 0;
		}
	}
}
