package crypto;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RSA {
	public final static int KEY_SIZE = 1024;  // [1024 = performance or 2048 = security]

	private RSAPublicKeySpec pub;
	private PublicKey pk; 
	private RSAPrivateKeySpec priv;
	private PrivateKey pr;
	private Cipher cipher;

	public RSA() 
	{
		try 
		{
			cipher = Cipher.getInstance("RSA");
		} 
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		} 
		catch (NoSuchPaddingException e) 
		{
			e.printStackTrace();
		}
		generateKeyPair();
	}

	public void setPublicKeySpec(KeyPair kp) 
	{
		try
		{
			KeyFactory fact = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec pub = fact.getKeySpec(kp.getPublic(),RSAPublicKeySpec.class);
			setPub(pub);
		}catch(Exception e) {
			System.out.println(e);
		} 
	}

	public void setPrivateKeySpec(KeyPair kp) 
	{
		try 
		{
			KeyFactory fact = KeyFactory.getInstance("RSA");
			RSAPrivateKeySpec priv = fact.getKeySpec(kp.getPrivate(),
					RSAPrivateKeySpec.class);
			setPriv(priv);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		} 
	}

	public void generateKeyPair() {
		try 
		{
			KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
			keyPairGen.initialize(KEY_SIZE);
			KeyPair kp = keyPairGen.generateKeyPair();
			pk = kp.getPublic();
			pr = kp.getPrivate();
			setPublicKeySpec(kp);
			setPrivateKeySpec(kp);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		} 
	}

	public byte[] crypt(String plaintext) {
		byte[] data = null;
		try 
		{
			cipher.init(Cipher.ENCRYPT_MODE, pk);
			data = cipher.doFinal(plaintext.getBytes());
		} 
		catch (InvalidKeyException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalBlockSizeException e) 
		{
			e.printStackTrace();
		} 
		catch (BadPaddingException e) 
		{
			e.printStackTrace();
		}
		return data;
	}

	public byte[] crypt(byte[]src) {
		byte[] data = null;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, pk);
			data = cipher.doFinal(src);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return data;
	}

	public byte[] decryptInBytes(byte[] ciphertext) {
		byte[] data = null;
		try {
			cipher.init(Cipher.DECRYPT_MODE, pr);
			data = cipher.doFinal(ciphertext);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return data;
	}

	public String decryptInString(byte[] ciphertext) {
		byte[] data = decryptInBytes(ciphertext);
		return new String(data,0,data.length);
	}     

	public PublicKey getPk() {
		return pk;
	}

	public void setPk(PublicKey pk) {
		this.pk = pk;
	}
	public void setPk(byte[] pk)
	{
		try 
		{
			this.pk = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(pk));
		} 
		catch (InvalidKeySpecException e) 
		{
			e.printStackTrace();
		} 
		catch (NoSuchAlgorithmException e) 
		{
			e.printStackTrace();
		}
	}
	public PrivateKey getPr() 
	{
		return pr;
	}

	public void setPr(PrivateKey pr)
	{
		this.pr = pr;
	}

	public RSAPrivateKeySpec getPriv()
	{
		return priv;
	}

	public void setPriv(RSAPrivateKeySpec priv) 
	{
		this.priv = priv;
	}

	public RSAPublicKeySpec getPub() 
	{
		return pub;
	}

	public void setPub(RSAPublicKeySpec pub)
	{
		this.pub = pub;
	}
}

